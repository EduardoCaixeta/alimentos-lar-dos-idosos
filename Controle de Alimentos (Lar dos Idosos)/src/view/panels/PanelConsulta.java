
package view.panels;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import java.awt.Component;
import javax.swing.BorderFactory;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Canvas;
import java.awt.Font;
import java.awt.Cursor;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import view.panels.utils.*;
import view.format.Limite_digitos;

@SuppressWarnings("serial")
public class PanelConsulta extends JPanel implements TratamentoDate {
	
	public PanelConsulta(String codUser, int daysAlert) {
		this.codUser = codUser;
		this.setBackground(new Color(240, 0, 189));
		scrollPane = new JScrollPane();
		table = new JTable();
		table.setCursor(new Cursor(Cursor.HAND_CURSOR));
		canvasVencidos = new Canvas();
		canvasIn15 = new Canvas();
		canvasBefore15 = new Canvas();
		labelVencidos = new JLabel();
		labelIn15 = new JLabel();
		labelBefore15 = new JLabel();
		labelNome = new JLabel();
		txtCountRows = new JTextField();
		txtStatus = new JTextField();
		txtCodBar = new JTextField();
		separador1 = new JSeparator();
		separador2 = new JSeparator();
		separador3 = new JSeparator();
		labelCodBar = new JLabel();
		txtNome = new UpperCaseField_Limitado(50, "[^a-z||^A-Z||^0-9||[ ]]",true);
		buttonPesquisa = new JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonDelete = new JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonAdd = new JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonEdit = new JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};

		buttonMarcaAll = new JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		

		initComponents(daysAlert);
	}

	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon(new File("").getCanonicalPath()+"\\images\\FundoConsulta.png").getImage(), 0, 0, this.getWidth(),this.getHeight(),this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int diffInDays(String d1, String d2) throws Exception {
		try {
			int MILLIS_IN_DAY = 86400000;

			Calendar c1 = Calendar.getInstance();
			c1.set(Calendar.MILLISECOND, 0);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.HOUR_OF_DAY, 0);

			Calendar c2 = Calendar.getInstance();
			c2.setTime(stringToDate(d2));
			c2.set(Calendar.MILLISECOND, 0);
			c2.add(Calendar.MONTH, -1);
			c2.set(Calendar.SECOND, 0);
			c2.set(Calendar.MINUTE, 0);
			c2.set(Calendar.HOUR_OF_DAY, 0);
			return (int) ((c2.getTimeInMillis() - c1.getTimeInMillis()) / MILLIS_IN_DAY);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public Date stringToDate(String string) throws Exception {
		try {
			Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
			Matcher matcher = dataPadrao.matcher(string);
			if (matcher.matches()) {
				int dia = Integer.parseInt(matcher.group(1));
				int mes = Integer.parseInt(matcher.group(2));
				int ano = Integer.parseInt(matcher.group(3));
				return (new GregorianCalendar(ano, mes, dia)).getTime();
			} else
				throw new Exception(" Data final inv�lida.");
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void initComponents(int daysAlert) {
		try {
			
			for(Component c: getComponents())
			{
				if(c instanceof JTextField)
				{
					((JTextField)c).setText("");
				}
			}
			
			this.removeAll();
			
			
			
			txtNome.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtNome.setFont(new Font("Arial", 0, 12)); // NOI18N
			txtCodBar.setFont(new Font("Arial", 0, 12)); // NOI18N
			txtCodBar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtCodBar.setDocument(new Limite_digitos(20, "[^0-9]"));

			modelTable = new ValidadeTableModel(codUser);
			

			setOpaque(false);
			setPreferredSize(new Dimension(650, 500));

			labelCodBar.setFont(new Font("Arial", 0, 12)); // NOI18N
			labelCodBar.setText("C�digo de Barras:");
			
			buttonPesquisa.setIcon(new ImageIcon(new File("").getCanonicalPath() + "\\images\\pesquisa.png")); // NOI18N
			buttonPesquisa.setBorder(null);
			buttonPesquisa.setContentAreaFilled(false);

			buttonDelete.setIcon(new ImageIcon(new File("").getCanonicalPath() + "\\images\\deletelittle.png")); // NOI18N
			buttonDelete.setBorder(null);
			buttonDelete.setContentAreaFilled(false);

			buttonAdd.setIcon(new ImageIcon(new File("").getCanonicalPath() + "\\images\\addlittle.png")); // NOI18N
			buttonAdd.setBorder(null);
			buttonAdd.setContentAreaFilled(false);

			buttonMarcaAll.setIcon(new ImageIcon(new File("").getCanonicalPath() + "\\images\\select.png")); // NOI18N
			buttonMarcaAll.setBorder(null);
			buttonMarcaAll.setContentAreaFilled(false);

			cellRender = new DefaultTableCellRenderer() {
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
						boolean hasFocus, int row, int column) {
					super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
					setBackground(new Color(204, 204, 204));
					return this;
				}
			};
			cellRender.setHorizontalAlignment(SwingConstants.CENTER);
			table.setFont(new Font("Arial", 0, 12)); // NOI18N
			table.setFocusCycleRoot(true);
			table.setRowHeight(19);
			table.setModel(modelTable);
			table.setShowHorizontalLines(false);
			table.getColumnModel().getColumn(4).setPreferredWidth(75);
			table.getColumnModel().getColumn(3).setPreferredWidth(75);
			table.getColumnModel().getColumn(2).setPreferredWidth(344);
			table.getColumnModel().getColumn(1).setPreferredWidth(96);
			table.getColumnModel().getColumn(0).setPreferredWidth(30);
			table.getTableHeader().getColumnModel().getColumn(4).setHeaderRenderer(cellRender);
			table.getTableHeader().getColumnModel().getColumn(3).setHeaderRenderer(cellRender);
			table.getTableHeader().getColumnModel().getColumn(2).setHeaderRenderer(cellRender);
			table.getTableHeader().getColumnModel().getColumn(1).setHeaderRenderer(cellRender);
			table.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(cellRender);
			table.getTableHeader().setResizingAllowed(false);
			table.getTableHeader().setReorderingAllowed(false);
			table.setShowVerticalLines(false);
			table.setBorder(null);

			cellRender = new DefaultTableCellRenderer() {
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
						boolean hasFocus, int row, int column) {
					super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

					try {
						Object obj = table.getModel().getValueAt(row, 4);
						int time = diffInDays("", String.valueOf(obj));
						if (time > daysAlert) {
							if (isSelected) {
								setBackground(new Color(100, 255, 100));
							} else
								setBackground(new Color(200, 255, 200));

						} else {
							if (time >= 0) {
								if (isSelected) {
									setBackground(new Color(244, 244, 0));
								} else
									setBackground(new Color(244, 244, 150));
							} else {
								if (isSelected) {
									setBackground(new Color(255, 100, 100));
								} else
									setBackground(new Color(255, 200, 200));
							}
						}
					} catch (Exception e) {

					}

					return this;
				}
			};
			cellRender.setHorizontalAlignment(SwingConstants.CENTER);
			table.getColumnModel().getColumn(4).setCellRenderer(cellRender);
			table.getColumnModel().getColumn(3).setCellRenderer(cellRender);
			table.getColumnModel().getColumn(1).setCellRenderer(cellRender);
			table.getColumnModel().getColumn(0).setCellRenderer(cellRender);
			table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
						boolean hasFocus, int row, int column) {
					super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

					try {
						Object obj = table.getModel().getValueAt(row, 4);
						int time = diffInDays("", String.valueOf(obj));
						if (time > daysAlert) {
							if (isSelected) {
								setBackground(new Color(100, 255, 100));
							} else
								setBackground(new Color(200, 255, 200));

						} else {
							if (time >= 0) {
								if (isSelected) {
									setBackground(new Color(244, 244, 0));
								} else
									setBackground(new Color(244, 244, 150));
							} else {
								if (isSelected) {
									setBackground(new Color(255, 100, 100));
								} else
									setBackground(new Color(255, 200, 200));
							}
						}
					} catch (Exception e) {

					}

					return this;
				}
			});
			table.setSelectionForeground(Color.BLACK);
			table.setIntercellSpacing(new Dimension(0, 0));
			scrollPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
			scrollPane.setViewportView(table);

			canvasVencidos.setBackground(new Color(255, 100, 100));
			canvasBefore15.setBackground(new Color(100, 255, 100));
			canvasIn15.setBackground(new Color(255, 255, 53));

			labelNome.setFont(new Font("Arial", 0, 12)); // NOI18N
			labelNome.setText("Nome:");

			labelVencidos.setFont(new Font("Dialog", 0, 12)); // NOI18N
			labelVencidos.setText("   Vencidos");

			labelIn15.setFont(new Font("Dialog", 0, 12)); // NOI18N
			labelIn15.setText("Vencimento dentro de "+daysAlert+" dias");

			labelBefore15.setFont(new Font("Dialog", 0, 12)); // NOI18N
			labelBefore15.setText("Vencimento depois de "+daysAlert+" dias");

			txtStatus.setEditable(false);
			txtStatus.setFont(new Font("Arial", 0, 12)); // NOI18N
			txtStatus.setDragEnabled(true);
			txtStatus.setOpaque(false);
			txtStatus.setBorder(null);
			txtStatus.setHorizontalAlignment(JTextField.RIGHT);

			txtCountRows.setEditable(false);
			txtCountRows.setFont(new Font("Arial", 0, 12)); // NOI18N
			txtCountRows.setDragEnabled(true);
			txtCountRows.setOpaque(false);
			txtCountRows.setBorder(null);
			txtCountRows.setHorizontalAlignment(JTextField.RIGHT);
			txtCountRows.setText(table.getRowCount() + " Validades cadastradas");

			buttonEdit.setFont(new Font("Yu Gothic", 0, 11)); // NOI18N
			buttonEdit.setIcon(new ImageIcon(new File("").getCanonicalPath() + "\\images\\editlittle.png")); // NOI18N
			buttonEdit.setBorder(null);
			buttonEdit.setContentAreaFilled(false);
			buttonEdit.setContentAreaFilled(false);
			buttonEdit.setDisabledIcon(null);
			buttonEdit.setFocusPainted(false);
			buttonEdit.setEnabled(true);

			buttonAdd.setEnabled(true);
			buttonAdd.setFocusPainted(false);
			buttonDelete.setEnabled(true);
			buttonDelete.setFocusPainted(false);
			buttonMarcaAll.setEnabled(true);
			buttonMarcaAll.setFocusPainted(false);
			buttonPesquisa.setEnabled(true);
			buttonPesquisa.setFocusPainted(false);

			JLabel labelLocal = new JLabel();
			JLabel comboLocal = new JLabel();
			
			GroupLayout layout = new GroupLayout(this);
			this.setLayout(layout);
			this.setLayout(layout);
			layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup().addContainerGap().addComponent(scrollPane).addGap(10, 10,
									10))
					.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
							.addGap(0, 171, Short.MAX_VALUE)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
									.addGroup(layout.createSequentialGroup()
											.addComponent(labelCodBar, javax.swing.GroupLayout.PREFERRED_SIZE, 100,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addComponent(txtCodBar))
									.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
											layout.createSequentialGroup().addComponent(labelLocal).addGap(6, 6, 6)
													.addComponent(comboLocal, javax.swing.GroupLayout.PREFERRED_SIZE,
															85,
															javax.swing.GroupLayout.PREFERRED_SIZE)
													.addGap(141, 141, 141).addComponent(buttonPesquisa,
															javax.swing.GroupLayout.PREFERRED_SIZE, 39,
															javax.swing.GroupLayout.PREFERRED_SIZE))
									.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
											layout.createSequentialGroup().addComponent(labelNome)
													.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
													.addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 263,
															javax.swing.GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 175, Short.MAX_VALUE)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addComponent(separador3, javax.swing.GroupLayout.PREFERRED_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addComponent(separador2, javax.swing.GroupLayout.PREFERRED_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addComponent(separador1, javax.swing.GroupLayout.PREFERRED_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.PREFERRED_SIZE)))
					.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup()
									.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(txtCountRows, javax.swing.GroupLayout.PREFERRED_SIZE, 607,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addContainerGap())
					.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
							layout.createSequentialGroup().addContainerGap().addComponent(buttonMarcaAll)
									.addGap(18, 18, 18).addComponent(buttonAdd).addGap(18, 18, 18)
									.addComponent(buttonEdit).addGap(18, 18, 18).addComponent(buttonDelete)
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
											javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 336,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addContainerGap())
					.addGroup(layout.createSequentialGroup().addContainerGap()
							.addComponent(canvasVencidos, javax.swing.GroupLayout.PREFERRED_SIZE, 27,
									javax.swing.GroupLayout.PREFERRED_SIZE)
							.addGap(2, 2, 2).addComponent(labelVencidos).addGap(84, 84, 84)
							.addComponent(canvasIn15, javax.swing.GroupLayout.PREFERRED_SIZE, 27,
									javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(labelIn15)
							.addGap(46, 46, 46)
							.addComponent(canvasBefore15, javax.swing.GroupLayout.PREFERRED_SIZE, 27,
									javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addComponent(labelBefore15)
							.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
			layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
					javax.swing.GroupLayout.Alignment.TRAILING,
					layout.createSequentialGroup().addContainerGap().addGroup(layout
							.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
							.addComponent(separador3, javax.swing.GroupLayout.PREFERRED_SIZE,
									javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addComponent(separador2, javax.swing.GroupLayout.PREFERRED_SIZE,
									javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addComponent(separador1, javax.swing.GroupLayout.PREFERRED_SIZE,
									javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
											.addComponent(labelNome).addComponent(txtNome,
													javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
													javax.swing.GroupLayout.PREFERRED_SIZE))
									.addGap(14, 14, 14)
									.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
											.addComponent(txtCodBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
													javax.swing.GroupLayout.PREFERRED_SIZE)
											.addComponent(labelCodBar, javax.swing.GroupLayout.PREFERRED_SIZE, 22,
													javax.swing.GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
									.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
											.addGroup(layout
													.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
													.addComponent(labelLocal, javax.swing.GroupLayout.PREFERRED_SIZE,
															22, javax.swing.GroupLayout.PREFERRED_SIZE)
													.addComponent(comboLocal, javax.swing.GroupLayout.PREFERRED_SIZE,
															javax.swing.GroupLayout.DEFAULT_SIZE,
															javax.swing.GroupLayout.PREFERRED_SIZE))
											.addComponent(buttonPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 22,
													javax.swing.GroupLayout.PREFERRED_SIZE))))
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
									.addComponent(buttonDelete).addComponent(buttonAdd).addComponent(buttonMarcaAll)
									.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addComponent(buttonEdit))
							.addGap(9, 9, 9)
							.addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addComponent(txtCountRows, javax.swing.GroupLayout.PREFERRED_SIZE,
									javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addGap(6, 6, 6)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
									.addComponent(canvasVencidos, javax.swing.GroupLayout.Alignment.TRAILING,
											javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)
									.addComponent(canvasIn15, javax.swing.GroupLayout.Alignment.TRAILING,
											javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)
									.addComponent(canvasBefore15, javax.swing.GroupLayout.Alignment.TRAILING,
											javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)
									.addComponent(labelIn15, javax.swing.GroupLayout.Alignment.TRAILING,
											javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)
									.addComponent(labelVencidos, javax.swing.GroupLayout.Alignment.TRAILING,
											javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
											Short.MAX_VALUE)
									.addComponent(labelBefore15))
							.addGap(6, 6, 6)));
			for(Component c: getComponents())
			{
				if(c instanceof JLabel)
				{
					((JLabel)c).setForeground(Color.WHITE);
				}
			}
			txtStatus.setForeground(Color.WHITE);
			txtCountRows.setForeground(Color.WHITE);
			labelLocal.setVisible(false);
			comboLocal.setVisible(false);
			
		} catch (Exception e) {

		}
	}

	public void noSelect() {
		table.clearSelection();
	}

	// Variables declaration - do not modify
	private JButton buttonAdd;
	private JButton buttonDelete;
	private JButton buttonMarcaAll;
	private JButton buttonPesquisa;
	private JButton buttonEdit;
	private Canvas canvasBefore15;
	private Canvas canvasIn15;
	private Canvas canvasVencidos;
	private JLabel labelBefore15;
	private JLabel labelCodBar;
	private JLabel labelIn15;
	private JLabel labelVencidos;
	private JLabel labelNome;
	private JScrollPane scrollPane;
	private JTable table;
	private JTextField txtCountRows;
	private JTextField txtCodBar, txtNome;
	private DefaultTableCellRenderer cellRender;
	private JTextField txtStatus;
	private ValidadeTableModel modelTable;
	private JSeparator separador1, separador2, separador3;
	private String codUser;
	// End of variables declaration

	public void analiseEndDate(String text) {
	}

	// Start GetterSetterExtension Source Code
	/** GET Method Propertie buttonAdd */
	public JButton getButtonAdd() {
		return this.buttonAdd;
	}// end method getButtonAdd

	/** SET Method Propertie buttonAdd */
	public void setButtonAdd(JButton buttonAdd) {
		this.buttonAdd = buttonAdd;
	}// end method setButtonAdd

	/** GET Method Propertie buttonDelete */
	public JButton getButtonDelete() {
		return this.buttonDelete;
	}// end method getButtonDelete

	/** SET Method Propertie buttonDelete */
	public void setButtonDelete(JButton buttonDelete) {
		this.buttonDelete = buttonDelete;
	}// end method setButtonDelete

	/** GET Method Propertie buttonMarcaAll */
	public JButton getButtonMarcaAll() {
		return this.buttonMarcaAll;
	}// end method getButtonMarcaAll

	/** SET Method Propertie buttonMarcaAll */
	public void setButtonMarcaAll(JButton buttonMarcaAll) {
		this.buttonMarcaAll = buttonMarcaAll;
	}// end method setButtonMarcaAll

	/** GET Method Propertie buttonPesquisa */
	public JButton getButtonPesqLocal() {
		return this.buttonPesquisa;
	}// end method getButtonPesqLocal

	/** SET Method Propertie buttonPesquisa */
	public void setButtonPesqLocal(JButton buttonPesquisa) {
		this.buttonPesquisa = buttonPesquisa;
	}// end method setButtonPesqLocal

	/** GET Method Propertie canvasBefore15 */
	public Canvas getCanvasBefore15() {
		return this.canvasBefore15;
	}// end method getCanvasBefore15

	/** SET Method Propertie canvasBefore15 */
	public void setCanvasBefore15(Canvas canvasBefore15) {
		this.canvasBefore15 = canvasBefore15;
	}// end method setCanvasBefore15

	/** GET Method Propertie canvasIn15 */
	public Canvas getCanvasIn15() {
		return this.canvasIn15;
	}// end method getCanvasIn15

	/** SET Method Propertie canvasIn15 */
	public void setCanvasIn15(Canvas canvasIn15) {
		this.canvasIn15 = canvasIn15;
	}// end method setCanvasIn15

	/** GET Method Propertie canvasVencidos */
	public Canvas getCanvasVencidos() {
		return this.canvasVencidos;
	}// end method getCanvasVencidos

	/** SET Method Propertie canvasVencidos */
	public void setCanvasVencidos(Canvas canvasVencidos) {
		this.canvasVencidos = canvasVencidos;
	}// end method setCanvasVencidos


	/** GET Method Propertie labelBefore15 */
	public JLabel getLabelBefore15() {
		return this.labelBefore15;
	}// end method getLabelBefore15

	/** SET Method Propertie labelBefore15 */
	public void setLabelBefore15(JLabel labelBefore15) {
		this.labelBefore15 = labelBefore15;
	}// end method setLabelBefore15

	/** GET Method Propertie labelCodBar */
	public JLabel getLabelCodBar() {
		return this.labelCodBar;
	}// end method getLabelCodBar

	/** SET Method Propertie labelCodBar */
	public void setLabelCodBar(JLabel labelCodBar) {
		this.labelCodBar = labelCodBar;
	}// end method setLabelCodBar

	/** GET Method Propertie labelIn15 */
	public JLabel getLabelIn15() {
		return this.labelIn15;
	}// end method getLabelIn15

	/** SET Method Propertie labelIn15 */
	public void setLabelIn15(JLabel labelIn15) {
		this.labelIn15 = labelIn15;
	}// end method setLabelIn15

	/** GET Method Propertie labelVencidos */
	public JLabel getLabelVencidos() {
		return this.labelVencidos;
	}// end method getLabelVencidos

	/** SET Method Propertie labelVencidos */
	public void setLabelVencidos(JLabel labelVencidos) {
		this.labelVencidos = labelVencidos;
	}// end method setLabelVencidos

	/** GET Method Propertie scrollPane */
	public JScrollPane getScrollPane() {
		return this.scrollPane;
	}// end method getScrollPane

	/** SET Method Propertie scrollPane */
	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}// end method setScrollPane

	/** GET Method Propertie table */
	public JTable getTable() {
		return this.table;
	}// end method getTable

	/** SET Method Propertie table */
	public void setTable(JTable table) {
		this.table = table;
	}// end method setTable

	/** GET Method Propertie cellRender */
	public DefaultTableCellRenderer getCellRender() {
		return this.cellRender;
	}// end method getCellRender

	/** SET Method Propertie cellRender */
	public void setCellRender(DefaultTableCellRenderer cellRender) {
		this.cellRender = cellRender;
	}// end method setCellRender

	/** GET Method Propertie txtStatus */
	public JTextField getTxtStatus() {
		return this.txtStatus;
	}// end method getTxtStatus

	/** SET Method Propertie txtStatus */
	public void setTxtStatus(JTextField txtStatus) {
		this.txtStatus = txtStatus;
	}// end method setTxtStatus

	// End GetterSetterExtension Source Code
	// !

	// Start GetterSetterExtension Source Code
	/** GET Method Propertie modelTable */
	public ValidadeTableModel getModelTable() {
		return this.modelTable;
	}// end method getModelTable

	/** SET Method Propertie modelTable */
	public void setModelTable(ValidadeTableModel modelTable) {
		this.modelTable = modelTable;
	}// end method setModelTable

	// End GetterSetterExtension Source Code
	// !

	// Start GetterSetterExtension Source Code
	/** GET Method Propertie txtCountRows */
	public JTextField getTxtCountRows() {
		return this.txtCountRows;
	}// end method getTxtCountRows

	/** SET Method Propertie txtCountRows */
	public void setTxtCountRows(JTextField txtCountRows) {
		this.txtCountRows = txtCountRows;
	}// end method setTxtCountRows

	/** GET Method Propertie separador1 */
	public JSeparator getSeparador1() {
		return this.separador1;
	}// end method getSeparador1

	/** SET Method Propertie separador1 */
	public void setSeparador1(JSeparator separador1) {
		this.separador1 = separador1;
	}// end method setSeparador1

	/** GET Method Propertie separador2 */
	public JSeparator getSeparador2() {
		return this.separador2;
	}// end method getSeparador2

	/** SET Method Propertie separador2 */
	public void setSeparador2(JSeparator separador2) {
		this.separador2 = separador2;
	}// end method setSeparador2

	/** GET Method Propertie separador3 */
	public JSeparator getSeparador3() {
		return this.separador3;
	}// end method getSeparador3

	/** SET Method Propertie separador3 */
	public void setSeparador3(JSeparator separador3) {
		this.separador3 = separador3;
	}// end method setSeparador3

	// End GetterSetterExtension Source Code
	// !

	// End GetterSetterExtension Source Code
	// !

	// Start GetterSetterExtension Source Code
	/** GET Method Propertie buttonPesquisa */
	public JButton getButtonPesquisa() {
		return this.buttonPesquisa;
	}// end method getButtonPesquisa

	/** SET Method Propertie buttonPesquisa */
	public void setButtonPesquisa(JButton buttonPesquisa) {
		this.buttonPesquisa = buttonPesquisa;
	}// end method setButtonPesquisa

	/** GET Method Propertie labelNome */
	public JLabel getLabelNome() {
		return this.labelNome;
	}// end method getLabelNome

	/** SET Method Propertie labelNome */
	public void setLabelNome(JLabel labelNome) {
		this.labelNome = labelNome;
	}// end method setLabelNome

	/** GET Method Propertie txtCodBar */
	public JTextField getTxtCodBar() {
		return this.txtCodBar;
	}// end method getTxtCodBar

	/** SET Method Propertie txtCodBar */
	public void setTxtCodBar(JTextField txtCodBar) {
		this.txtCodBar = txtCodBar;
	}// end method setTxtCodBar

	/** GET Method Propertie txtNome */
	public JTextField getTxtNome() {
		return this.txtNome;
	}// end method getTxtNome

	/** SET Method Propertie txtNome */
	public void setTxtNome(JTextField txtNome) {
		this.txtNome = txtNome;
	}// end method setTxtNome

	// End GetterSetterExtension Source Code
	// !

	// Start GetterSetterExtension Source Code
	/** GET Method Propertie buttonEdit */
	public JButton getButtonEdit() {
		return this.buttonEdit;
	}// end method getButtonEdit

	/** SET Method Propertie buttonEdit */
	public void setButtonEdit(JButton buttonEdit) {
		this.buttonEdit = buttonEdit;
	}// end method setButtonEdit

	// End GetterSetterExtension Source Code
//!
}
