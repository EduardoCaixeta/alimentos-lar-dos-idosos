package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;

import model.entity.Produtos;

import java.util.List;

public class ProdutosDao
{
    public static Database database;
    public static Dao<Produtos, Integer> dao;
	public Produtos produtoCarregada;
    private List<Produtos> produtosCarregados;

    public ProdutosDao (Database database) throws Exception
    {
        ProdutosDao.setDatabase(database);
        try
        {
        produtosCarregados = dao.queryForAll();
        }
        catch(Exception e)
        {
        	throw new Exception("Erro ao ler produtos cadastrados");
        }
    }

    public static void setDatabase(Database database)
    {
        ProdutosDao.database = database;
        try
        {
            dao = DaoManager.createDao(database.getConnection(), Produtos.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Produtos.class);
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }

    public void create (Produtos produto) throws Exception
    {
        int nrows = 0;
        try
        {
            if(existCodBar(produto.getCodBar()) == false)
            {
            nrows = dao.create(produto);
            if(nrows == 0)
                throw new SQLException("Erro ao cadastrar o produto.");
            this.produtoCarregada = produto;
            produtosCarregados.add (produto);
        } else throw new Exception("Erro ao cadastrar o produto: Produto já cadastrado.");
        }
        catch(SQLException e)
        {
            throw new Exception();
        }
    }   

    public List<Produtos> readAll() throws Exception
    {
        try 
        {
            this.produtosCarregados = dao.queryForAll();
            if(this.produtosCarregados.size() !=0)
                this.produtoCarregada = this.produtosCarregados.get(0);
        }
        catch(SQLException e)
        {
            throw new Exception();
        }

        return this.produtosCarregados;
    }

    public Produtos loadForCodBar(String codBar) throws Exception
    {
        Produtos produto;
        try 
        {
            this.produtosCarregados = dao.queryForAll();
            if(this.produtosCarregados.size() == 0)
                throw new Exception("N�o h� produtos cadastrados.");
            for( int i = 0; i < produtosCarregados.size(); i++)
            {
                if( codBar.equals(produtosCarregados.get(i).getCodBar()))
                {
                    produto = produtosCarregados.get(i);
                    return produto;
                }
            }
            throw new Exception("Produto não registrado.");
        }
        catch(SQLException e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public boolean existCodBar(String codBar) throws Exception
    {
        try 
        {
            this.produtosCarregados = dao.queryForAll();
            for( int i = 0; i < produtosCarregados.size(); i++)
            {
                if( codBar.equals(produtosCarregados.get(i).getCodBar()))
                {               
                    return true;
                }
            }
            return false;
        }
        catch(SQLException e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void update (Produtos produto) throws Exception
    {
        try
        {
            dao.update(produto);
        }   
        catch (java.sql.SQLException e)
        {
            throw new Exception("N�o foi poss�vel atualizar o produto.");
        }   
    }
    
    public void delete(Produtos produto) throws Exception
    {
        try
        {
            dao.delete(produto);
        }   
        catch (java.sql.SQLException e)
        {
            throw new Exception("N�o foi poss�vel deletar o produto.");
        }   
    }
}