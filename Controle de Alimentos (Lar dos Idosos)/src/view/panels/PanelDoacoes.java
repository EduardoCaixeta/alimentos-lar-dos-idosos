package view.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.MaskFormatter;

import controller.DoacaoController;
import controller.SettingsController;
import model.Database;
import model.entity.Doacao;
import model.entity.Users;
import view.App;
import view.format.Limite_digitos;
import view.forms.utils.CreatePDF;
import view.forms.utils.OptionConfirmDoacao;
import view.forms.utils.SendEmailDonation;
import view.panels.utils.DoacaoTableModel;
import view.panels.utils.UpperCaseField_Limitado;

/**
 *
 * @author EDUARDO
 */
@SuppressWarnings("serial")
public class PanelDoacoes extends javax.swing.JPanel {

	public Database dbDoacao, dbSettings;
	private DoacaoController controllerDoacao1;
	private JFrame frame;
	private Users user;
	private App app;

	public PanelDoacoes(Database dD, Database dS, JFrame frame, Users user, App app) {
		this.dbDoacao = dD;
		this.app = app;
		this.frame = frame;
		this.user = user;
		this.dbSettings = dS;
		initComponents();
	}

	public void inicioState() {

		initComponents();
	}

	@Override
	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon(new File("").getCanonicalPath() + "\\images\\FundoDoacao.jpeg").getImage(),
					this.getX(), this.getY(), this.getWidth(), this.getHeight(), this);
		} catch (IOException e) {
			System.out.print(e.getMessage());
		}

	}

	public void initComponents() {
		this.removeAll();
		buttonVoltar = null;
		buttonNewDoacao = new javax.swing.JButton();
		buttonSeeDoacao = new javax.swing.JButton();

		buttonNewDoacao.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
		buttonNewDoacao.setText("NOVA DOA��O");

		buttonNewDoacao.setContentAreaFilled(false);
		buttonNewDoacao.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
		buttonNewDoacao.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonNewDoacao.setBackground(Color.RED);
		buttonNewDoacao.addActionListener(e -> initComponentsNew());
		buttonNewDoacao.setFocusCycleRoot(true);
		buttonNewDoacao.setFocusPainted(false);
		buttonNewDoacao.setFocusable(false);

		buttonSeeDoacao.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
		buttonSeeDoacao.setText("CONSULTAR DOA��ES");
		buttonSeeDoacao.addActionListener(e -> initComponentsSee());
		buttonSeeDoacao.setFocusCycleRoot(true);
		buttonSeeDoacao.setContentAreaFilled(false);
		buttonSeeDoacao.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
		buttonSeeDoacao.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonSeeDoacao.setBackground(Color.RED);
		buttonSeeDoacao.setFocusPainted(false);
		buttonSeeDoacao.setFocusable(false);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap(108, Short.MAX_VALUE)
						.addComponent(buttonNewDoacao, javax.swing.GroupLayout.PREFERRED_SIZE, 152,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(56, 56, 56).addComponent(buttonSeeDoacao, javax.swing.GroupLayout.PREFERRED_SIZE, 208,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(126, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap(213, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(buttonNewDoacao, javax.swing.GroupLayout.PREFERRED_SIZE, 44,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonSeeDoacao, javax.swing.GroupLayout.PREFERRED_SIZE, 44,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(243, Short.MAX_VALUE)));
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JButton buttonNewDoacao;
	private javax.swing.JButton buttonSeeDoacao;

	public void initComponentsNew() {

		this.removeAll();
		labelTitulo = new javax.swing.JLabel();
		labelTipo = new javax.swing.JLabel();
		labelEspecifique = new javax.swing.JLabel();
		comboDoacao = new javax.swing.JComboBox<>();
		buttonVoltar = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonVoltar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
		buttonVoltar.setText("VOLTAR");
		buttonVoltar.setBorder(BorderFactory.createMatteBorder(3, 2, 2, 3, Color.RED));
		buttonVoltar.setContentAreaFilled(false);
		buttonVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonVoltar.setFocusable(false);
		buttonVoltar.setForeground(Color.WHITE);
		buttonVoltar.setBackground(Color.RED);
		buttonVoltar.setOpaque(true);
		buttonVoltar.addActionListener(e -> inicioState());
		txtOutros = new UpperCaseField_Limitado(50, "", false, true) {
			String text;

			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				if (enabled == false) {
					if (super.getText().isEmpty() == false)
						text = super.getText();
					super.setText("");
					super.setBorder(null);
				} else {
					super.setText(text);
					super.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

				}

				labelEspecifique.setVisible(enabled);

			}

			@Override
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}
		};
		txtOutros.setMargin(new java.awt.Insets(3, 0, 0, 0));
		labelData = new javax.swing.JLabel();
		labelDoador = new javax.swing.JLabel();
		txtDoador = new UpperCaseField_Limitado(30, "", false, true) {
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		txtDoador.setMargin(new java.awt.Insets(3, 0, 0, 0));
		labelReceptor = new javax.swing.JLabel();
		try {
			txtData = new JFormattedTextField(new MaskFormatter("##/##/####")) {
				public void setEditable(boolean edit) {
					super.setEditable(edit);
					if (edit) {
						super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
						super.setOpaque(true);
					} else {
						super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
						super.setOpaque(false);
					}
				}
			};
			txtData.setFont(new java.awt.Font("Arial", 0, 12));
			txtData.setHorizontalAlignment(SwingConstants.RIGHT);
		} catch (ParseException e) {
			txtData = new JTextField();
			txtData.setDocument(new Limite_digitos(10, "[^0-9|[/]]"));
			txtData.setFont(new java.awt.Font("Arial", 0, 12));
		}
		labelQntd = new javax.swing.JLabel();
		txtReceptor = new UpperCaseField_Limitado(35, "[^a-zA-Z ]", false, true) {
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		txtReceptor.setMargin(new java.awt.Insets(3, 0, 0, 0));
		labelPhoto = new javax.swing.JLabel();
		labeDesc = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();

		spinnerQntd = new JSpinner();

		txtDesc = new javax.swing.JTextArea() {
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		buttonSave = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonClear = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		checkSendTesouraria = new javax.swing.JCheckBox();

		txtStatus = new javax.swing.JTextField();

		labelCaracteres = new javax.swing.JLabel();

		labelTitulo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
		labelTitulo.setText("DOA��ES");

		labelTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelTipo.setText("Tipo de doa��o:");

		labelEspecifique.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEspecifique.setText("Especifique:");

		comboDoacao.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		comboDoacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dinheiro", "Alimento", "Outros" }));
		comboDoacao.setVerifyInputWhenFocusTarget(false);
		comboDoacao.setFocusCycleRoot(false);
		comboDoacao.setOpaque(false);
		comboDoacao.setBackground(new Color(190, 190, 190));
		comboDoacao.setFocusable(false);
		comboDoacao.setBorder(null);
		comboDoacao.addItemListener(e -> comboNew_Click());

		txtOutros.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		labelData.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelData.setText("Data de recebimento:");

		labelDoador.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelDoador.setText("Doador:");

		txtDoador.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		labelReceptor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelReceptor.setText("Receptor:");

		labelQntd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelQntd.setText("Quantidade:");

		txtReceptor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		labelPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		try {
			labelPhoto.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//velhos.png"));
		} catch (IOException e1) {
			txtStatus.setText(e1.getMessage());
		} // NOI18N

		labeDesc.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labeDesc.setText("Descri��o:");

		jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPane1.setOpaque(false);

		txtDesc.setColumns(20);
		txtDesc.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtDesc.setDocument(new Limite_digitos(120, "",true));
		txtDesc.setRows(5);
		txtDesc.setTabSize(3);
		txtDesc.setWrapStyleWord(true);
		txtDesc.setDragEnabled(true);
		txtDesc.setLineWrap(true);
		txtDesc.setMargin(new java.awt.Insets(3, 3, 2, 2));
		txtDesc.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				labelCaracteres.setText(String.valueOf(txtDesc.getText().length()) + "/120");

			}

			
			public void keyPressed(KeyEvent e) {
				labelCaracteres.setText(String.valueOf(txtDesc.getText().length()) + "/120");

			}
		});
		jScrollPane1.setViewportView(txtDesc);

		buttonSave.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonSave.setText("SALVAR");
		buttonSave.setVerifyInputWhenFocusTarget(false);

		buttonClear.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonClear.setText("LIMPAR");
		buttonClear.setVerifyInputWhenFocusTarget(false);

		checkSendTesouraria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		checkSendTesouraria.setText("Enviar email � tesouraria");
		checkSendTesouraria.setVerifyInputWhenFocusTarget(false);
		checkSendTesouraria.setFocusCycleRoot(false);
		checkSendTesouraria.setOpaque(false);
		checkSendTesouraria.setBackground(new Color(190, 190, 190));
		checkSendTesouraria.setFocusable(false);
		checkSendTesouraria.setBorder(null);
		checkSendTesouraria.addActionListener(e -> {
			try {
				checkSendTesouraria.setSelected(new SettingsController(dbSettings).getSettings().isSendEmailTesouraria());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

		spinnerQntd.setFont(new java.awt.Font("Arial", 0, 12));
		spinnerQntd.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.1d, null, 0.01d));
		spinnerQntd.setVerifyInputWhenFocusTarget(false);

		labelCaracteres.setText("0/120");
		labelCaracteres.setFont(new java.awt.Font("Arial", 0, 12));

		txtStatus.setFont(new java.awt.Font("Arial", 0, 12));
		txtStatus.setOpaque(false);
		txtStatus.setEditable(false);
		txtStatus.setFocusable(false);
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap(14, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
						.addGroup(layout.createSequentialGroup().addComponent(labelTipo)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(comboDoacao, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(labelEspecifique))
						.addGroup(layout.createSequentialGroup().addComponent(labelData)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 96,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addComponent(labelQntd)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(spinnerQntd, javax.swing.GroupLayout.PREFERRED_SIZE, 76,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addComponent(labelReceptor)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txtReceptor))
						.addGroup(layout.createSequentialGroup().addComponent(labelDoador)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txtDoador, javax.swing.GroupLayout.PREFERRED_SIZE, 211,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup()
						.addGap(100,100,100)
						.addComponent(checkSendTesouraria)
						)
						.addGroup(layout.createSequentialGroup().addComponent(labeDesc)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(labelCaracteres))
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 256,
								javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addGap(18, 18, 18)
								.addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 332,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(26, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup()
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txtOutros, javax.swing.GroupLayout.PREFERRED_SIZE, 199,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(buttonSave)
						.addGap(37, 37, 37).addComponent(buttonClear)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 380,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonVoltar))
						.addGap(0, 0, Short.MAX_VALUE))
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(labelTitulo)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addGap(10, 10, 10)
				.addComponent(
						labelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(labelTipo)
						.addComponent(comboDoacao, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(txtOutros, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(labelEspecifique))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addComponent(labelPhoto, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(labelDoador).addComponent(txtDoador,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(labelReceptor).addComponent(txtReceptor,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(labelData).addComponent(txtData,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(labelQntd)
										.addComponent(spinnerQntd, javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup().addComponent(labeDesc)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(18, 18, 18).addComponent(checkSendTesouraria))
										.addComponent(labelCaracteres))
								.addGap(5, 5, 5)))
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(buttonSave).addComponent(buttonClear))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(buttonVoltar)));
		txtData.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		jScrollPane1.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtDoador.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtReceptor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtStatus.setBorder(null);
		spinnerQntd.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

		txtOutros.setEnabled(false);
		txtOutros.setOpaque(false);

		for (Component c : getComponents()) {
			if (c instanceof JButton) {
				((JButton) c).setEnabled(true);
				((JButton) c).setFocusable(false);
			}
		}
		try {
			getCheckSendTesouraria()
					.setSelected(new SettingsController(dbSettings).getSettings().isSendEmailTesouraria());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}

		FormDoacaoNew(dbDoacao, frame, user);
	}// </editor-fold>

	public void comboNew_Click() {
		try {
			if (((String) comboDoacao.getSelectedItem()).equals("Outros")) {
				txtOutros.setEnabled(true);
				txtOutros.setOpaque(true);
				txtOutros.requestFocus();
			} else {
				txtOutros.setEnabled(false);
				txtOutros.setOpaque(false);
				txtDoador.requestFocus();
			}
			if (((String) comboDoacao.getSelectedItem()).equals("Dinheiro")
					|| ((String) comboDoacao.getSelectedItem()).equals("Outros")) {
				spinnerQntd.setModel(new javax.swing.SpinnerNumberModel(1.0d, 1.0d, null, 0.01d));
			} else
				spinnerQntd.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

		} catch (Exception e) {

		}
	}

	public void FormDoacaoNew(Database dbD, JFrame frame, Users user) {
		this.frame = frame;
		this.user = user;
		this.dbDoacao = dbD;
		try {

			controllerDoacao1 = new DoacaoController(dbDoacao);
			this.getCheckSendTesouraria()
					.setSelected(new SettingsController(dbSettings).getSettings().isSendEmailTesouraria());
		} catch (Exception e) {
			this.getTxtStatus().setText(e.getMessage());
		}

		this.getTxtOutros().addActionListener(e -> txtOutros_passFocus());
		this.getTxtDoador().addActionListener(e -> txtDoador_passFocus());
		this.getTxtReceptor().addActionListener(e -> txtReceptor_passFocus());
		this.getTxtData().addActionListener(e -> txtData_passFocus());

		this.getButtonSave().addActionListener(e -> save_Click());
		this.getButtonClear().addActionListener(e -> clearTextFieldsNew());

	}

	@SuppressWarnings("deprecation")
	public void txtOutros_passFocus() {
		this.getTxtOutros().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (this.getTxtOutros().getText().isEmpty() == false)
			this.getTxtOutros().nextFocus();
		else
			this.getTxtOutros().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
	}

	@SuppressWarnings("deprecation")
	public void txtDoador_passFocus() {
		this.getTxtDoador().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (this.getTxtDoador().getText().isEmpty() == false)
			this.getTxtDoador().nextFocus();
		else
			this.getTxtDoador().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
	}

	@SuppressWarnings("deprecation")
	public void txtReceptor_passFocus() {
		this.getTxtReceptor().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (this.getTxtReceptor().getText().isEmpty() == false)
			this.getTxtReceptor().nextFocus();
		else
			this.getTxtReceptor().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
	}

	@SuppressWarnings("deprecation")
	public void txtData_passFocus() {
		this.getTxtData().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (this.getTxtData().getText().isEmpty() == false) {
			try {
				checkEndDate();
				this.getTxtData().nextFocus();
			} catch (Exception e) {
				this.getTxtData().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				this.getTxtData().requestFocus();
				this.getTxtStatus().setText(e.getMessage().toUpperCase());
			}

		} else
			this.getTxtData().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
	}

	public void clearTextFieldsNew() {
		Component components[] = this.getComponents();

		for (Component c : components) {
			if (c instanceof JTextField) {
				((JTextField) c).setText("");

			}

		}
		if (((String) getComboDoacao().getSelectedItem()).equals("Outros"))
			getTxtOutros().requestFocus();
		else
			getTxtDoador().requestFocus();
		this.getTxtDesc().setText("");
		this.getLabelCaracteres().setText("0/120");
		this.getSpinnerQntd().setValue((Object) 1);
	}

	public void save_Click() {
		try {
			this.getTxtStatus().setText("");
			Doacao doacao = getDoacao();
			doacao.setIndexDiario();
			new OptionConfirmDoacao(doacao.getCodigo(), frame) {
				@SuppressWarnings("deprecation")
				public void confirm_Click() {
					try {
						if (getTxtPassword().getText().isEmpty() == false) {
							if (getTxtPassword().getText().equals(user.getPassword())) {
								if (new SettingsController(dbSettings).getSettings().isSendEmailTesouraria()) {
									new Thread(new Runnable() {
										public void run() {
											try {
												new SendEmailDonation(doacao,
														new SettingsController(dbSettings).getSettings());
											} catch (Exception e) {
												JOptionPane.showMessageDialog(frame, e.getMessage());
											}

										}
									}).start();
								}
								controllerDoacao1.create(doacao);
								close();
								clearTextFieldsNew();

								// ENVIAR EMAIL A TESOURARIA

								throw new Exception("Doa��o registrada com sucesso.");
							} else {
								getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
								throw new Exception("Senha incorreta.");
							}

						} else {
							getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							throw new Exception("Insira a senha.");
						}
					} catch (Exception e) {
						getTxtStatus().setText(e.getMessage());
					}
				}
			};
		} catch (Exception e) {
			this.getTxtStatus().setText(e.getMessage());
		}
	}

	public Doacao getDoacao() throws Exception {
		Doacao doacao = new Doacao();

		if (((String) this.getComboDoacao().getSelectedItem()).equals("Outros") == false) {
			doacao.setTipo((String) this.getComboDoacao().getSelectedItem());
		} else if (this.getTxtOutros().getText().isEmpty() == false) {
			this.getTxtOutros().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			doacao.setTipo(this.getTxtOutros().getText());
		} else {
			this.getTxtOutros().requestFocus();
			this.getTxtOutros().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Especifique o tipo de doa��o.");
		}

		this.getTxtDoador().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (this.getTxtDoador().getText().isEmpty() == false) {
			doacao.setDoador(this.getTxtDoador().getText());
		} else {
			this.getTxtDoador().requestFocus();
			this.getTxtDoador().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Insira o nome do doador.");
		}

		this.getTxtReceptor().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (this.getTxtReceptor().getText().isEmpty() == false) {
			doacao.setReceptor(this.getTxtReceptor().getText());
		} else {
			this.getTxtReceptor().requestFocus();
			this.getTxtReceptor().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Insira o nome do receptor (seu nome).");
		}

		this.getTxtData().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		try {
			checkEndDate();
			Calendar data = Calendar.getInstance();
			data.setTime(stringToDate(this.getTxtData().getText()));
			data.add(Calendar.MONTH, -1);
			doacao.setData(data.getTime());
		} catch (Exception e) {
			this.getTxtData().requestFocus();
			this.getTxtData().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception(e.getMessage());
		}

		doacao.setDesc(this.getTxtDesc().getText());
		doacao.setQnt(String.valueOf(this.getSpinnerQntd().getValue()));
		doacao.setDb(dbDoacao);
		return doacao;
	}

	public void checkEndDate() throws Exception {
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(stringToDate(this.getTxtData().getText()));

		try {
			Calendar rgCalendar = Calendar.getInstance();
			endCalendar.add(Calendar.MONTH, -1);
			analiseEndDate(this.getTxtData().getText());
			if (endCalendar.before(rgCalendar) == false) {
				throw new Exception(" Data Inv�lida");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}
	}

	public Date stringToDate(String string) throws Exception {
		Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
		Matcher matcher = dataPadrao.matcher(string);
		if (matcher.matches()) {
			int dia = Integer.parseInt(matcher.group(1));
			int mes = Integer.parseInt(matcher.group(2));
			int ano = Integer.parseInt(matcher.group(3));
			return (new GregorianCalendar(ano, mes, dia)).getTime();
		} else
			throw new Exception(" Data final inv�lida.");
	}

	public void analiseEndDate(String text) throws Exception {
		try {
			int dia = Integer.parseInt(text.substring(0, 2));
			int mes = Integer.parseInt(text.substring(3, 5));
			int ano = Integer.parseInt(text.substring(6, 10));

			int[] meses31 = { 4, 6, 9, 11 };

			if (1 > dia || dia > 31)
				throw new Exception("Dia inv�lido");

			if (1 > mes || mes > 12)
				throw new Exception("M�s inv�lido");
			if (dia == 31) {
				for (int i = 0; i < 4; i++) {
					if (mes == meses31[i]) {
						throw new Exception("Dia inv�lido");
					}
				}
			}
			if (mes == 2) {
				boolean ehBissexto = false;
				if ((ano % 400) == 0) {
					ehBissexto = true;
				} else {
					if (((ano % 4) == 0) && ((ano % 100) != 0)) {
						ehBissexto = true;
					}
				}

				if (ehBissexto) {
					if (dia > 29)
						throw new Exception("Dia inv�lido");
				} else {
					if (dia > 28)
						throw new Exception("Dia inv�lido");
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}

	}

	public JPanel getPanel() {
		return this;
	}

	public void comboSee_Click() {
		try {
			if (((String) comboTipo.getSelectedItem()).equals("Outros")) {
				txtTipo.setEnabled(true);
				txtTipo.setOpaque(true);
				txtTipo.requestFocus();
			} else {
				txtTipo.setOpaque(false);
				txtTipo.setEnabled(false);
			}

		} catch (Exception e) {

		}
	}

	public void initComponentsSee() {
		this.removeAll();
		labelCod = new javax.swing.JLabel();
		labelTipo = new javax.swing.JLabel();
		labelReceptor = new javax.swing.JLabel();
		labelData = new javax.swing.JLabel();
		txtCod = new javax.swing.JTextField() {
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		comboTipo = new javax.swing.JComboBox<>();
		labelEspecifique = new javax.swing.JLabel();
		txtTipo = new UpperCaseField_Limitado(40, "", false, true) {
			String text;

			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				if (enabled == false) {
					if (super.getText().isEmpty() == false)
						text = super.getText();
					super.setText("");
					super.setBorder(null);
				} else {
					super.setText(text);
					super.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

				}

				labelEspecifique.setVisible(enabled);

			}

			@Override
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}
		};
		txtReceptor = new UpperCaseField_Limitado(35, "[^a-zA-Z ]", false, true) {
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		try {
			txtData = new JFormattedTextField(new MaskFormatter("##/##/####")) {
				public void setEditable(boolean edit) {
					super.setEditable(edit);
					if (edit) {
						super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
						super.setOpaque(true);
					} else {
						super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
						super.setOpaque(false);
					}
				}
			};
			txtData.setFont(new java.awt.Font("Arial", 0, 12));
			txtData.setHorizontalAlignment(SwingConstants.RIGHT);
		} catch (ParseException e) {
			txtData = new JTextField();
			txtData.setDocument(new Limite_digitos(10, "[^0-9|[/]]"));
			txtData.setFont(new java.awt.Font("Arial", 0, 12));
		}
		buttonPesquisa = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		scrollPane = new javax.swing.JScrollPane();
		table = new javax.swing.JTable();
		txtCountRows = new javax.swing.JLabel();
		buttonVoltar = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonAdd = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonMarcaAll = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonEdit = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonMore = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		progressBar = new javax.swing.JProgressBar();
		txtStatus = new javax.swing.JTextField();
		model = new DoacaoTableModel(dbDoacao);

		labelCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelCod.setText("C�digo:");

		labelTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelTipo.setText("Tipo:");

		labelReceptor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelReceptor.setText("Receptor:");

		labelData.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelData.setText("Data:");

		txtCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		comboTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		comboTipo.setModel(
				new javax.swing.DefaultComboBoxModel<>(new String[] { "Todos", "Dinheiro", "Alimento", "Outros" }));
		comboTipo.setVerifyInputWhenFocusTarget(false);
		comboTipo.setFocusCycleRoot(false);
		comboTipo.setOpaque(false);
		comboTipo.setBackground(new Color(190, 190, 190));
		comboTipo.setFocusable(false);
		comboTipo.setBorder(null);
		comboTipo.addItemListener(e -> comboSee_Click());

		labelEspecifique.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEspecifique.setText("Especifique:");

		txtTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		txtReceptor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		txtData.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonPesquisa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonVoltar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18
		buttonVoltar.setText("VOLTAR");
		buttonVoltar.setBorder(BorderFactory.createMatteBorder(3, 2, 2, 3, Color.RED));
		buttonVoltar.setContentAreaFilled(false);
		buttonVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonVoltar.setFocusable(false);
		buttonVoltar.setForeground(Color.WHITE);
		buttonVoltar.setBackground(Color.RED);
		buttonVoltar.setOpaque(true);
		buttonVoltar.addActionListener(e -> inicioState());
		table.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		cellRender = new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
				setBackground(new Color(204, 204, 204));
				return this;
			}
		};
		cellRender.setHorizontalAlignment(SwingConstants.CENTER);
		table.setFont(new Font("Arial", 0, 12)); // NOI18N
		table.setFocusCycleRoot(true);
		table.setRowHeight(19);
		table.setModel(model);
		table.setShowHorizontalLines(false);
		table.getColumnModel().getColumn(4).setPreferredWidth(75);
		table.getColumnModel().getColumn(3).setPreferredWidth(75);
		table.getColumnModel().getColumn(2).setPreferredWidth(344);
		table.getColumnModel().getColumn(1).setPreferredWidth(70);
		table.getColumnModel().getColumn(0).setPreferredWidth(90);
		table.getTableHeader().getColumnModel().getColumn(4).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(3).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(2).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(1).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(cellRender);
		table.getTableHeader().setResizingAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.setShowVerticalLines(false);
		table.setBorder(null);

		cellRender = new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

					if (isSelected) {
						setBackground(new Color(100, 100, 255));
					} else
						setBackground(new Color(160, 160, 255));

				return this;
			}
		};
		cellRender.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(4).setCellRenderer(cellRender);
		table.getColumnModel().getColumn(3).setCellRenderer(cellRender);

		table.getColumnModel().getColumn(1).setCellRenderer(cellRender);
		table.setDefaultRenderer(Object.class, cellRender);

		table.getColumnModel().getColumn(0).setCellRenderer(cellRender);

		table.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

				try {

					if (isSelected) {
						setBackground(new Color(100, 100, 255));
					} else
						setBackground(new Color(160, 160, 255));

				} catch (Exception e) {

				}

				return this;
			}
		});
		table.setSelectionForeground(Color.BLACK);
		table.setIntercellSpacing(new Dimension(0, 0));

		scrollPane.setViewportView(table);

		txtCountRows.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
		txtCountRows.setFont(new java.awt.Font("Arial", 0, 12));

		buttonAdd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonEdit.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonMore.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		try {
			buttonMarcaAll.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\select.png")); // NOI18N
			buttonPesquisa
					.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\pesquisa.png")); // NOI18N
			buttonEdit.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\editlittle.png")); // NOI18N
			buttonAdd.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\addlittle.png")); // NOI18N
			buttonMore.setIcon(
					new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\menu_vertical_16px.png")); // NOI18N
		} catch (Exception e) {
			txtStatus.setText(e.getMessage());
		}
		txtStatus.setEditable(false);
		txtStatus.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtStatus.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		txtStatus.setFocusable(false);
		txtStatus.setOpaque(false);
		txtStatus.setRequestFocusEnabled(false);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addContainerGap()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(scrollPane)
										.addGroup(layout.createSequentialGroup()
												.addComponent(buttonMarcaAll, javax.swing.GroupLayout.PREFERRED_SIZE,
														23, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(6, 6, 6)
												.addComponent(buttonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 28,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(buttonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 24,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(buttonMore, javax.swing.GroupLayout.PREFERRED_SIZE, 24,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(86, 86, 86).addComponent(txtStatus))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
												layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
														.addComponent(txtCountRows))))
						.addGroup(layout.createSequentialGroup().addComponent(buttonVoltar).addGap(0, 0,
								Short.MAX_VALUE)))
				.addContainerGap())
				.addGroup(layout.createSequentialGroup().addContainerGap(185, Short.MAX_VALUE).addGroup(layout
						.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup().addComponent(labelData)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 97,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(63, 63, 63))
										.addGroup(layout.createSequentialGroup().addComponent(labelReceptor)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtReceptor, javax.swing.GroupLayout.PREFERRED_SIZE, 136,
														javax.swing.GroupLayout.PREFERRED_SIZE)))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(buttonPesquisa))
						.addGroup(layout.createSequentialGroup().addComponent(labelTipo)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(37, 37, 37).addComponent(labelEspecifique)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 126,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup().addComponent(labelCod)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, 171,
										javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(116, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(labelCod)
						.addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(labelTipo)
						.addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addComponent(labelEspecifique).addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelReceptor).addComponent(txtReceptor, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(buttonPesquisa, javax.swing.GroupLayout.Alignment.TRAILING)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelData).addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(buttonMore, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonAdd, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonEdit, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonMarcaAll, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(txtCountRows)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(buttonVoltar)));

		Component[] components = getComponents();

		for (Component c : components) {
			if (c instanceof JTextField) {
				((JTextField) c).setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			} else if (c instanceof JButton) {

				if (((JButton) c).getText().trim().toLowerCase().equals("voltar") == false)
					((JButton) c).setBorder(null);
				((JButton) c).setEnabled(true);
				((JButton) c).setContentAreaFilled(false);
				((JButton) c).setFocusable(false);
			}
		}
		
		txtCountRows.setText(model.getDoacoes().size() + " Doa��es cadastradas");
		scrollPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		txtStatus.setBorder(null);
		txtTipo.setEnabled(false);
		txtTipo.setOpaque(false);
		buttonEdit.setVisible(false);
		FormSeeDoacoes(dbDoacao, frame, app);
	}// </editor-fold>

	private DoacaoController controllerDoacao;
	private List<Doacao> doacoes;
	private boolean selectAll;

	public void FormSeeDoacoes(Database dD, JFrame frame, App app) {
		selectAll = false;
		controllerDoacao1 = new DoacaoController(dD);
		try {
			doacoes = controllerDoacao1.readAll();
			if (doacoes.size() == 0) {
				this.getButtonMarcaAll().setEnabled(false);
				this.getButtonEdit().setEnabled(false);
				this.getButtonMore().setEnabled(false);
				throw new Exception("N�o h� doa��es cadastradas.");
			}
		} catch (Exception e) {
			this.getTxtStatus().setText(e.getMessage());
		}

		this.getButtonAdd().addActionListener(e -> initComponentsNew());
		this.getButtonAdd().setMnemonic(KeyEvent.VK_S);
		this.getButtonAdd().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					getButtonAdd().doClick();
				}
			}
		});

		this.getButtonPesquisa().addActionListener(e -> pesquisa_Click());
		this.getButtonPesquisa().setMnemonic(KeyEvent.VK_S);
		this.getButtonPesquisa().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					getButtonPesquisa().doClick();
				}
			}
		});

		this.getButtonMarcaAll().addActionListener(e -> marcaAll_Click());
		this.getButtonMarcaAll().setMnemonic(KeyEvent.VK_S);
		this.getButtonMarcaAll().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					getButtonMarcaAll().doClick();
				}
			}
		});

		// this.getButtonEdit().addActionListener(e -> edit_Click());
		this.getButtonEdit().setMnemonic(KeyEvent.VK_S);
		this.getButtonEdit().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					getButtonEdit().doClick();
				}
			}
		});

		this.getButtonMore().addActionListener(e -> more_Click());
		this.getButtonMore().setMnemonic(KeyEvent.VK_S);
		this.getButtonMore().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					getButtonMore().doClick();
				}
			}
		});

		this.getTable().addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				try {
					getTxtStatus().setText("");
					if (e.isMetaDown())
						if (getTable().getSelectedRow() != -1) {
							int row = getTable().rowAtPoint(e.getPoint());
							boolean select = false;
							for (int i = 0; i < getTable().getSelectedRows().length; i++) {
								if (getTable().getSelectedRows()[i] == row)
									select = true;
							}
							if (select) {
								List<Doacao> selecionados = new ArrayList<Doacao>();
								for (int i = 0; i < getTable().getSelectedRows().length; i++) {
									selecionados.add(getModelTable().getDoacoes().get(getTable().getSelectedRows()[i]));
								}
								new PanelMoreDoacao(frame, selecionados);
							}

						} else

							throw new Exception("Selecione ao menos um cadastro");

				} catch (Exception ex) {
					getTxtStatus().setText(ex.getMessage());
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});

		this.getTable().addKeyListener(new KeyAdapter() {
			@SuppressWarnings("deprecation")
			public void keyPressed(KeyEvent e) {
				if ((e.getKeyCode() == KeyEvent.VK_P) && (e.getModifiers() & KeyEvent.CTRL_MASK) != 0) {
					if (getTable().getSelectedRow() != -1) {
						List<Doacao> doacoes = new ArrayList<Doacao>();
						for (int i = 0; i < getTable().getSelectedRows().length; i++) {
							doacoes.add(getModelTable().getDoacoes().get(getTable().getSelectedRows()[i]));
						}
						new CreatePDF().criarDoacao(frame, doacoes);
					} else {
						getTxtStatus().setText("Selecione um cadastro.");
					}
				}
			}
		});

	}

	public void more_Click() {
		try {
			if (getTable().getSelectedRow() != -1) {
				List<Doacao> selecionados = new ArrayList<Doacao>();
				for (int i = 0; i < getTable().getSelectedRows().length; i++) {
					selecionados.add(getModelTable().getDoacoes().get(getTable().getSelectedRows()[i]));
				}
				new PanelMoreDoacao(frame, selecionados);

			} else

				throw new Exception("Selecione ao menos um cadastro");
		} catch (Exception e) {
			getTxtStatus().setText(e.getMessage());
		}
	}

	public void marcaAll_Click() {
		if (selectAll) {
			this.getTable().clearSelection();
			selectAll = false;
		} else {
			this.getTable().selectAll();
			selectAll = true;
		}

		this.getTable().requestFocus();
	}

	public void pesquisa_Click() {
		this.getTxtStatus().setText("");
		this.getTable().clearSelection();
		List<Doacao> doacoesPesq = new ArrayList<Doacao>();
		try {
			if (this.getTxtCod().getText().trim().isEmpty() == false) {
				for (Doacao doacao : doacoes) {
					if (doacao.getCodigo().equals(this.getTxtCod().getText())) {
						doacoesPesq.add(doacao);
						break;
					}
				}
				if (doacoesPesq.size() > 0) {
					this.getModelTable().setDoacoes(doacoesPesq);
					this.getTxtCountRows().setText(this.getTable().getRowCount() + " Doa��es cadastradas");
					throw new Exception("Doa��o encontrada.");
				} else {
					this.getModelTable().setDoacoes(doacoes);
					this.getTxtCountRows().setText(this.getTable().getRowCount() + " Doa��es cadastradas");
					throw new Exception("Doa��o n�o encontrada.");
				}
			} else {
				Doacao doacaoPesq = new Doacao();
				if (((String) this.getComboTipo().getSelectedItem()).equals("Outros")) {
					if (this.getTxtTipo().getText().trim().isEmpty() == false) {
						doacaoPesq.setTipo(this.getTxtTipo().getText());
						if (this.getTxtReceptor().getText().trim().isEmpty() == false) {
							doacaoPesq.setReceptor(this.getTxtReceptor().getText());

							String date = this.getTxtData().getText().substring(0, 2);
							date += this.getTxtData().getText().substring(3, 5);
							date += this.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(this.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						} else {
							String date = this.getTxtData().getText().substring(0, 2);
							date += this.getTxtData().getText().substring(3, 5);
							date += this.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(this.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						}
					} else {
						if (this.getTxtReceptor().getText().trim().isEmpty() == false) {
							doacaoPesq.setReceptor(this.getTxtReceptor().getText());

							String date = this.getTxtData().getText().substring(0, 2);
							date += this.getTxtData().getText().substring(3, 5);
							date += this.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(this.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						} else {
							String date = this.getTxtData().getText().substring(0, 2);
							date += this.getTxtData().getText().substring(3, 5);
							date += this.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(this.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						}
					}
				} else {
					doacaoPesq.setTipo((String) this.getComboTipo().getSelectedItem());
					if (this.getTxtReceptor().getText().trim().isEmpty() == false) {
						doacaoPesq.setReceptor(this.getTxtReceptor().getText());

						String date = this.getTxtData().getText().substring(0, 2);
						date += this.getTxtData().getText().substring(3, 5);
						date += this.getTxtData().getText().substring(6, 10);
						if (date.trim().isEmpty() == false) {
							checkDate();
							Calendar data = Calendar.getInstance();
							data.setTime(stringToDate(this.getTxtData().getText()));
							data.add(Calendar.MONTH, -1);
							doacaoPesq.setData(data.getTime());
						}
					} else {
						String date = this.getTxtData().getText().substring(0, 2);
						date += this.getTxtData().getText().substring(3, 5);
						date += this.getTxtData().getText().substring(6, 10);
						if (date.trim().isEmpty() == false) {
							checkDate();
							Calendar data = Calendar.getInstance();
							data.setTime(stringToDate(this.getTxtData().getText()));
							data.add(Calendar.MONTH, -1);
							doacaoPesq.setData(data.getTime());
						}
					}

				}
				for (Doacao d : doacoes) {
					if (d.getTipo().equals(doacaoPesq.getTipo())
							|| ((String) this.getComboTipo().getSelectedItem()).equals("Todos")) {
						if (doacaoPesq.getData() != null) {
							System.out.print(doacaoPesq.printData());
							System.out.print("\n" + d.printData() + "\n\n\n");

							if (d.printData().equals(doacaoPesq.printData())) {
								if (doacaoPesq.getReceptor() != null) {
									if (d.getReceptor().equals(doacaoPesq.getReceptor())) {
										doacoesPesq.add(d);
									}
								} else {
									doacoesPesq.add(d);
								}
							}
						} else {
							if (doacaoPesq.getReceptor() != null) {
								if (d.getReceptor().equals(doacaoPesq.getReceptor())) {
									doacoesPesq.add(d);
								}
							} else {
								doacoesPesq.add(d);
							}
						}

					}

				}
				if (doacoesPesq.size() > 0) {
					this.getModelTable().setDoacoes(doacoesPesq);
					this.getTxtCountRows().setText(this.getTable().getRowCount() + " Doa��escadastradas");
					clearTextFieldsSee();
					throw new Exception("Doa��o encontrada.");
				} else {
					this.getModelTable().setDoacoes(doacoes);
					this.getTxtCountRows().setText(this.getTable().getRowCount() + " Doa��es cadastradas");
					throw new Exception("Doa��o n�o encontrada.");
				}
			}

		} catch (Exception e) {
			this.getTxtStatus().setText(e.getMessage());
		}

	}

	public void clearTextFieldsSee() {
		Component components[] = this.getComponents();

		for (Component c : components) {
			if (c instanceof JTextField) {
				((JTextField) c).setText("");

			}
		}
	}

	public void checkDate() throws Exception {
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(stringToDate(this.getTxtData().getText()));

		try {
			endCalendar.add(Calendar.MONTH, -1);
			analiseDate(this.getTxtData().getText());
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}
	}

	public void analiseDate(String text) throws Exception {
		try {
			int dia = Integer.parseInt(text.substring(0, 2));
			int mes = Integer.parseInt(text.substring(3, 5));
			int ano = Integer.parseInt(text.substring(6, 10));

			int[] meses31 = { 4, 6, 9, 11 };

			if (1 > dia || dia > 31)
				throw new Exception("Dia inv�lido");

			if (1 > mes || mes > 12)
				throw new Exception("M�s inv�lido");
			if (dia == 31) {
				for (int i = 0; i < 4; i++) {
					if (mes == meses31[i]) {
						throw new Exception("Dia inv�lido");
					}
				}
			}
			if (mes == 2) {
				boolean ehBissexto = false;
				if ((ano % 400) == 0) {
					ehBissexto = true;
				} else {
					if (((ano % 4) == 0) && ((ano % 100) != 0)) {
						ehBissexto = true;
					}
				}

				if (ehBissexto) {
					if (dia > 29)
						throw new Exception("Dia inv�lido");
				} else {
					if (dia > 28)
						throw new Exception("Dia inv�lido");
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}

	}

	// Variables declaration - do not modify
	private javax.swing.JButton buttonClear;
	private javax.swing.JButton buttonSave;
	private javax.swing.JCheckBox checkSendTesouraria;
	private javax.swing.JComboBox<String> comboDoacao;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JLabel labeDesc;
	private javax.swing.JLabel labelCaracteres;
	private javax.swing.JLabel labelEspecifique;
	private javax.swing.JLabel labelData;
	private javax.swing.JLabel labelDoador;
	private javax.swing.JLabel labelPhoto;
	private javax.swing.JLabel labelQntd;
	private javax.swing.JLabel labelReceptor;
	private javax.swing.JLabel labelTipo;
	private javax.swing.JLabel labelTitulo;
	private javax.swing.JSpinner spinnerQntd;
	private javax.swing.JTextField txtData;
	private javax.swing.JTextArea txtDesc;
	private javax.swing.JTextField txtDoador;
	private javax.swing.JTextField txtOutros;
	private javax.swing.JTextField txtReceptor;
	private javax.swing.JTextField txtStatus;

	private javax.swing.JButton buttonAdd;
	private javax.swing.JButton buttonEdit;
	private javax.swing.JButton buttonMore;
	private javax.swing.JButton buttonPesquisa;
	private javax.swing.JButton buttonVoltar;
	private javax.swing.JButton buttonMarcaAll;
	private javax.swing.JComboBox<String> comboTipo;
	private javax.swing.JLabel labelCod;
	private javax.swing.JLabel txtCountRows;
	private javax.swing.JProgressBar progressBar;
	private javax.swing.JScrollPane scrollPane;
	private javax.swing.JTable table;
	private javax.swing.JTextField txtCod;
	private javax.swing.JTextField txtTipo;
	private DefaultTableCellRenderer cellRender;
	private DoacaoTableModel model;

	public javax.swing.JButton getButtonClear() {
		return buttonClear;
	}

	public void setButtonClear(javax.swing.JButton buttonClear) {
		this.buttonClear = buttonClear;
	}

	public javax.swing.JButton getButtonSave() {
		return buttonSave;
	}

	public void setButtonSave(javax.swing.JButton buttonSave) {
		this.buttonSave = buttonSave;
	}

	public javax.swing.JCheckBox getCheckSendTesouraria() {
		return checkSendTesouraria;
	}

	public void setCheckSendTesouraria(javax.swing.JCheckBox checkSendTesouraria) {
		this.checkSendTesouraria = checkSendTesouraria;
	}

	public javax.swing.JComboBox<String> getComboDoacao() {
		return comboDoacao;
	}

	public void setComboDoacao(javax.swing.JComboBox<String> comboDoacao) {
		this.comboDoacao = comboDoacao;
	}

	public javax.swing.JScrollPane getjScrollPane1() {
		return jScrollPane1;
	}

	public void setjScrollPane1(javax.swing.JScrollPane jScrollPane1) {
		this.jScrollPane1 = jScrollPane1;
	}

	public javax.swing.JLabel getLabeDesc() {
		return labeDesc;
	}

	public void setLabeDesc(javax.swing.JLabel labeDesc) {
		this.labeDesc = labeDesc;
	}

	public javax.swing.JLabel getLabelCaracteres() {
		return labelCaracteres;
	}

	public void setLabelCaracteres(javax.swing.JLabel labelCaracteres) {
		this.labelCaracteres = labelCaracteres;
	}

	public javax.swing.JLabel getLabelEspecifique() {
		return labelEspecifique;
	}

	public void setLabelEspecifique(javax.swing.JLabel labelEspecifique) {
		this.labelEspecifique = labelEspecifique;
	}

	public javax.swing.JLabel getLabelData() {
		return labelData;
	}

	public void setLabelData(javax.swing.JLabel labelData) {
		this.labelData = labelData;
	}

	public javax.swing.JLabel getLabelDoador() {
		return labelDoador;
	}

	public void setLabelDoador(javax.swing.JLabel labelDoador) {
		this.labelDoador = labelDoador;
	}

	public javax.swing.JLabel getLabelPhoto() {
		return labelPhoto;
	}

	public void setLabelPhoto(javax.swing.JLabel labelPhoto) {
		this.labelPhoto = labelPhoto;
	}

	public javax.swing.JLabel getLabelQntd() {
		return labelQntd;
	}

	public void setLabelQntd(javax.swing.JLabel labelQntd) {
		this.labelQntd = labelQntd;
	}

	public javax.swing.JLabel getLabelReceptor() {
		return labelReceptor;
	}

	public void setLabelReceptor(javax.swing.JLabel labelReceptor) {
		this.labelReceptor = labelReceptor;
	}

	public javax.swing.JLabel getLabelTipo() {
		return labelTipo;
	}

	public void setLabelTipo(javax.swing.JLabel labelTipo) {
		this.labelTipo = labelTipo;
	}

	public javax.swing.JLabel getLabelTitulo() {
		return labelTitulo;
	}

	public void setLabelTitulo(javax.swing.JLabel labelTitulo) {
		this.labelTitulo = labelTitulo;
	}

	public javax.swing.JSpinner getSpinnerQntd() {
		return spinnerQntd;
	}

	public void setSpinnerQntd(javax.swing.JSpinner spinnerQntd) {
		this.spinnerQntd = spinnerQntd;
	}

	public javax.swing.JTextField getTxtData() {
		return txtData;
	}

	public void setTxtData(javax.swing.JTextField txtData) {
		this.txtData = txtData;
	}

	public javax.swing.JTextArea getTxtDesc() {
		return txtDesc;
	}

	public void setTxtDesc(javax.swing.JTextArea txtDesc) {
		this.txtDesc = txtDesc;
	}

	public javax.swing.JTextField getTxtDoador() {
		return txtDoador;
	}

	public void setTxtDoador(javax.swing.JTextField txtDoador) {
		this.txtDoador = txtDoador;
	}

	public javax.swing.JTextField getTxtOutros() {
		return txtOutros;
	}

	public void setTxtOutros(javax.swing.JTextField txtOutros) {
		this.txtOutros = txtOutros;
	}

	public javax.swing.JTextField getTxtReceptor() {
		return txtReceptor;
	}

	public void setTxtReceptor(javax.swing.JTextField txtReceptor) {
		this.txtReceptor = txtReceptor;
	}

	public javax.swing.JTextField getTxtStatus() {
		return txtStatus;
	}

	public void setTxtStatus(javax.swing.JTextField txtStatus) {
		this.txtStatus = txtStatus;
	}

	public Database getdD() {
		return dbDoacao;
	}

	public void setdD(Database dD) {
		this.dbDoacao = dD;
	}

	public javax.swing.JButton getButtonNewDoacao() {
		return buttonNewDoacao;
	}

	public void setButtonNewDoacao(javax.swing.JButton buttonNewDoacao) {
		this.buttonNewDoacao = buttonNewDoacao;
	}

	public javax.swing.JButton getButtonSeeDoacao() {
		return buttonSeeDoacao;
	}

	public void setButtonSeeDoacao(javax.swing.JButton buttonSeeDoacao) {
		this.buttonSeeDoacao = buttonSeeDoacao;
	}

	public javax.swing.JButton getButtonAdd() {
		return buttonAdd;
	}

	public void setButtonAdd(javax.swing.JButton buttonAdd) {
		this.buttonAdd = buttonAdd;
	}

	public javax.swing.JButton getButtonEdit() {
		return buttonEdit;
	}

	public void setButtonEdit(javax.swing.JButton buttonEdit) {
		this.buttonEdit = buttonEdit;
	}

	public javax.swing.JButton getButtonMore() {
		return buttonMore;
	}

	public void setButtonMore(javax.swing.JButton buttonMore) {
		this.buttonMore = buttonMore;
	}

	public javax.swing.JButton getButtonPesquisa() {
		return buttonPesquisa;
	}

	public void setButtonPesquisa(javax.swing.JButton buttonPesquisa) {
		this.buttonPesquisa = buttonPesquisa;
	}

	public javax.swing.JButton getButtonVoltar() {
		return buttonVoltar;
	}

	public void setButtonVoltar(javax.swing.JButton buttonVoltar) {
		this.buttonVoltar = buttonVoltar;
	}

	public javax.swing.JButton getButtonMarcaAll() {
		return buttonMarcaAll;
	}

	public void setButtonMarcaAll(javax.swing.JButton buttonMarcaAll) {
		this.buttonMarcaAll = buttonMarcaAll;
	}

	public javax.swing.JComboBox<String> getComboTipo() {
		return comboTipo;
	}

	public void setComboTipo(javax.swing.JComboBox<String> comboTipo) {
		this.comboTipo = comboTipo;
	}

	public javax.swing.JLabel getLabelCod() {
		return labelCod;
	}

	public void setLabelCod(javax.swing.JLabel labelCod) {
		this.labelCod = labelCod;
	}

	public javax.swing.JLabel getTxtCountRows() {
		return txtCountRows;
	}

	public void setTxtCountRows(javax.swing.JLabel txtCountRows) {
		this.txtCountRows = txtCountRows;
	}

	public javax.swing.JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(javax.swing.JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public javax.swing.JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(javax.swing.JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public javax.swing.JTable getTable() {
		return table;
	}

	public void setTable(javax.swing.JTable table) {
		this.table = table;
	}

	public javax.swing.JTextField getTxtCod() {
		return txtCod;
	}

	public void setTxtCod(javax.swing.JTextField txtCod) {
		this.txtCod = txtCod;
	}

	public javax.swing.JTextField getTxtTipo() {
		return txtTipo;
	}

	public void setTxtTipo(javax.swing.JTextField txtTipo) {
		this.txtTipo = txtTipo;
	}

	public DefaultTableCellRenderer getCellRender() {
		return cellRender;
	}

	public void setCellRender(DefaultTableCellRenderer cellRender) {
		this.cellRender = cellRender;
	}

	public DoacaoTableModel getModelTable() {
		return model;
	}

	public void setModel(DoacaoTableModel model) {
		this.model = model;
	}

	public Database getDbDoacao() {
		return dbDoacao;
	}

	public void setDbDoacao(Database dbDoacao) {
		this.dbDoacao = dbDoacao;
	}

	public DoacaoController getControllerDoacao1() {
		return controllerDoacao1;
	}

	public void setControllerDoacao1(DoacaoController controllerDoacao1) {
		this.controllerDoacao1 = controllerDoacao1;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public DoacaoController getControllerDoacao() {
		return controllerDoacao;
	}

	public void setControllerDoacao(DoacaoController controllerDoacao) {
		this.controllerDoacao = controllerDoacao;
	}

	public List<Doacao> getDoacoes() {
		return doacoes;
	}

	public void setDoacoes(List<Doacao> doacoes) {
		this.doacoes = doacoes;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}

}