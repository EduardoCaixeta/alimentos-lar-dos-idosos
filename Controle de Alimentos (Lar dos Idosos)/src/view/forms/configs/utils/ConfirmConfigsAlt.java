package view.forms.configs.utils;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import controller.SettingsController;
import model.entity.Users;
import view.panels.PanelConfig;

@SuppressWarnings("serial")
public class ConfirmConfigsAlt extends JPanel {

	private JDialog optionPane;
	private JFrame bloq;
	private Users user;
	private SettingsController controllerSettings;
	private PanelConfig panel;

	public ConfirmConfigsAlt(JFrame frame, Users user, SettingsController controllerSettings, PanelConfig panel) {
		this.bloq = frame;
		this.user = user;
		this.controllerSettings = controllerSettings;
		this.panel = panel;
		initComponents();
		showFrame();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(255, 200, 200));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() {
		optionPane = new JDialog(bloq);
		optionPane.add(this);
		optionPane.setSize(287, 112);
		optionPane.setModal(true);
		optionPane.setUndecorated(true);
		optionPane.setLocationRelativeTo(bloq);
		optionPane.setResizable(false);
		optionPane.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		optionPane.setLocation(bloq.getX() + bloq.getWidth() / 2 - optionPane.getWidth() / 2,
				bloq.getY() + bloq.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}

	public void close() {
		optionPane.dispose();
	}

	@SuppressWarnings("deprecation")
	public void confirm_Click() {
		try {
			if (this.txtPassword.getText().isEmpty() == false) {
				this.getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
				if (this.getTxtPassword().getText().equals(user.getPassword())) {

					if ((int) panel.getSpinnerDays().getValue() != controllerSettings.getSettings()
							.getDiasAlertaVencimento()) {
						controllerSettings.getSettings()
								.setDiasAlertaVencimento((int) panel.getSpinnerDays().getValue());
						controllerSettings.update();
						panel.getTxtStatus().setText("Altera��o realizada com sucesso!");
						close();
					}
					if (panel.getCheckStartLogin().isSelected() != controllerSettings.getSettings().isStartLogin()) {
						controllerSettings.getSettings().setStartLogin(panel.getCheckStartLogin().isSelected());
						if (panel.getCheckStartLogin().isSelected()==false) {
							controllerSettings.getSettings().setUsername(user.getUsername());
							controllerSettings.getSettings().setPassword(user.getPassword());
						} else {
							controllerSettings.getSettings().setUsername("");
							controllerSettings.getSettings().setPassword("");
						}
						controllerSettings.update();
						close();
					}
				} else {
					this.getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					JOptionPane.showMessageDialog(optionPane, "Senha incorreta.");
					this.getTxtPassword().setText("");
				}
			} else {
				this.getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
			close();
		}
	}

	private void initComponents() {

		labelPassword = new javax.swing.JLabel();
		txtPassword = new javax.swing.JPasswordField();
		buttonConfirm = new javax.swing.JButton();
		buttonConfirm.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonVoltar = new javax.swing.JButton();
		buttonVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));

		labelPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelPassword.setText("Senha do usu�rio:");

		txtPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtPassword.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

		buttonConfirm.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonConfirm.setText("CONFIRMAR");
		buttonConfirm.setFocusable(false);
		buttonConfirm.addActionListener(e -> confirm_Click());

		buttonVoltar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonVoltar.setText("CANCELAR");
		buttonVoltar.setFocusable(false);
		buttonVoltar.addActionListener(e -> close());

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap(17, Short.MAX_VALUE)
						.addComponent(labelPassword).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 140,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(24, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonConfirm).addGap(18, 18, 18).addComponent(buttonVoltar)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addGap(28, 28, 28)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelPassword).addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(21, 21, 21).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(buttonConfirm).addComponent(buttonVoltar))
				.addContainerGap(20, Short.MAX_VALUE)));
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JButton buttonConfirm;
	private javax.swing.JButton buttonVoltar;
	private javax.swing.JLabel labelPassword;
	private javax.swing.JPasswordField txtPassword;

	// End of variables declaration
	public javax.swing.JButton getButtonConfirm() {
		return buttonConfirm;
	}

	public void setButtonConfirm(javax.swing.JButton buttonConfirm) {
		this.buttonConfirm = buttonConfirm;
	}

	public javax.swing.JButton getButtonVoltar() {
		return buttonVoltar;
	}

	public void setButtonVoltar(javax.swing.JButton buttonVoltar) {
		this.buttonVoltar = buttonVoltar;
	}

	public javax.swing.JLabel getLabelPassword() {
		return labelPassword;
	}

	public void setLabelPassword(javax.swing.JLabel labelPassword) {
		this.labelPassword = labelPassword;
	}

	public javax.swing.JPasswordField getTxtPassword() {
		return txtPassword;
	}

	public void setTxtPassword(javax.swing.JPasswordField txtPassword) {
		this.txtPassword = txtPassword;
	}
}
