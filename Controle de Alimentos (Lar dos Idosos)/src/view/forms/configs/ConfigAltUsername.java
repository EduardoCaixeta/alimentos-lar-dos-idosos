package view.forms.configs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.format.Limite_digitos;
import view.panels.utils.UpperCaseField_Limitado;

@SuppressWarnings("serial")
public class ConfigAltUsername extends JPanel {

	Users user;
	public ConfigAltUsername(JFrame frame, Users user, String nameDBU) throws Exception {
		this.user = user;
		this.frame = frame;
		controllerUser = new UsersController(new Database(nameDBU));
		initComponents();
		manipularComponents();
		showFrame();
		txtNewUser.requestFocus();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(210, 210, 210));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() throws Exception {
		optionPane = new JDialog(frame);
		optionPane.add(this);
		optionPane.setSize(340, 230);
		optionPane.setModal(true);
		optionPane.setResizable(false);
		optionPane.setLocation(frame.getX() + frame.getWidth() / 2 - optionPane.getWidth() / 2,
				frame.getY() + frame.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}

	public void close() {
		optionPane.dispose();
	}

	public void cancel_Click() {
		close();
	}

	public void initComponents() {
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});
		
		buttonCancel = new javax.swing.JButton();
		labelOldUser = new javax.swing.JLabel();
		labelNewUser = new javax.swing.JLabel();
		labelConfUser = new javax.swing.JLabel();
		buttonSave = new javax.swing.JButton();
		txtOldUser = new javax.swing.JTextField();
		txtNewUser = new UpperCaseField_Limitado(20, "[^a-zA-Z0-9_]",false,true);
		txtConfPass = new JPasswordField();

		txtNewUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtNewUser.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtNewUser.addActionListener(e -> {
			txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtNewUser.getText().isEmpty() == false)
				txtConfPass.requestFocus();
			else {
				txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});

		txtConfPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtConfPass.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtConfPass.setDocument(new Limite_digitos(15, ""));
		txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtConfPass.addActionListener(e -> {
			txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtConfPass.getText().isEmpty() == false)
				save_Click();
			else {
				txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});

		buttonCancel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonCancel.setText("CANCELAR");

		buttonCancel.addActionListener(e -> cancel_Click());
		buttonCancel.setMnemonic(KeyEvent.VK_S);
		buttonCancel.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonCancel.doClick();
				}
			}
		});

		labelOldUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelOldUser.setText("Nome de usu�rio atual:");

		labelNewUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelNewUser.setText("Novo nome de usu�rio:");

		labelConfUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelConfUser.setText("Senha:");

		buttonSave.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonSave.setText("SALVAR");

		buttonSave.addActionListener(e -> save_Click());
		buttonSave.setMnemonic(KeyEvent.VK_S);
		buttonSave.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonSave.doClick();
				}
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup().addComponent(labelConfUser).addGap(10, 10, 10)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtConfPass, javax.swing.GroupLayout.PREFERRED_SIZE, 156,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(layout
										.createSequentialGroup().addGap(47, 47, 47).addComponent(buttonSave)
										.addGap(42, 42, 42).addComponent(buttonCancel))
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
										.addGroup(javax.swing.GroupLayout.Alignment.LEADING,
												layout.createSequentialGroup().addComponent(labelNewUser)
														.addGap(10, 10, 10)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(txtNewUser))
										.addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout
												.createSequentialGroup().addComponent(labelOldUser).addGap(10, 10, 10)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtOldUser, javax.swing.GroupLayout.PREFERRED_SIZE, 167,
														javax.swing.GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap(21, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelOldUser).addComponent(txtOldUser, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelNewUser).addComponent(txtNewUser, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelConfUser).addComponent(txtConfPass, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(buttonSave).addComponent(buttonCancel))
				.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		
		txtOldUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtOldUser.setEditable(false);
		txtOldUser.setFocusable(false);
		txtOldUser.setOpaque(false);
		txtOldUser.setText(user.getUsername());
		txtOldUser.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtOldUser.setBorder(null);
	}// </editor-fold>

	public void manipularComponents() {
		Component components[] = getComponents();

		for (Component c : components) {
			if (c instanceof JButton) {
				((JButton) c).setCursor(new Cursor(Cursor.HAND_CURSOR));
				((JButton) c).setFocusCycleRoot(true);
				((JButton) c).setFocusPainted(false);
			}
			if (c instanceof JTextField) {
			((JTextField) c).addKeyListener(new KeyAdapter() {
    			public void keyPressed(KeyEvent e) {
    				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
    					buttonCancel.doClick();
    				}
    			}
    		});
		}
			}
	}

	public void save_Click() {
		try {
			txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtNewUser.getText().isEmpty() == false) {
				if (txtNewUser.getText().length() >= 4 && txtNewUser.getText().length() <= 15) {
					if (txtNewUser.getText().equals(txtOldUser.getText()) == false) {
						if (txtConfPass.getText().isEmpty() == false) {
							if (txtConfPass.getText().equals(user.getPassword())) {
								if (controllerUser.existUsername(txtNewUser.getText()) == false) {
									user.setUsername(txtNewUser.getText());
									controllerUser.update(user);
									JOptionPane.showMessageDialog(optionPane, "Nome de Usu�rio Alterado Com Sucesso!",
											"CONFIGURA��ES", JOptionPane.INFORMATION_MESSAGE);
									close();
								} else {
									txtNewUser.setText("");
									txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
									txtNewUser.requestFocus();
									throw new Exception("Nome de usu�rio indispon�vel.");
								}
							} else {
								txtConfPass.setText("");
								txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
								txtConfPass.requestFocus();
								throw new Exception("Senha incorreta.");
							}
						} else {
							txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							txtConfPass.requestFocus();
							throw new Exception("~");
						}
					} else {
						txtNewUser.setText("");
						txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
						txtNewUser.requestFocus();
						throw new Exception("O novo nome de usu�rio n�o pode ser igual ao anterior.");
					}
				} else {
					txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					txtNewUser.requestFocus();
					throw new Exception("O nome de usu�rio deve conter entre 4 e 15 caracteres.");
				}
			} else {
				txtNewUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				txtNewUser.requestFocus();
				throw new Exception("~");
			}
		} catch (Exception e) {
			if (e.getMessage().equals("~") == false)
				JOptionPane.showMessageDialog(optionPane, e.getMessage().toUpperCase(), "Algo deu errado",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private javax.swing.JButton buttonCancel;
	private javax.swing.JButton buttonSave;
	private javax.swing.JLabel labelConfUser;
	private javax.swing.JLabel labelNewUser;
	private javax.swing.JLabel labelOldUser;
	private javax.swing.JTextField txtConfPass;
	private javax.swing.JTextField txtNewUser;
	private javax.swing.JTextField txtOldUser;
	private JFrame frame;
	private JDialog optionPane;
	private UsersController controllerUser;
	// End of variables declaration
}