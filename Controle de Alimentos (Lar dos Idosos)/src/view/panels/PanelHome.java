package view.panels;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class PanelHome extends javax.swing.JPanel {

	JFrame frame;
    public PanelHome(JFrame frame) {
    	this.frame = frame;
        initComponents();
    }

    @Override
	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon(new File("").getCanonicalPath()+ "\\images\\FundoHome.png").getImage(), this.getX(), this.getY(), this.getWidth(), this.getHeight()	,this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    public void sobre_Click()
    {
    	new PanelSobre(frame);
    }
    
    public void inicioState()
    {
    	initComponents();
    }
    
	private void initComponents() {
		this.removeAll();
	        buttonSobre = new javax.swing.JButton()
	        		{
	        	@Override
	            protected void paintComponent(Graphics g) {
	        		g.setColor(Color.CYAN);
	        		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	        		super.paintComponent(g);
	        		setContentAreaFilled(false);
	                setFocusPainted(false);
	        		repaint();


	        	}
	        		};
	        txtTitulo = new javax.swing.JTextField();

	        buttonSobre.setBackground(new java.awt.Color(255, 255, 102));
	        buttonSobre.setText("Sobre");
	        buttonSobre.setCursor(new Cursor(Cursor.HAND_CURSOR));
	        buttonSobre.addActionListener(e -> sobre_Click());

	        txtTitulo.setEditable(false);
	        txtTitulo.setBackground(new java.awt.Color(255, 255, 0));
	        txtTitulo.setFont(new java.awt.Font("Comic Sans MS", 0, 56)); // NOI18N
	        txtTitulo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
	        txtTitulo.setText("FOOD CONTROL");
	        txtTitulo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
	        txtTitulo.setFocusable(false);

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
	        this.setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(77, Short.MAX_VALUE)
	                .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(57, Short.MAX_VALUE))
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(buttonSobre)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(132, Short.MAX_VALUE)
	                .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(143, 143, 143)
	                .addComponent(buttonSobre)
	                .addContainerGap(89, Short.MAX_VALUE))
	        );
	       
	    }// </editor-fold>                        


	    // Variables declaration - do not modify                     
	    private javax.swing.JButton buttonSobre;
	    private javax.swing.JTextField txtTitulo;
	    // End of variables declaration                   
	}