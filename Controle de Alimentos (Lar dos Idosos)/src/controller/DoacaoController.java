package controller;

import java.util.List;

import model.Database;
import model.DoacaoDao;
import model.entity.Doacao;

public class DoacaoController {
	private DoacaoDao dao;
	private Database db;

	public DoacaoController(Database database) {
		this.dao = new DoacaoDao(database);
		this.db = database;
	}

	public Database getDatabase() {
		return db;
	}

	public void create(Doacao doacao) throws Exception {
		dao.create(doacao);
	}

	public List<Doacao> readAll() throws Exception {
		return dao.readAll();
	}
}
