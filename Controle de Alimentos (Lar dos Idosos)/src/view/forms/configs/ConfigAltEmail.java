package view.forms.configs;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.format.Limite_digitos;

@SuppressWarnings("serial")
public class ConfigAltEmail extends javax.swing.JPanel {                   
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonSave;
    private javax.swing.JLabel labelNewEmail;
    private javax.swing.JLabel labelOldEmail;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JTextField txtNewEmail;
    private javax.swing.JTextField txtOldEmail;
    private javax.swing.JPasswordField txtConfPass;
	private UsersController controllerUser;
	private JFrame frame;
	private JDialog optionPane;
	
	Users user;
	public ConfigAltEmail(JFrame frame, Users user, String nameDBU) throws Exception {
		this.user = user;
		this.frame = frame;
		controllerUser = new UsersController(new Database(nameDBU));
		initComponents();
		showFrame();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(210, 210, 210));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() throws Exception {
		optionPane = new JDialog(frame);
		optionPane.add(this);
		optionPane.setSize(340, 230);
		optionPane.setModal(true);
		optionPane.setResizable(false);
		optionPane.setLocation(frame.getX() + frame.getWidth() / 2 - optionPane.getWidth() / 2,
				frame.getY() + frame.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}

	public void close() {
		optionPane.dispose();
	}

	public void cancel_Click() {
		close();
	}
    
	public boolean isValiidadEmail(String email) {

		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}
	
    @SuppressWarnings("deprecation")
	public void save_Click()
    {
    	try
    	{
    		String emailUP = "";
			for (int i = 0; i < txtNewEmail.getText().length(); i++) {
				try {
					emailUP += Character.toUpperCase(txtNewEmail.getText().charAt(i));
				} catch (Exception e) {
					emailUP += txtNewEmail.getText().charAt(i);
				}
			}
			txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtNewEmail.getText().isEmpty() == false) {
				if (txtOldEmail.getText().equals(emailUP) == false) {
					if (controllerUser.existEmail(txtNewEmail.getText())==false) {
						if (isValiidadEmail(txtNewEmail.getText())) {
							if (txtConfPass.getText().equals(user.getPassword())) {
									user.setEmail(txtNewEmail.getText());
									controllerUser.update(user);
									JOptionPane.showMessageDialog(optionPane, "Email Alterado Com Sucesso!",
											"CONFIGURA��ES", JOptionPane.INFORMATION_MESSAGE);
									close();
							
							} else {
								txtConfPass.setText("");
								txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
								txtConfPass.requestFocus();
								throw new Exception("Senha incorreta.");
							}
						} else {
							txtNewEmail.setText("");
							txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							txtNewEmail.requestFocus();
							throw new Exception("Email inv�lido.");
						}
					} else {
						txtNewEmail.setText("");
						txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
						txtNewEmail.requestFocus();
						throw new Exception("Este email j� est� cadastrado em outro cadastro.");
					}
				} else {
					txtNewEmail.setText("");
					txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					txtNewEmail.requestFocus();
					throw new Exception("Seu novo email n�o pode ser igual ao atual.");
				}
			} else {
				txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				txtNewEmail.requestFocus();
				throw new Exception("~");
			}
		} catch (Exception e) {
			if (e.getMessage().equals("~") == false)
				JOptionPane.showMessageDialog(optionPane, e.getMessage().toUpperCase(), "Algo deu errado",
						JOptionPane.INFORMATION_MESSAGE);
		}
    }
 
    
    @SuppressWarnings("deprecation")
	private void initComponents() {
    	this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});
    	
        labelOldEmail = new javax.swing.JLabel();
        txtOldEmail = new javax.swing.JTextField();
        labelNewEmail = new javax.swing.JLabel();
        txtNewEmail = new javax.swing.JTextField();
        labelPassword = new javax.swing.JLabel();
        buttonSave = new javax.swing.JButton();
        buttonCancel = new javax.swing.JButton();
        txtConfPass = new javax.swing.JPasswordField();

        txtOldEmail.setFont(new java.awt.Font("Arial", 0, 12));
        
        txtNewEmail.setFont(new java.awt.Font("Arial", 0, 12));
        txtNewEmail.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtNewEmail.addActionListener(e -> {
			txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtNewEmail.getText().isEmpty() == false)
				txtConfPass.requestFocus();
			else {
				txtNewEmail.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});
		txtNewEmail.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});
        
        txtConfPass.setFont(new java.awt.Font("Arial", 0, 12));
        txtConfPass.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        txtConfPass.setDocument(new Limite_digitos(15, ""));
		txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtConfPass.addActionListener(e -> {
			txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtConfPass.getText().isEmpty() == false)
				save_Click();
			else {
				txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});
		txtConfPass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});
        
        labelOldEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelOldEmail.setText("Email atual:");

        labelNewEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelNewEmail.setText("Novo email:");

        labelPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelPassword.setText("Senha:");

        buttonSave.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonSave.setText("SALVAR");	
        buttonSave.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonSave.setFocusCycleRoot(true);
		buttonSave.setFocusPainted(false);
		buttonSave.addActionListener(e -> save_Click());
		buttonSave.setMnemonic(KeyEvent.VK_S);
		buttonSave.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonSave.doClick();
				}
			}
		});

        buttonCancel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonCancel.setText("CANCELAR");buttonCancel.addActionListener(e -> cancel_Click());
		buttonCancel.setMnemonic(KeyEvent.VK_S);
		buttonCancel.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonCancel.doClick();
				}
			}
		});
		
		txtOldEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtOldEmail.setEditable(false);
		txtOldEmail.setOpaque(false);
		txtOldEmail.setFocusable(false);
		txtOldEmail.setText(user.getEmail().toLowerCase());
		txtOldEmail.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtOldEmail.setBorder(null);
		
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(labelOldEmail)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtOldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(labelNewEmail)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtNewEmail)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelPassword)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtConfPass, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(buttonSave)
                        .addGap(43, 43, 43)
                        .addComponent(buttonCancel)))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelOldEmail)
                    .addComponent(txtOldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNewEmail)
                    .addComponent(txtNewEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPassword)
                    .addComponent(txtConfPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSave)
                    .addComponent(buttonCancel))
                .addContainerGap(13, Short.MAX_VALUE))
        );
    }// </editor-fold>                        

    // End of variables declaration                   
}
