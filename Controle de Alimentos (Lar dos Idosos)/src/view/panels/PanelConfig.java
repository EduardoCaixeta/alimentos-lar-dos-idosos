package view.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JViewport;

@SuppressWarnings("serial")
public class PanelConfig extends javax.swing.JPanel {

	/**
	 * Creates new form PanelConfig
	 */
	public PanelConfig() {
		labelTitulo = new javax.swing.JLabel();
		scrollPane = new javax.swing.JScrollPane();
		panelScroll = new javax.swing.JPanel();
		panelBackup = new javax.swing.JPanel();
		txtQntDoacoes = new javax.swing.JTextField();
		buttonPDFProd = new javax.swing.JButton();
		buttonPDFVali = new javax.swing.JButton();
		labelBackup = new javax.swing.JLabel();
		buttonPDFDonations = new javax.swing.JButton();
		buttonBackup = new javax.swing.JButton();
		labelQntProd = new javax.swing.JLabel();
		txtQntProd = new javax.swing.JTextField();
		labelQntVali = new javax.swing.JLabel();
		txtQntVali = new javax.swing.JTextField();
		labelQntDoacoes = new javax.swing.JLabel();
		panelInfo = new javax.swing.JPanel();
		panelInfo.setBackground(new Color(0, 0, 0, 64));
		buttonAltEmail = new javax.swing.JButton();
		buttonAltPass = new javax.swing.JButton();
		labelInfoConta = new javax.swing.JLabel();
		buttonAltUsername = new javax.swing.JButton();
		buttonDeslogar = new javax.swing.JButton();
		panelAdicionais = new javax.swing.JPanel();
		;
		labelAdicionais = new javax.swing.JLabel();
		labelQntDays = new javax.swing.JLabel();
		spinnerDays = new javax.swing.JSpinner();
		checkStartLogin = new javax.swing.JCheckBox();
		checkSendEmailTesouraria = new javax.swing.JCheckBox();
		buttonSave = new javax.swing.JButton();
		txtStatus = new javax.swing.JTextField();
		labelDays = new javax.swing.JLabel();
		panelDesenvolver = new javax.swing.JPanel();
		labelDesenvolvedor = new javax.swing.JLabel();
		labelNameEmpresa = new javax.swing.JLabel();
		labelEmailTesouraria = new javax.swing.JLabel();
		labelEmailBackup = new javax.swing.JLabel();
		labelEmailPrograma = new javax.swing.JLabel();
		txtNameEmpresa = new javax.swing.JTextField();
		txtEmailTesouraria = new javax.swing.JTextField();
		txtEmailBackup = new javax.swing.JTextField();
		txtEmailPrograma = new javax.swing.JTextField();
		buttonDesenvolver = new javax.swing.JButton();
		separator2 = new javax.swing.JSeparator();
		separator1 = new javax.swing.JSeparator();
		separator3 = new javax.swing.JSeparator();
		separator4 = new javax.swing.JSeparator();
		initComponents();
		manipularComponents();
	}

	@Override
	public void paintComponent(Graphics g) {
			try {
				g.drawImage(new ImageIcon(new File("").getCanonicalPath()+"\\images\\FundoConfig.png").getImage(), this.getX(), this.getY(),
						this.getWidth(), this.getHeight(), this);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	
	public void initComponents() {
		this.removeAll();

		labelTitulo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
		labelTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelTitulo.setText("CONFIGURA��ES");

		scrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);

		panelScroll.setOpaque(false);
		panelScroll.setBorder(null);
		panelBackup.setOpaque(false);
		panelBackup.setBorder(null);

		txtQntDoacoes.setEditable(false);
		txtQntDoacoes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtQntDoacoes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		txtQntDoacoes.setFocusable(false);

		buttonPDFProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonPDFProd.setText("Gerar PDF - Produtos");
		buttonPDFProd.setFocusable(false);

		buttonPDFVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonPDFVali.setText("Gerar PDF - Validades");
		buttonPDFVali.setFocusable(false);

		labelBackup.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
		labelBackup.setText("BACKUP");

		buttonPDFDonations.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonPDFDonations.setText("Gerar PDF - Doa��es");
		buttonPDFDonations.setFocusable(false);

		buttonBackup.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonBackup.setText("Fazer Backup");
		buttonBackup.setFocusable(false);

		buttonDeslogar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonDeslogar.setText("Deslogar");
		buttonDeslogar.setFocusable(false);

		labelQntProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelQntProd.setText("Produtos Cadastrados:");

		txtQntProd.setEditable(false);
		txtQntProd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtQntProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		txtQntProd.setFocusable(false);

		labelQntVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelQntVali.setText("Validades Cadastrados:");

		txtQntVali.setEditable(false);
		txtQntVali.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtQntVali.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		txtQntVali.setFocusable(false);

		labelQntDoacoes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelQntDoacoes.setText("   Doa��es Registradas:");

		javax.swing.GroupLayout panelBackupLayout = new javax.swing.GroupLayout(panelBackup);
		panelBackup.setLayout(panelBackupLayout);
		panelBackupLayout.setHorizontalGroup(panelBackupLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panelBackupLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(panelBackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(panelBackupLayout.createSequentialGroup().addComponent(labelQntDoacoes)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtQntDoacoes, javax.swing.GroupLayout.PREFERRED_SIZE, 103,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(panelBackupLayout.createSequentialGroup()
										.addGroup(panelBackupLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(labelQntProd).addComponent(labelQntVali))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(panelBackupLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(txtQntVali, javax.swing.GroupLayout.PREFERRED_SIZE, 104,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(txtQntProd, javax.swing.GroupLayout.PREFERRED_SIZE, 104,
														javax.swing.GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						panelBackupLayout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
								.addComponent(labelBackup)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBackupLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(panelBackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(buttonPDFVali, javax.swing.GroupLayout.PREFERRED_SIZE, 155,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonPDFProd, javax.swing.GroupLayout.PREFERRED_SIZE, 155,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonPDFDonations, javax.swing.GroupLayout.PREFERRED_SIZE, 155,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(panelBackupLayout.createSequentialGroup().addGap(20, 20, 20)
										.addComponent(buttonBackup)))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panelBackupLayout.setVerticalGroup(panelBackupLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panelBackupLayout.createSequentialGroup().addContainerGap().addComponent(labelBackup)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(panelBackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelQntProd)
								.addComponent(txtQntProd, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(panelBackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtQntVali, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(labelQntVali))
						.addGap(18, 18, 18)
						.addGroup(panelBackupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelQntDoacoes).addComponent(txtQntDoacoes,
										javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18).addComponent(buttonPDFProd).addGap(18, 18, 18).addComponent(buttonPDFVali)
						.addGap(18, 18, 18).addComponent(buttonPDFDonations).addGap(18, 18, 18)
						.addComponent(buttonBackup)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		panelInfo.setOpaque(false);

		buttonAltEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonAltEmail.setText("Alterar Email");
		buttonAltEmail.setFocusable(false);

		buttonAltPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonAltPass.setText("Alterar Senha");
		buttonAltPass.setFocusable(false);

		labelInfoConta.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
		labelInfoConta.setText("INFORMA��ES DA CONTA");
		labelInfoConta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

		buttonAltUsername.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonAltUsername.setText("Alterar Usu�rio");
		buttonAltUsername.setFocusable(false);

		javax.swing.GroupLayout panelInfoLayout = new javax.swing.GroupLayout(panelInfo);
		panelInfo.setLayout(panelInfoLayout);
		panelInfoLayout.setHorizontalGroup(panelInfoLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panelInfoLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(panelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(labelInfoConta)
								.addGroup(panelInfoLayout.createSequentialGroup().addGap(24, 24, 24)
										.addGroup(panelInfoLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(buttonAltUsername,
														javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(buttonAltPass, javax.swing.GroupLayout.PREFERRED_SIZE,
														115, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(buttonAltEmail, javax.swing.GroupLayout.PREFERRED_SIZE,
														115, javax.swing.GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						panelInfoLayout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonDeslogar)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panelInfoLayout.setVerticalGroup(panelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						panelInfoLayout.createSequentialGroup()
								.addComponent(labelInfoConta, javax.swing.GroupLayout.PREFERRED_SIZE, 29,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(buttonAltUsername).addGap(18, 18, 18).addComponent(buttonAltPass)
								.addGap(18, 18, 18).addComponent(buttonAltEmail).addGap(18, 18, 18)
								.addComponent(buttonDeslogar)));

		panelAdicionais.setOpaque(false);

		labelAdicionais.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
		labelAdicionais.setText("ADICIONAIS");

		labelQntDays.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelQntDays.setText("Acusar vencimento dentro de:");

		spinnerDays.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		spinnerDays.setFocusable(false);

		checkStartLogin.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		checkStartLogin.setText("Iniciar em tela Login");
		checkStartLogin.setFocusable(false);
		checkStartLogin.setOpaque(false);

		checkSendEmailTesouraria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		checkSendEmailTesouraria.setText("Enviar email � tesouraria a cada doa��o");
		checkSendEmailTesouraria.setFocusable(false);
		checkSendEmailTesouraria.setOpaque(false);

		buttonSave.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonSave.setText("Salvar");
		buttonSave.setFocusable(false);

		txtStatus.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtStatus.setBorder(null);
		txtStatus.setFocusable(false);
		txtStatus.setOpaque(false);

		labelDays.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelDays.setText("dias");

		javax.swing.GroupLayout panelAdicionaisLayout = new javax.swing.GroupLayout(panelAdicionais);
		panelAdicionais.setLayout(panelAdicionaisLayout);
		panelAdicionaisLayout.setHorizontalGroup(panelAdicionaisLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panelAdicionaisLayout.createSequentialGroup()
						.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 320,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(panelAdicionaisLayout.createSequentialGroup().addContainerGap(101, Short.MAX_VALUE)
						.addComponent(checkStartLogin).addGap(0, 100, Short.MAX_VALUE))
				.addGroup(panelAdicionaisLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(panelAdicionaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(panelAdicionaisLayout.createSequentialGroup().addComponent(labelQntDays)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(spinnerDays, javax.swing.GroupLayout.PREFERRED_SIZE, 56,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(labelDays))
								.addComponent(checkSendEmailTesouraria, javax.swing.GroupLayout.PREFERRED_SIZE, 247,
										javax.swing.GroupLayout.PREFERRED_SIZE))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(panelAdicionaisLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(buttonSave)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(panelAdicionaisLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(labelAdicionais)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panelAdicionaisLayout.setVerticalGroup(panelAdicionaisLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panelAdicionaisLayout.createSequentialGroup().addContainerGap().addComponent(labelAdicionais)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addGroup(panelAdicionaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelQntDays)
								.addComponent(spinnerDays, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(labelDays))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(checkSendEmailTesouraria).addGap(8, 8, 8).addComponent(checkStartLogin)
						.addGap(18, 18, 18).addComponent(buttonSave)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(0, 0, Short.MAX_VALUE)));

		panelDesenvolver.setOpaque(false);

		labelDesenvolvedor.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
		labelDesenvolvedor.setText("DESENVOLVEDOR");

		labelNameEmpresa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelNameEmpresa.setText("Nome da empresa:");

		labelEmailTesouraria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEmailTesouraria.setText("Email da tesouraria:");

		labelEmailBackup.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEmailBackup.setText("Email backup:");

		labelEmailPrograma.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEmailPrograma.setText("Email do programa:");

		txtNameEmpresa.setEditable(false);
		txtNameEmpresa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtNameEmpresa.setFocusable(false);

		txtEmailTesouraria.setEditable(false);
		txtEmailTesouraria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtEmailTesouraria.setFocusable(false);

		txtEmailBackup.setEditable(false);
		txtEmailBackup.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtEmailBackup.setFocusable(false);

		txtEmailPrograma.setEditable(false);
		txtEmailPrograma.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtEmailPrograma.setFocusable(false);

		buttonDesenvolver.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonDesenvolver.setText("Atalhos do Desenvolvedor");
		buttonDesenvolver.setFocusable(false);

		javax.swing.GroupLayout panelDesenvolverLayout = new javax.swing.GroupLayout(panelDesenvolver);
		panelDesenvolver.setLayout(panelDesenvolverLayout);
		panelDesenvolverLayout.setHorizontalGroup(panelDesenvolverLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						panelDesenvolverLayout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
								.addComponent(labelDesenvolvedor).addGap(0, 0, Short.MAX_VALUE))
				.addGroup(panelDesenvolverLayout.createSequentialGroup().addContainerGap(13, Short.MAX_VALUE).addGroup(
						panelDesenvolverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								panelDesenvolverLayout.createSequentialGroup().addComponent(labelEmailTesouraria)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtEmailTesouraria, javax.swing.GroupLayout.PREFERRED_SIZE, 196,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										panelDesenvolverLayout.createSequentialGroup().addComponent(labelEmailBackup)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtEmailBackup, javax.swing.GroupLayout.PREFERRED_SIZE,
														216, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										panelDesenvolverLayout.createSequentialGroup().addComponent(labelEmailPrograma)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtEmailPrograma, javax.swing.GroupLayout.PREFERRED_SIZE,
														205, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										panelDesenvolverLayout.createSequentialGroup().addComponent(labelNameEmpresa)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtNameEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE,
														165, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(11, 11, 11)))
						.addContainerGap(14, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						panelDesenvolverLayout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonDesenvolver)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panelDesenvolverLayout.setVerticalGroup(panelDesenvolverLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(panelDesenvolverLayout.createSequentialGroup().addContainerGap()
						.addComponent(labelDesenvolvedor).addGap(21, 21, 21)
						.addGroup(panelDesenvolverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtNameEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(labelNameEmpresa))
						.addGap(18, 18, 18)
						.addGroup(panelDesenvolverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtEmailTesouraria, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(labelEmailTesouraria))
						.addGap(18, 18, 18)
						.addGroup(panelDesenvolverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtEmailBackup, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(labelEmailBackup))
						.addGap(18, 18, 18)
						.addGroup(panelDesenvolverLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(txtEmailPrograma, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(labelEmailPrograma))
						.addGap(18, 18, 18).addComponent(buttonDesenvolver)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		javax.swing.GroupLayout panelScrollLayout = new javax.swing.GroupLayout(panelScroll);
		panelScroll.setLayout(panelScrollLayout);
		panelScrollLayout.setHorizontalGroup(panelScrollLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(separator1)
				.addComponent(separator4).addComponent(separator3, javax.swing.GroupLayout.Alignment.TRAILING)
				.addComponent(separator2)
				.addGroup(panelScrollLayout.createSequentialGroup().addContainerGap(164, Short.MAX_VALUE)
						.addGroup(panelScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(panelDesenvolver, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(panelAdicionais, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(panelScrollLayout.createSequentialGroup().addGap(39, 39, 39).addComponent(
										panelBackup, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(163, Short.MAX_VALUE))
				.addGroup(panelScrollLayout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panelInfo, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		panelScrollLayout
				.setVerticalGroup(panelScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(panelScrollLayout.createSequentialGroup()
								.addComponent(separator1, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(panelInfo, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(separator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(panelBackup, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(separator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(panelAdicionais, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(separator4, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(panelDesenvolver, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(15, 15, 15)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(labelTitulo)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(21, 21, 21).addComponent(labelTitulo).addGap(24, 24, 24)
						.addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)));

		JViewport viewport = new JViewport();
		panelScroll.setOpaque(false);
		viewport.setView(panelScroll);
		
		viewport.setOpaque(false);

		scrollPane.setViewportView(viewport);
		scrollPane.getViewport().setOpaque(false);
		scrollPane.setOpaque(false);
		for(Component c: scrollPane.getComponents())
		{
			if(c instanceof JPanel)
			{
				((JPanel)c).setOpaque(false);
			}
		}
		
		labelAdicionais.setForeground(Color.WHITE);
		labelBackup.setForeground(Color.WHITE);
		labelDays.setForeground(Color.WHITE);
		labelDesenvolvedor.setForeground(Color.WHITE);
		labelEmailBackup.setForeground(Color.WHITE);
		labelEmailPrograma.setForeground(Color.WHITE);
		labelEmailTesouraria.setForeground(Color.WHITE);
		labelInfoConta.setForeground(Color.WHITE);
		labelNameEmpresa.setForeground(Color.WHITE);
		labelQntDays.setForeground(Color.WHITE);
		labelQntDoacoes.setForeground(Color.WHITE);
		labelQntProd.setForeground(Color.WHITE);
		labelQntVali.setForeground(Color.WHITE);
		checkSendEmailTesouraria.setForeground(Color.WHITE);
		checkStartLogin.setForeground(Color.WHITE);
		txtStatus.setForeground(Color.WHITE);
		separator1.setForeground(Color.BLUE);
		separator2.setForeground(Color.BLUE);
		separator3.setForeground(Color.BLUE);
		separator4.setForeground(Color.BLUE);
		labelTitulo.setForeground(Color.WHITE);
		scrollPane.setBorder(null);
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JButton buttonAltEmail;
	private javax.swing.JButton buttonAltPass;
	private javax.swing.JButton buttonAltUsername;
	private javax.swing.JButton buttonBackup;
	private javax.swing.JButton buttonDesenvolver;
	private javax.swing.JButton buttonPDFDonations;
	private javax.swing.JButton buttonPDFProd;
	private javax.swing.JButton buttonPDFVali;
	private javax.swing.JButton buttonSave;
	private javax.swing.JButton buttonDeslogar;
	private javax.swing.JCheckBox checkSendEmailTesouraria;
	private javax.swing.JCheckBox checkStartLogin;
	private javax.swing.JLabel labelQntVali;
	private javax.swing.JTextField txtStatus;
	private javax.swing.JLabel labelAdicionais;
	private javax.swing.JLabel labelBackup;
	private javax.swing.JLabel labelDays;
	private javax.swing.JLabel labelDesenvolvedor;
	private javax.swing.JLabel labelEmailBackup;
	private javax.swing.JLabel labelEmailPrograma;
	private javax.swing.JLabel labelEmailTesouraria;
	private javax.swing.JLabel labelInfoConta;
	private javax.swing.JLabel labelNameEmpresa;
	private javax.swing.JLabel labelQntDays;
	private javax.swing.JLabel labelQntDoacoes;
	private javax.swing.JLabel labelQntProd;
	private javax.swing.JLabel labelTitulo;
	private javax.swing.JPanel panelAdicionais;
	private javax.swing.JPanel panelBackup;
	private javax.swing.JPanel panelDesenvolver;
	private javax.swing.JPanel panelInfo;
	private javax.swing.JPanel panelScroll;
	private javax.swing.JScrollPane scrollPane;
	private javax.swing.JSeparator separator1;
	private javax.swing.JSeparator separator2;
	private javax.swing.JSeparator separator3;
	private javax.swing.JSeparator separator4;
	private javax.swing.JSpinner spinnerDays;
	private javax.swing.JTextField txtEmailBackup;
	private javax.swing.JTextField txtEmailPrograma;
	private javax.swing.JTextField txtEmailTesouraria;
	private javax.swing.JTextField txtNameEmpresa;
	private javax.swing.JTextField txtQntDoacoes;
	private javax.swing.JTextField txtQntProd;
	private javax.swing.JTextField txtQntVali;

	public void manipularComponents() {
		Component components[] = this.getComponents();

		for (Component c : components) {
			if (c instanceof JButton) {
				((JButton) c).setCursor(new Cursor(Cursor.HAND_CURSOR));
				((JButton) c).setFocusCycleRoot(true);
				((JButton) c).setFocusPainted(false);
				;

			}
		}
	}

	public javax.swing.JButton getButtonAltEmail() {
		return buttonAltEmail;
	}

	public void setButtonAltEmail(javax.swing.JButton buttonAltEmail) {
		this.buttonAltEmail = buttonAltEmail;
	}

	public javax.swing.JButton getButtonAltPass() {
		return buttonAltPass;
	}

	public void setButtonAltPass(javax.swing.JButton buttonAltPass) {
		this.buttonAltPass = buttonAltPass;
	}

	public javax.swing.JButton getButtonAltUsername() {
		return buttonAltUsername;
	}

	public void setButtonAltUsername(javax.swing.JButton buttonAltUsername) {
		this.buttonAltUsername = buttonAltUsername;
	}

	public javax.swing.JButton getButtonBackup() {
		return buttonBackup;
	}

	public void setButtonBackup(javax.swing.JButton buttonBackup) {
		this.buttonBackup = buttonBackup;
	}

	public javax.swing.JButton getButtonDesenvolver() {
		return buttonDesenvolver;
	}

	public void setButtonDesenvolver(javax.swing.JButton buttonDesenvolver) {
		this.buttonDesenvolver = buttonDesenvolver;
	}

	public javax.swing.JButton getButtonPDFProduto() {
		return buttonPDFProd;
	}

	public void setButtonPDFProduto(javax.swing.JButton buttonPDFProduto) {
		this.buttonPDFProd = buttonPDFProduto;
	}

	public javax.swing.JButton getButtonPDFVali() {
		return buttonPDFVali;
	}

	public void setButtonPDFVali(javax.swing.JButton buttonPDFVali) {
		this.buttonPDFVali = buttonPDFVali;
	}

	public javax.swing.JCheckBox getCheckStartLogin() {
		return checkStartLogin;
	}

	public void setCheckStartLogin(javax.swing.JCheckBox checkStartLogin) {
		this.checkStartLogin = checkStartLogin;
	}

	public javax.swing.JPanel getPanel() {
		return panelScroll;
	}

	public void setPanel(javax.swing.JPanel panel) {
		this.panelScroll = panel;
	}

	public javax.swing.JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(javax.swing.JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public javax.swing.JLabel getLabelAdicionais() {
		return labelAdicionais;
	}

	public void setLabelAdicionais(javax.swing.JLabel labelAdicionais) {
		this.labelAdicionais = labelAdicionais;
	}

	public javax.swing.JLabel getLabelBackup() {
		return labelBackup;
	}

	public void setLabelBackup(javax.swing.JLabel labelBackup) {
		this.labelBackup = labelBackup;
	}

	public javax.swing.JLabel getLabelInfConta() {
		return labelInfoConta;
	}

	public void setLabelInfConta(javax.swing.JLabel labelInfConta) {
		this.labelInfoConta = labelInfConta;
	}

	public javax.swing.JLabel getLabelQntProd() {
		return labelQntProd;
	}

	public void setLabelQntProd(javax.swing.JLabel labelQntProd) {
		this.labelQntProd = labelQntProd;
	}

	public javax.swing.JLabel getLabelQntVali() {
		return labelQntVali;
	}

	public void setLabelQntVali(javax.swing.JLabel labelQntVali) {
		this.labelQntVali = labelQntVali;
	}

	public javax.swing.JLabel getLabelTitulo() {
		return labelTitulo;
	}

	public void setLabelTitulo(javax.swing.JLabel labelTitulo) {
		this.labelTitulo = labelTitulo;
	}

	public javax.swing.JTextField getTxtQntProd() {
		return txtQntProd;
	}

	public void setTxtQntProd(javax.swing.JTextField txtQntProd) {
		this.txtQntProd = txtQntProd;
	}

	public javax.swing.JTextField getTxtQntVali() {
		return txtQntVali;
	}

	public void setTxtQntVali(javax.swing.JTextField txtQntVali) {
		this.txtQntVali = txtQntVali;
	}

	public javax.swing.JButton getButtonPDFDonations() {
		return buttonPDFDonations;
	}

	public void setButtonPDFDonations(javax.swing.JButton buttonPDFDonations) {
		this.buttonPDFDonations = buttonPDFDonations;
	}

	public javax.swing.JButton getButtonPDFProd() {
		return buttonPDFProd;
	}

	public void setButtonPDFProd(javax.swing.JButton buttonPDFProd) {
		this.buttonPDFProd = buttonPDFProd;
	}

	public javax.swing.JButton getButtonSave() {
		return buttonSave;
	}

	public void setButtonSave(javax.swing.JButton buttonSave) {
		this.buttonSave = buttonSave;
	}

	public javax.swing.JCheckBox getCheckSendEmailTesouraria() {
		return checkSendEmailTesouraria;
	}

	public void setCheckSendEmailTesouraria(javax.swing.JCheckBox checkSendEmailTesouraria) {
		this.checkSendEmailTesouraria = checkSendEmailTesouraria;
	}

	public javax.swing.JTextField getTxtStatus() {
		return txtStatus;
	}

	public void setTxtStatus(javax.swing.JTextField txtStatus) {
		this.txtStatus = txtStatus;
	}

	public javax.swing.JLabel getLabelDays() {
		return labelDays;
	}

	public void setLabelDays(javax.swing.JLabel labelDays) {
		this.labelDays = labelDays;
	}

	public javax.swing.JLabel getLabelDesenvolvedor() {
		return labelDesenvolvedor;
	}

	public void setLabelDesenvolvedor(javax.swing.JLabel labelDesenvolvedor) {
		this.labelDesenvolvedor = labelDesenvolvedor;
	}

	public javax.swing.JLabel getLabelEmailBackup() {
		return labelEmailBackup;
	}

	public void setLabelEmailBackup(javax.swing.JLabel labelEmailBackup) {
		this.labelEmailBackup = labelEmailBackup;
	}

	public javax.swing.JLabel getLabelEmailPrograma() {
		return labelEmailPrograma;
	}

	public void setLabelEmailPrograma(javax.swing.JLabel labelEmailPrograma) {
		this.labelEmailPrograma = labelEmailPrograma;
	}

	public javax.swing.JLabel getLabelEmailTesouraria() {
		return labelEmailTesouraria;
	}

	public void setLabelEmailTesouraria(javax.swing.JLabel labelEmailTesouraria) {
		this.labelEmailTesouraria = labelEmailTesouraria;
	}

	public javax.swing.JLabel getLabelInfoConta() {
		return labelInfoConta;
	}

	public void setLabelInfoConta(javax.swing.JLabel labelInfoConta) {
		this.labelInfoConta = labelInfoConta;
	}

	public javax.swing.JLabel getLabelNameEmpresa() {
		return labelNameEmpresa;
	}

	public void setLabelNameEmpresa(javax.swing.JLabel labelNameEmpresa) {
		this.labelNameEmpresa = labelNameEmpresa;
	}

	public javax.swing.JLabel getLabelQntDays() {
		return labelQntDays;
	}

	public void setLabelQntDays(javax.swing.JLabel labelQntDays) {
		this.labelQntDays = labelQntDays;
	}

	public javax.swing.JLabel getLabelQntDoacoes() {
		return labelQntDoacoes;
	}

	public void setLabelQntDoacoes(javax.swing.JLabel labelQntDoacoes) {
		this.labelQntDoacoes = labelQntDoacoes;
	}

	public javax.swing.JPanel getPanelAdicionais() {
		return panelAdicionais;
	}

	public void setPanelAdicionais(javax.swing.JPanel panelAdicionais) {
		this.panelAdicionais = panelAdicionais;
	}

	public javax.swing.JPanel getPanelBackup() {
		return panelBackup;
	}

	public void setPanelBackup(javax.swing.JPanel panelBackup) {
		this.panelBackup = panelBackup;
	}

	public javax.swing.JPanel getPanelDesenvolver() {
		return panelDesenvolver;
	}

	public void setPanelDesenvolver(javax.swing.JPanel panelDesenvolver) {
		this.panelDesenvolver = panelDesenvolver;
	}

	public javax.swing.JPanel getPanelInfo() {
		return panelInfo;
	}

	public void setPanelInfo(javax.swing.JPanel panelInfo) {
		this.panelInfo = panelInfo;
	}

	public javax.swing.JPanel getPanelScroll() {
		return panelScroll;
	}

	public void setPanelScroll(javax.swing.JPanel panelScroll) {
		this.panelScroll = panelScroll;
	}

	public javax.swing.JSeparator getSeparator1() {
		return separator1;
	}

	public void setSeparator1(javax.swing.JSeparator separator1) {
		this.separator1 = separator1;
	}

	public javax.swing.JSeparator getSeparator2() {
		return separator2;
	}

	public void setSeparator2(javax.swing.JSeparator separator2) {
		this.separator2 = separator2;
	}

	public javax.swing.JSeparator getSeparator3() {
		return separator3;
	}

	public void setSeparator3(javax.swing.JSeparator separator3) {
		this.separator3 = separator3;
	}

	public javax.swing.JSeparator getSeparator4() {
		return separator4;
	}

	public void setSeparator4(javax.swing.JSeparator separator4) {
		this.separator4 = separator4;
	}

	public javax.swing.JSpinner getSpinnerDays() {
		return spinnerDays;
	}

	public void setSpinnerDays(javax.swing.JSpinner spinnerDays) {
		this.spinnerDays = spinnerDays;
	}

	public javax.swing.JTextField getTxtEmailBackup() {
		return txtEmailBackup;
	}

	public void setTxtEmailBackup(javax.swing.JTextField txtEmailBackup) {
		this.txtEmailBackup = txtEmailBackup;
	}

	public javax.swing.JTextField getTxtEmailPrograma() {
		return txtEmailPrograma;
	}

	public void setTxtEmailPrograma(javax.swing.JTextField txtEmailPrograma) {
		this.txtEmailPrograma = txtEmailPrograma;
	}

	public javax.swing.JTextField getTxtEmailTesouraria() {
		return txtEmailTesouraria;
	}

	public void setTxtEmailTesouraria(javax.swing.JTextField txtEmailTesouraria) {
		this.txtEmailTesouraria = txtEmailTesouraria;
	}

	public javax.swing.JTextField getTxtNameEmpresa() {
		return txtNameEmpresa;
	}

	public javax.swing.JButton getButtonDeslogar() {
		return buttonDeslogar;
	}

	public void setButtonDeslogar(javax.swing.JButton buttonDeslogar) {
		this.buttonDeslogar = buttonDeslogar;
	}

	public void setTxtNameEmpresa(javax.swing.JTextField txtNameEmpresa) {
		this.txtNameEmpresa = txtNameEmpresa;
	}

	public javax.swing.JTextField getTxtQntDoacoes() {
		return txtQntDoacoes;
	}

	public void setTxtQntDoacoes(javax.swing.JTextField txtQntDoacoes) {
		this.txtQntDoacoes = txtQntDoacoes;
	}
}
