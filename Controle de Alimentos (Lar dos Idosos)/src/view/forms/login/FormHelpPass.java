package view.forms.login;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.Login;
import view.panels.login.PanelHelpPass;

public class FormHelpPass {

	private PanelHelpPass panel;
	private UsersController controllerUser;
	private JFrame frame;
	private String passwordDesenvolver;
	private String  emailPrimitive, senhaPrimitive;
	private Database dS;
	public FormHelpPass(Database dU, JFrame frame, String passwordDesenvolver, Database dS, String emailPrimitive, String senhaPrimitive) throws Exception {
		this.panel = new PanelHelpPass();
		this.dS = dS;
		this.emailPrimitive = emailPrimitive;
		this.senhaPrimitive = senhaPrimitive;
		this.passwordDesenvolver = passwordDesenvolver;
		this.frame = frame;
		controllerUser = new UsersController(dU);

panel.getTxtEmail().addActionListener(e -> send_Click());
		
		panel.getButtonSend().addActionListener(e -> send_Click());
		panel.getButtonSend().setMnemonic(KeyEvent.VK_S);
		panel.getButtonSend().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonSend().doClick();
				}
			}
		});

		panel.getButtonVoltar().addActionListener(e -> voltar_Click());
		panel.getButtonVoltar().setMnemonic(KeyEvent.VK_S);
		panel.getButtonVoltar().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonVoltar().doClick();
				}
			}
		});
	}

	public boolean isValiidadEmail(String email) {

		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public void send_Click() {
		try {
			panel.getTxtEmail().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (panel.getTxtEmail().getText().trim().isEmpty() == false) {
				if (isValiidadEmail(panel.getTxtEmail().getText().trim())) {
					Users user = controllerUser.getUser(panel.getTxtEmail().getText().trim(), "email");
					Thread sendEmail = new Thread(
							new RememberPassEmail(user.getEmail(), user.getUsername(), user.getPassword(), this));
					sendEmail.start();
				} else {
					panel.getTxtEmail().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					panel.getTxtEmail().requestFocus();
					throw new Exception("Endere�o de email inv�lido.");
				}
			} else {
				panel.getTxtEmail().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				panel.getTxtEmail().requestFocus();
			}
		} catch (Exception e) {
			panel.getTxtEmail().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			panel.getTxtStatus().setText(e.getMessage());
		}
	}

	public void voltar_Click() {
		frame.getContentPane().removeAll();
		Login.setPanel(frame, new FormLogin(controllerUser.getDatabase(), frame, passwordDesenvolver, dS, emailPrimitive,senhaPrimitive).getPanel());
	}

	public PanelHelpPass getPanel() {
		return panel;
	}

	public void setPanel(PanelHelpPass panel) {
		this.panel = panel;
	}

	public UsersController getControllerUser() {
		return controllerUser;
	}

	public void setControllerUser(UsersController controllerUser) {
		this.controllerUser = controllerUser;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
