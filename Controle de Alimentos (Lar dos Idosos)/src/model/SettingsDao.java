package model;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;

import model.entity.Settings;

public class SettingsDao {
	public static Database db;
	public static Dao<Settings, Integer> dao;
	public Settings settings;
	public boolean setado;

	public SettingsDao(Database database) throws Exception
	{
		SettingsDao.setDatabase(database);
		createIfNotExist();
	}

	public boolean isEmailSetado()
	{
		if(settings.getEmailPrograma().isEmpty() || settings.getSenhaEmailPrograma().isEmpty())
			return false;
		else return true;
	}
	
	public boolean isSetado() {
		if (settings.getDiasAlertaVencimento() == 15 && settings.getEmailBacukp().equals("")
				&& settings.getEmailPrograma().equals("") && settings.getEmailTesouraria().equals("")
				&& settings.getHelpEmail().equals("") && settings.getNomeEmpresa().equals("")
				&& settings.getPassword().equals("") && settings.getPasswordTesouraria().equals("")
				&& settings.getSenhaEmailPrograma().equals("") && settings.getUsername().equals("")
				&& settings.isSendEmailTesouraria() && settings.isStartLogin()) {
			return false;
		} else
			return true;
	}
	
	public boolean isBackupSetado()
	{
		if( settings.getEmailBacukp().isEmpty()) return false;
		else return true;
	}

	public static void setDatabase(Database database) {
		SettingsDao.db = database;
		try {
			dao = DaoManager.createDao(database.getConnection(), Settings.class);
			TableUtils.createTableIfNotExists(database.getConnection(), Settings.class);
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public void createIfNotExist() throws Exception {
		if (dao.queryForAll().size() == 0) {
			settings = new Settings();
			settings.setEmailPrograma("");
			settings.setSenhaEmailPrograma("");
			settings.setEmailTesouraria("");
			settings.setEmailBacukp("");
			settings.setSendEmailTesouraria(true);
			settings.setStartLogin(true);
			settings.setUsername("");
			settings.setPassword("");
			settings.setNomeEmpresa("");
			settings.setHelpEmail("");
			settings.setPasswordTesouraria("");
			settings.setDiasAlertaVencimento(15);
			int nrows = 0;
			nrows = dao.create(settings);
			if (nrows == 0)
				throw new SQLException("N�o foi poss�vel criar as configura��es do programa");
		} else {
			settings = dao.queryForAll().get(0);
		}

	}

	public Settings getSettings() {
		return settings;
	}

	public void update() throws Exception {
		int nrows = 0;
		nrows = dao.update(settings);
		if (nrows == 0)
			throw new SQLException("N�o foi poss�vel atualizar as configura��es");
	}
}
