package model.entity;

import com.j256.ormlite.field.DatabaseField;
public class Produtos
{
    @DatabaseField(generatedId = true)
    int id;
    
    @DatabaseField
    String codBar;
    
    @DatabaseField
    String nome;
    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie codBar*/
    public String getCodBar(){
        return this.codBar;
    }//end method getCodBar

    /**SET Method Propertie codBar*/
    public void setCodBar(String codBar){
        this.codBar = codBar;
    }//end method setCodBar

    /**GET Method Propertie nome*/
    public String getNome(){
        return this.nome;
    }//end method getNome

    /**SET Method Propertie nome*/
    public void setNome(String nome){
        this.nome = nome;
    }//end method setNome

    //End GetterSetterExtension Source Code
//!
}
