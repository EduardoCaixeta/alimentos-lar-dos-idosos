package view.forms.configs;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.format.Limite_digitos;

@SuppressWarnings("serial")
public class ConfigAltPassword extends JPanel {

	private javax.swing.JButton buttonCancel;
	private javax.swing.JButton buttonSave;
	private javax.swing.JLabel labelConfPass;
	private javax.swing.JLabel labelNewPass;
	private javax.swing.JLabel labelOldPass;
	private javax.swing.JPasswordField txtConfPass;
	private javax.swing.JPasswordField txtNewPass;
	private javax.swing.JPasswordField txtOldPass;
	private UsersController controllerUser;
	private JFrame frame;
	private JDialog optionPane;

	Users user;
	public ConfigAltPassword(JFrame frame, Users user, String nameDBU) throws Exception {
		this.user = user;
		this.frame = frame;
		controllerUser = new UsersController(new Database(nameDBU));
		initComponents();
		showFrame();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(210, 210, 210));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() throws Exception {
		optionPane = new JDialog(frame);
		optionPane.add(this);
		optionPane.setSize(340, 230);
		optionPane.setModal(true);
		optionPane.setResizable(false);
		optionPane.setLocation(frame.getX() + frame.getWidth() / 2 - optionPane.getWidth() / 2,
				frame.getY() + frame.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}
	public void close() {
		optionPane.dispose();
	}

	public void cancel_Click() {
		close();
	}

	@SuppressWarnings("deprecation")
	private void initComponents() {
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});
		buttonCancel = new javax.swing.JButton();
		labelOldPass = new javax.swing.JLabel();
		txtOldPass = new javax.swing.JPasswordField();
		labelNewPass = new javax.swing.JLabel();
		labelConfPass = new javax.swing.JLabel();
		buttonSave = new javax.swing.JButton();
		txtNewPass = new javax.swing.JPasswordField();
		txtConfPass = new javax.swing.JPasswordField();

		buttonCancel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonCancel.setText("CANCELAR");
		buttonCancel.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonCancel.setFocusCycleRoot(true);
		buttonCancel.setFocusPainted(false);
		buttonCancel.addActionListener(e -> cancel_Click());
		buttonCancel.setMnemonic(KeyEvent.VK_S);
		buttonCancel.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonCancel.doClick();
				}
			}
		});

		labelNewPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelNewPass.setText("Nova senha:");

		labelConfPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelConfPass.setText("Confirmar senha:");

		buttonSave.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonSave.setText("SALVAR");
		buttonSave.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonSave.setFocusCycleRoot(true);
		buttonSave.setFocusPainted(false);
		buttonSave.addActionListener(e -> save_Click());
		buttonSave.setMnemonic(KeyEvent.VK_S);
		buttonSave.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					buttonSave.doClick();
				}
			}
		});

		labelOldPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelOldPass.setText("Senha atual:");

		txtOldPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtOldPass.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtOldPass.setDocument(new Limite_digitos(15, ""));
		txtOldPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtOldPass.addActionListener(e -> {
			txtOldPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtOldPass.getText().isEmpty() == false)
				txtNewPass.requestFocus();
			else {
				txtOldPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});
		txtOldPass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});

		txtNewPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtNewPass.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtNewPass.setDocument(new Limite_digitos(15, ""));
		txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtNewPass.addActionListener(e -> {
			txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtNewPass.getText().isEmpty() == false)
				txtConfPass.requestFocus();
			else {
				txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});
		txtNewPass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});

		txtConfPass.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtConfPass.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtConfPass.setDocument(new Limite_digitos(15, ""));
		txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtConfPass.addActionListener(e -> {
			txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtConfPass.getText().isEmpty() == false)
				save_Click();
			else {
				txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		});
		txtConfPass.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					buttonCancel.doClick();
				}
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap(24, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup().addComponent(labelConfPass)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(txtConfPass, javax.swing.GroupLayout.PREFERRED_SIZE, 165,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup().addComponent(buttonSave).addGap(42, 42, 42)
												.addComponent(buttonCancel).addGap(25, 25, 25))
								.addGroup(layout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addGroup(layout.createSequentialGroup().addComponent(labelOldPass)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(txtOldPass, javax.swing.GroupLayout.PREFERRED_SIZE, 172,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGroup(javax.swing.GroupLayout.Alignment.LEADING,
												layout.createSequentialGroup().addComponent(labelNewPass)
														.addPreferredGap(
																javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
														.addComponent(txtNewPass,
																javax.swing.GroupLayout.PREFERRED_SIZE, 172,
																javax.swing.GroupLayout.PREFERRED_SIZE))))
						.addContainerGap(21, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap(21, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelOldPass).addComponent(txtOldPass, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelNewPass).addComponent(txtNewPass, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelConfPass).addComponent(txtConfPass, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18).addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(buttonSave).addComponent(buttonCancel))
				.addContainerGap(12, Short.MAX_VALUE)));
	}// </editor-fold>

	@SuppressWarnings("deprecation")
	public void save_Click() {
		try {
			txtOldPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtOldPass.getText().isEmpty() == false) {
				if (txtOldPass.getText().equals(user.getPassword())) {
					if (txtNewPass.getText().isEmpty() == false) {
						if (txtNewPass.getText().equals(txtOldPass.getText()) == false) {
							if (txtConfPass.getText().isEmpty() == false) {
								if (txtNewPass.getText().length() >= 5 && txtNewPass.getText().length() <= 12) {
									if (txtConfPass.getText().equals(txtNewPass.getText())) {

										user.setPassword(txtNewPass.getText());
										controllerUser.update(user);
										JOptionPane.showMessageDialog(optionPane, "Senha Alterada Com Sucesso!",
												"CONFIGURA��ES", JOptionPane.INFORMATION_MESSAGE);
										close();

									} else {
										txtConfPass.setText("");
										txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
										txtNewPass.setText("");
										txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
										txtNewPass.requestFocus();
										throw new Exception("Nova senha n�o confirmada.");
									}
								} else {
									txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
									txtNewPass.requestFocus();
									throw new Exception("A senha deve conter de 5 a 12 caracteres.");
								}
							} else {
								txtConfPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
								txtConfPass.requestFocus();
								throw new Exception("~");
							}
						} else {
							txtNewPass.setText("");
							txtConfPass.setText("");
							txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							txtNewPass.requestFocus();
							throw new Exception("Sua nova senha n�o pode igual a anterior.");
						}
					} else {
						txtNewPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
						txtNewPass.requestFocus();
						throw new Exception("~");
					}
				} else {
					txtNewPass.setText("");
					txtConfPass.setText("");
					txtOldPass.setText("");
					txtOldPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					txtOldPass.requestFocus();
					throw new Exception("Senha incorreta.");
				}
			} else {
				txtOldPass.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				txtOldPass.requestFocus();
				throw new Exception("~");
			}
		} catch (Exception e) {
			if (e.getMessage().equals("~") == false)
				JOptionPane.showMessageDialog(optionPane, e.getMessage().toUpperCase(), "Algo deu errado",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	// Variables declaration - do not modify

	// End of variables declaration
}