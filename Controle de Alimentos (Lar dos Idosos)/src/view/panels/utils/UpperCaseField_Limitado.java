package view.panels.utils;

import javax.swing.JTextField;
import javax.swing.text.*;

@SuppressWarnings("serial")
public class UpperCaseField_Limitado extends JTextField {

	private int quantidadeMax;
	private String bloq;
	private boolean upper;
	private boolean fristUpper = false;
	
	public UpperCaseField_Limitado(int maxLen, String bloq, boolean upper) {
		super();
		quantidadeMax = maxLen;
		this.bloq = bloq;
		this.upper = upper;
	}

	public UpperCaseField_Limitado(int maxLen, String bloq, boolean upper, boolean fristUpper) {
		super();
		quantidadeMax = maxLen;
		this.bloq = bloq;
		this.upper = upper;
		this.fristUpper = fristUpper;
	}
	
	protected Document createDefaultModel() {
		return new UpperCaseDocument();
	}

	public class UpperCaseDocument extends PlainDocument {

		@SuppressWarnings("unused")
		@Override
		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			int totalquantia = (getLength() + str.length());
			if (totalquantia <= quantidadeMax) {
				char[] upp = str.toCharArray();
				if (upper) {
					if (str == null) {
						return;
					}
					for (int i = 0; i < upp.length; i++) {
						upp[i] = Character.toUpperCase(upp[i]);
					}
				}
				if(totalquantia==1 && fristUpper)
				{
					upp[0] = Character.toUpperCase(upp[0]);
				}
				super.insertString(offset, new String(upp).replaceAll(bloq, ""), attr);
			}
		}
	}
}
