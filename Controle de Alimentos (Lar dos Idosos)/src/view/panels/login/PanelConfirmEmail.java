package view.panels.login;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;

import javax.swing.JDialog;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class PanelConfirmEmail extends javax.swing.JPanel {

    JFrame bloq;
    JDialog optionPane;
    String email;
    public PanelConfirmEmail(JFrame frame,String email) {
    	this.bloq = frame;
    	this.email = email;
        initComponents();
        showFrame();
    }

    public void showFrame() {
		optionPane = new JDialog(bloq);
		optionPane.add(this);
		optionPane.setSize(341, 209);
		optionPane.setModal(true);
		optionPane.setLocationRelativeTo(bloq);
		optionPane.setUndecorated(true);
		optionPane.setResizable(false);
		optionPane.setLocation(bloq.getX()+bloq.getWidth()/2-optionPane.getWidth()/2, bloq.getY()+bloq.getHeight()/2-optionPane.getHeight()/2);
		optionPane.setVisible(true);
	}
    
    public void paintComponent(Graphics g) {
		g.setColor(new Color(130, 130, 230));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}
    
    
    public void confirm_Click()
    {
    	
    }
    
    public void send_Click()
    {
    	
    }
    
    public void cancel_Click()
    {
    	optionPane.dispose();
    }
    
    private void initComponents() {

        labelCod = new javax.swing.JLabel();
        txtCod = new javax.swing.JTextField();
        labelFC = new javax.swing.JLabel();
        buttonSendEmail = new javax.swing.JButton();
        buttonCancel = new javax.swing.JButton();
        buttonConfirm = new javax.swing.JButton();
        labelMsg = new javax.swing.JLabel();
        labelEmail = new javax.swing.JLabel();
        labelTitulo = new javax.swing.JLabel();

        labelCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelCod.setText("C�DIGO DE CONFIRMA��O:");

        txtCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        labelFC.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelFC.setText("FC - ");

        buttonSendEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonSendEmail.setText("Reenviar email");
        buttonSendEmail.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonSendEmail.addActionListener(e -> send_Click());
        buttonSendEmail.setFocusable(false);

        buttonCancel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonCancel.setText("Cancelar");
        buttonCancel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonCancel.addActionListener(e -> optionPane.dispose());
        buttonCancel.setFocusable(false);

        buttonConfirm.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonConfirm.setText("Confirmar");
        buttonConfirm.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonConfirm.addActionListener(e -> confirm_Click());
        buttonConfirm.setFocusable(false);

        labelMsg.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelMsg.setText("Um email com o c�digo de confirma��o foi enviado para");

        labelEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelEmail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelEmail.setText(email.toLowerCase());

        labelTitulo.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        labelTitulo.setText("CONFIRME SEU ENDERE�O DE EMAIL");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(labelEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(labelCod)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(labelFC)
                            .addGap(0, 0, 0)
                            .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelTitulo)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(buttonConfirm)
                    .addGap(48, 48, 48)
                    .addComponent(buttonCancel)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonSendEmail)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(labelTitulo)
                    .addGap(21, 21, 21)
                    .addComponent(labelMsg)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelEmail)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelCod)
                        .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelFC))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(buttonSendEmail)
                    .addGap(21, 21, 21)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonConfirm)
                        .addComponent(buttonCancel))
                    .addContainerGap())
            );
    }// </editor-fold>                        


    // Variables declaration - do not modify                     
    private javax.swing.JButton buttonSendEmail;
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonConfirm;
    private javax.swing.JLabel labelCod;
    private javax.swing.JLabel labelEmail;
    private javax.swing.JLabel labelFC;
    private javax.swing.JLabel labelMsg;
    private javax.swing.JLabel labelTitulo;
    private javax.swing.JTextField txtCod;
    // End of variables declaration                   
	public javax.swing.JButton getButtonSendEmail() {
		return buttonSendEmail;
	}

	public void setButtonSendEmail(javax.swing.JButton buttonSendEmail) {
		this.buttonSendEmail = buttonSendEmail;
	}

	public javax.swing.JButton getButtonCancel() {
		return buttonCancel;
	}

	public void setButtonCancel(javax.swing.JButton buttonCancel) {
		this.buttonCancel = buttonCancel;
	}

	public javax.swing.JButton getButtonConfirm() {
		return buttonConfirm;
	}

	public void setButtonConfirm(javax.swing.JButton buttonConfirm) {
		this.buttonConfirm = buttonConfirm;
	}

	public javax.swing.JLabel getLabelCod() {
		return labelCod;
	}

	public void setLabelCod(javax.swing.JLabel labelCod) {
		this.labelCod = labelCod;
	}

	public javax.swing.JLabel getLabelEmail() {
		return labelEmail;
	}

	public void setLabelEmail(javax.swing.JLabel labelEmail) {
		this.labelEmail = labelEmail;
	}

	public javax.swing.JLabel getLabelFC() {
		return labelFC;
	}

	public void setLabelFC(javax.swing.JLabel labelFC) {
		this.labelFC = labelFC;
	}

	public javax.swing.JLabel getLabelMsg() {
		return labelMsg;
	}

	public void setLabelMsg(javax.swing.JLabel labelMsg) {
		this.labelMsg = labelMsg;
	}

	public javax.swing.JLabel getLabelTitulo() {
		return labelTitulo;
	}

	public void setLabelTitulo(javax.swing.JLabel labelTitulo) {
		this.labelTitulo = labelTitulo;
	}

	public javax.swing.JTextField getTxtCod() {
		return txtCod;
	}

	public void setTxtCod(javax.swing.JTextField txtCod) {
		this.txtCod = txtCod;
	}
}