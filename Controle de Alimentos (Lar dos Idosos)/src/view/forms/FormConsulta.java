package view.forms;

import view.panels.PanelConsulta;
import view.panels.consulta.OptionPaneQntd;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.KeyAdapter;
import controller.*;
import model.Database;
import model.entity.*;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import java.util.ArrayList;
import java.awt.MouseInfo;

import view.App;
import view.forms.utils.*;

public class FormConsulta {
	private PanelConsulta panel;
	private ValidadeController controllerValidade;
	private Database dP;
	private List<Validade> validades;
	private JFrame frame;
	private boolean selectAll;
	private String nameDBS;

	public FormConsulta(Database dV, Database dP, String nameDBS, JFrame frame, String codUser, App app) {
		this.nameDBS = nameDBS;
		try {
			panel = new PanelConsulta(codUser,
					new SettingsController(new Database(nameDBS)).getSettings().getDiasAlertaVencimento());
			validades = controllerValidade.readAll();
			if (validades.size() == 0) {
				panel.getButtonMarcaAll().setEnabled(false);
				panel.getButtonDelete().setEnabled(false);
				panel.getButtonEdit().setEnabled(false);
				throw new Exception("N�o h� produtos cadastrados.");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}

		selectAll = false;
		this.frame = frame;
		this.dP = dP;
		controllerValidade = new ValidadeController(dV);

		panel.getButtonAdd().addActionListener(e -> app.getMenuBarra().getButtonCadastrar().doClick());
		panel.getButtonAdd().setMnemonic(KeyEvent.VK_S);
		panel.getButtonAdd().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonAdd().doClick();
				}
			}
		});

		panel.getButtonPesquisa().addActionListener(e -> pesquisa_Click());
		panel.getButtonPesquisa().setMnemonic(KeyEvent.VK_S);
		panel.getButtonPesquisa().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonPesquisa().doClick();
				}
			}
		});

		panel.getButtonDelete().addActionListener(e -> delete_Click());
		panel.getButtonDelete().setMnemonic(KeyEvent.VK_S);
		panel.getButtonDelete().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonDelete().doClick();
				}
			}
		});

		panel.getButtonMarcaAll().addActionListener(e -> marcaAll_Click());
		panel.getButtonMarcaAll().setMnemonic(KeyEvent.VK_S);
		panel.getButtonMarcaAll().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonMarcaAll().doClick();
				}
			}
		});

		panel.getButtonEdit().addActionListener(e -> edit_Click());
		panel.getButtonEdit().setMnemonic(KeyEvent.VK_S);
		panel.getButtonEdit().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonEdit().doClick();
				}
			}
		});

		panel.getTxtCodBar().addActionListener(e -> pesquisa_Click());

		panel.getTxtNome().addActionListener(e -> pesquisa_Click());

		panel.getTable().addKeyListener(new KeyAdapter() {
			@SuppressWarnings("deprecation")
			public void keyPressed(KeyEvent e) {
				if (String.valueOf(e.getKeyChar()).equals("+") && panel.getTable().getSelectedRow() != -1) {
					MoreMinus(panel.getTable().getSelectedRows(), 1);
				} else if (String.valueOf(e.getKeyChar()).equals("-") && panel.getTable().getSelectedRow() != -1) {
					MoreMinus(panel.getTable().getSelectedRows(), -1);
				} else if ((e.getKeyCode() == KeyEvent.VK_P) && (e.getModifiers() & KeyEvent.CTRL_MASK) != 0) {
					if (panel.getTable().getSelectedRow() != -1) {
						List<Validade> validades = new ArrayList<Validade>();
						for (int i = 0; i < panel.getTable().getSelectedRows().length; i++) {
							validades.add(
									panel.getModelTable().getValidades().get(panel.getTable().getSelectedRows()[i]));
						}
						new CreatePDF().criarValidades(frame, validades);
					} else {
						panel.getTxtStatus().setText("Selecione um cadastro.");
					}
				}
			}
		});
		panel.getTable().addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				panel.getTxtStatus().setText("");
				if (e.isMetaDown())
					if (panel.getTable().getSelectedRow() != -1) {
						int row = panel.getTable().rowAtPoint(e.getPoint());
						boolean select = false;
						for (int i = 0; i < panel.getTable().getSelectedRows().length; i++) {
							if (panel.getTable().getSelectedRows()[i] == row)
								select = true;
						}
						if (select) {
							JDialog altQntd = new JDialog(frame);

							OptionPaneQntd qntPane = new OptionPaneQntd();
							qntPane.getButtonAdd()
									.addActionListener(event -> MoreMinus(panel.getTable().getSelectedRows(), 1));
							qntPane.getButtonAdd().addKeyListener(new KeyAdapter() {
								public void keyPressed(KeyEvent e) {
									if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
										altQntd.dispose();
									}
								}
							});

							qntPane.getButtonMinus()
									.addActionListener(event -> MoreMinus(panel.getTable().getSelectedRows(), -1));
							qntPane.getButtonMinus().addKeyListener(new KeyAdapter() {
								public void keyPressed(KeyEvent e) {
									if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
										altQntd.dispose();
									}
								}
							});
							altQntd.add(qntPane);
							altQntd.setUndecorated(true);
							altQntd.pack();
							altQntd.setSize(116, 43);
							altQntd.setLocation(MouseInfo.getPointerInfo().getLocation());
							altQntd.setVisible(true);
							altQntd.addKeyListener(new KeyAdapter() {
								public void keyPressed(KeyEvent e) {
									if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
										altQntd.dispose();
									}
								}
							});
							altQntd.addWindowFocusListener(new WindowFocusListener() {

								@Override
								public void windowLostFocus(WindowEvent e) {
									altQntd.dispose();

								}

								@Override
								public void windowGainedFocus(WindowEvent e) {
									// TODO Auto-generated method stub

								}
							});
						}
					} else
						panel.getTxtStatus().setText("Selecione um produto.");
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	public void inicioState() {
		
		try {
			panel.initComponents(new SettingsController(new Database(nameDBS)).getSettings().getDiasAlertaVencimento());
			validades = controllerValidade.readAll();
			if (validades.size() == 0) {
				panel.getButtonMarcaAll().setEnabled(false);
				panel.getButtonDelete().setEnabled(false);
				panel.getButtonEdit().setEnabled(false);
				throw new Exception("N�o h� produtos cadastrados.");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}
	}

	public void MoreMinus(int rows[], int value) {
		List<Validade> validades = panel.getModelTable().getValidades();
		int[] zeros = new int[rows.length + 1];
		for (int i = 0; i < zeros.length; i++) {
			zeros[i] = -1;
		}
		try {
			int k = 0;
			for (int i = 0; i < rows.length; i++) {
				if (validades.get(rows[i]).getQuantidade() >= 0) {
					if (value > 0) {
						validades.get(rows[i]).setQuantidade(validades.get(rows[i]).getQuantidade() + value);
						updateHD(validades.get(rows[i]));
					} else if (validades.get(rows[i]).getQuantidade() > 0) {
						validades.get(rows[i]).setQuantidade(validades.get(rows[i]).getQuantidade() + value);
						updateHD(validades.get(rows[i]));
					}
					if (validades.get(rows[i]).getQuantidade() == 0) {
						zeros[k] = rows[i] + 1;
						k++;
					}
					if (zeros[0] != -1) {
						String msg = "Linha";
						;
						if (zeros[1] != -1)
							msg += "s";
						for (int j = 0; j < k; j++) {
							if (zeros[j] != -1 && j < k - 2)
								msg += " " + String.valueOf(zeros[j]) + ",";
							else if (j < k - 1)
								msg += " " + String.valueOf(zeros[j]) + " e";
							else
								msg += " " + String.valueOf(zeros[j]);

						}
						msg += " zerada";
						if (zeros[1] != -1)
							msg += "s";
						msg += ".";
						panel.getTxtStatus().setText(msg);
					}
				}

			}
			panel.getModelTable().updateValidade();

		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}

	}

	public void updateHD(Validade validade) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					controllerValidade.update(validade);
				} catch (Exception e) {
					panel.getTxtStatus().setText(e.getMessage());
				}

			}
		}).start();
	}

	public PanelConsulta getPanel() {
		return this.panel;
	}

	public void marcaAll_Click() {
		if (selectAll) {
			panel.noSelect();
			selectAll = false;
		} else {
			panel.getTable().selectAll();
			selectAll = true;
		}
		panel.getTable().requestFocus();
	}

	public void altere(Consulta_Alterar editPane) {
		Alteracoes alterar = new Alteracoes(editPane, this);
		alterar.altere();
	}

	public Database getdP() {
		return dP;
	}

	public void setdP(Database dP) {
		this.dP = dP;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public void edit_Click() {
		try {
			panel.getTxtStatus().setText("");
			if (panel.getTable().getSelectedRow() != -1) {
				Validade updateValidade = panel.getModelTable().getValidades().get(panel.getTable().getSelectedRow());
				@SuppressWarnings("serial")
				Consulta_Alterar editPane = new Consulta_Alterar(frame, updateValidade) {
					@Override
					public void save_Click() throws Exception {
						try {
							altere(this);

							panel.getModelTable().updateValidade();
						} catch (Exception e) {
							throw new Exception(e.getMessage());
						}
					}
				};
				editPane.showFrame();
			} else {
				throw new Exception("Selecione um cadastro");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}
	}

	public void pesquisa_Click() {
		panel.getTxtStatus().setText("");
		panel.getTable().clearSelection();
		
		try {
			validades = controllerValidade.readAll();
			if (panel.getTxtNome().getText().trim().isEmpty()) {
				if (panel.getTxtCodBar().getText().trim().isEmpty()) {
					panel.getModelTable().setValidades(validades);
					panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
				} else {
					String codBar = panel.getTxtCodBar().getText();
					List<Validade> validadesCodBar = new ArrayList<Validade>();
					for (int i = 0; i < validades.size(); i++) {
						if (validades.get(i).getCodBar().equals(codBar)) {
							if (validadesCodBar.add(validades.get(i)))
								;
							else {
								panel.getModelTable().setValidades(validades);
								panel.getTxtCountRows()
										.setText(panel.getTable().getRowCount() + " Validades cadastradas");
								throw new Exception("Erro ao buscar.");
							}
						}
					}

					if (validadesCodBar.size() != 0) {

						panel.getModelTable().setValidades(validadesCodBar);
						panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
						throw new Exception("Produto encontrado.");
					} else {
						panel.getModelTable().setValidades(validades);
						panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
						throw new Exception("Produto n�o encontrado.");
					}
				}
			} else {
				String nome = panel.getTxtNome().getText();
				List<Validade> validadesNomes = new ArrayList<Validade>();
				for (int i = 0; i < validades.size(); i++) {
					String sub = validades.get(i).getNome().substring(0, nome.length());
					if (sub.equals(nome)) {
						if (validadesNomes.add(validades.get(i)))
							;
						else {
							panel.getModelTable().setValidades(validades);
							panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
							throw new Exception("Erro ao buscar.");
						}
					}
				}
				if (panel.getTxtCodBar().getText().trim().isEmpty()) {
					if (validadesNomes.size() != 0) {

						panel.getModelTable().setValidades(validadesNomes);
						panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
						throw new Exception("Produto encontrado.");
					} else {

						panel.getModelTable().setValidades(validades);
						panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
						throw new Exception("Produto n�o encontrado.");
					}
				} else {
					String codBar = panel.getTxtCodBar().getText();
					List<Validade> validadesCodBar = new ArrayList<Validade>();
					for (int i = 0; i < validadesNomes.size(); i++) {
						if (validadesNomes.get(i).getCodBar().equals(codBar)) {
							if (validadesCodBar.add(validadesNomes.get(i)))
								;
							else {
								panel.getModelTable().setValidades(validades);
								panel.getTxtCountRows()
										.setText(panel.getTable().getRowCount() + " Validades cadastradas");
								throw new Exception("Erro ao buscar.");
							}
						}
					}

					if (validadesCodBar.size() != 0) {

						panel.getModelTable().setValidades(validadesCodBar);
						panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
						throw new Exception("Produto encontrado.");
					} else {
						panel.getModelTable().setValidades(validades);
						panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
						throw new Exception("Produto n�o encontrado.");
					}
				}
			}

		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}

	}

	public void delete_Click() {
		panel.getTxtStatus().setText("");
		try {
			if (panel.getTable().getSelectedRowCount() != 0) {

				List<Validade> selecionados = new ArrayList<Validade>();
				List<Validade> validadesTable = panel.getModelTable().getValidades();
				for (int i = 0; i < panel.getTable().getSelectedRowCount(); i++) {
					int p = panel.getTable().getSelectedRows()[i];
					selecionados.add(validadesTable.get(p));
				}
				if (selecionados.size() != 0) {
					@SuppressWarnings({ "serial", "unused" })
					OptionPaneDelete delete = new OptionPaneDelete(selecionados, frame) {
						@Override
						public void delete_Click() {
							removerValidades(selecionados);
							close();
							panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
							if (validades.size() != 0)
								panel.noSelect();
						}

						@Override
						public void cancel_Click() {
							close();
							panel.noSelect();
						}
					};
				} else {
					throw new Exception("Erro ao ler as validades.");
				}
			} else {
				throw new Exception("Selecione um cadastro.");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}

	}

	public void removerValidades(List<Validade> selecionados) {
		panel.getTxtStatus().setText("");
		try {
			for (int i = 0; i < selecionados.size(); i++) {
				panel.getModelTable().removeValidade(selecionados.get(i));
				controllerValidade.delete(selecionados.get(i));
			}

			if (panel.getModelTable().getValidades().size() == 0) {
				panel.getButtonMarcaAll().setEnabled(false);
				panel.getButtonDelete().setEnabled(false);
				panel.getButtonEdit().setEnabled(false);
			}

			validades = controllerValidade.readAll();
		}

		catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}
	}

	public ValidadeController getControllerValidade() {
		return controllerValidade;
	}

	public void setControllerValidade(ValidadeController controllerValidade) {
		this.controllerValidade = controllerValidade;
	}

	public List<Validade> getValidades() {
		return validades;
	}

	public void setValidades(List<Validade> validades) {
		this.validades = validades;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}

	public void setPanel(PanelConsulta panel) {
		this.panel = panel;
	}

}
