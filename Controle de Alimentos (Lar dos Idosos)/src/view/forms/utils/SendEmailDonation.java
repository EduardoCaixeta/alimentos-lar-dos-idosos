package view.forms.utils;

import java.io.File;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.commons.mail.DefaultAuthenticator;

import model.entity.Doacao;
import model.entity.Settings;

import org.apache.commons.mail.*;

public class SendEmailDonation {

	public Doacao doacao;
	public Settings settings;

	public SendEmailDonation(Doacao doacao, Settings settings) throws Exception {
		this.doacao = doacao;
		this.settings = settings;
		enviaEmailHtml(settings.getEmailTesouraria());
	}

	@SuppressWarnings("deprecation")
	public HtmlEmail conectaEmail() throws Exception {
		String hostname = "smtp.gmail.com";
		String username = settings.getEmailPrograma();
		String password = settings.getSenhaEmailPrograma();
		HtmlEmail email = new HtmlEmail();
		email.setHostName(hostname);
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator(username, password));
		email.setTLS(true);
		email.setFrom(username, "Food Control");
		email.setDebug(true);

		email.setCharset(HtmlEmail.ISO_8859_1);
		Properties props = new Properties();
		props.setProperty(hostname, "smtp");
		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		props.setProperty("mail.smtp.port", "" + 587);
		props.setProperty("mail.smtp.starttls.enable", "true");
		Session mailSession = Session.getInstance(props, new DefaultAuthenticator(username, password));
		email.setMailSession(mailSession);
		return email;
	}

	public void enviaEmailHtml(String email) throws Exception {

		HtmlEmail mensagem = conectaEmail();
		mensagem.addTo(email);
		mensagem.setSubject("O " + settings.getNomeEmpresa() + " RECEBEU UMA NOVA DOA��O");

		File img = new File("images//velhos.png");
		mensagem.setHtmlMsg("<html>" + "<head>" + "<center>" + "<p> " + "<font face=\"Comics San\" size=\"5\">"
				+ "&emsp;&emsp;NOVA DOA��O REGISTRADA" + "</font>" + "<br /> " + "</p>" + "</center>" + "<center>"
				+ "<p>" + "<img src=cid:" + mensagem.embed(img) + ">" + "</p>" + "</center>" + "<head>" + "<body>"
				+ "<left>" + "<p>" + "<font face=\"Arial\" size=\"2\">"
				+ "&emsp;&emsp;Nova doa��o resgistrada pelo Food Control. Segue abaixo as especifica��es da doa��o."
				+ "</br>" + "</p>" + "</font>" + "<p>"
				+ "</p><p><hr></p><p>&emsp;&emsp;&emsp;&emsp; C�digo da doa��o: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.getCodigo() + "</b></font><hr>"
				+ "</p><p>&emsp;&emsp;&emsp;&emsp; Tipo: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.getTipo() + "</b></font><hr>"
				+ "</p><p>&emsp;&emsp;&emsp;&emsp; Doador: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.getDoador() + "</b></font><hr>"
				+ "</p><p>&emsp;&emsp;&emsp;&emsp; Receptor: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.getReceptor() + "</b></font><hr>"
				+ "</p><p>&emsp;&emsp;&emsp;&emsp; Data: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.printData() + "</b></font><hr>"
				+ "</p><p>&emsp;&emsp;&emsp;&emsp; Quantidade: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.getQnt() + "</b></font><hr>"
				+ "</p><p>&emsp;&emsp;&emsp;&emsp; Descri��o: <font face=\"Arial\" size=\"2\" color = \"#00009b\" > <b>"
				+ doacao.getDesc() + "</b></font><hr>" 

				+ "</p>" + "</left>" + "</body>" + "</html>");
		mensagem.buildMimeMessage();
		mensagem.setDebug(true);
		Message m = mensagem.getMimeMessage();
		Transport transport = mensagem.getMailSession().getTransport("smtp");
		transport.connect(mensagem.getHostName(), settings.getEmailPrograma(), settings.getSenhaEmailPrograma());
		transport.sendMessage(m, m.getAllRecipients());

	}
}
