
package view.forms.utils;

import java.awt.Font;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import model.entity.*;
import javax.swing.ImageIcon;

public class OptionPaneFonts {

	public int getOptionPaneUpdateProduto(JPanel panel, Produtos produto, Validade validade) {
		try {
			Object[] options = { "ATUALIZAR", "APENAS CADASTRAR" };
			UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.PLAIN, 12)));

			String text = "Deseja atualizar o produto?";
				text += "\n\nNome atual: " + produto.getNome() + "\nNome novo: " + validade.getNome();
				return JOptionPane.showOptionDialog(panel, text, "Atualizar Produto", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						new ImageIcon(new File("").getCanonicalPath() + "\\images\\edit.png"), options, options[0]);
				

		} catch (Exception e) {
			return JOptionPane.NO_OPTION;
		}
	}

	public int getOptionPaneEdit(JPanel panel, Validade oldValidade, Validade newValidade) {
		try {
			Object[] options = { "ATUALIZAR AMBOS", "APENAS A VALIDADE" };
			UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.PLAIN, 12)));

			String text = "Deseja atualizar o produto?";
			if (oldValidade.getCodBar().equals(newValidade.getCodBar()) == false) {
				text += "\n\nC�digo de Barras atual: " + oldValidade.getCodBar() + "\nC�digo de Barras novo: "
						+ newValidade.getCodBar();

				if (oldValidade.getNome().equals(newValidade.getNome()) == false) {
					text += "\n\nNome atual: " + oldValidade.getNome() + "\nNome novo: " + newValidade.getNome();
					
					return JOptionPane.showOptionDialog(panel, text, "Atualizar Produto", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							new ImageIcon(new File("").getCanonicalPath() + "\\images\\edit.png"), options, options[0]);
				} else {
						return JOptionPane.showOptionDialog(panel, text, "Atualizar Produto", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE,
								new ImageIcon(new File("").getCanonicalPath() + "\\images\\edit.png"), options,
								options[0]);
					}
				
			} else {
				if (oldValidade.getNome().equals(newValidade.getNome()) == false) {
					text += "\n\nNome atual: " + oldValidade.getNome() + "\nNome novo: " + newValidade.getNome();
					
					return JOptionPane.showOptionDialog(panel, text, "Atualizar Produto", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							new ImageIcon(new File("").getCanonicalPath() + "\\images\\edit.png"), options, options[0]);
				} else {
					
						if (oldValidade.printEndDate().equals(newValidade.printEndDate()) == false) {
							int u = JOptionPane.CANCEL_OPTION;
							return u;
						} else {
							return -1;
						}
					}
				}
			
		} catch (Exception e) {
			return JOptionPane.NO_OPTION;
		}
	}

	public int getOptionPaneExistProduto(JPanel panel, Produtos existProduto) {
		try {
			String text = "O C�digo de Barra " + existProduto.getCodBar() + " j� est� relacionado com o produto: "
					+ "\nNome: " + existProduto.getNome()
					+ ". \n\nDeseja alterar o c�digo de barras?"
					+ " (Esta a��o ir� alterar os produtos j� cadastrados.)";
			Object[] options = { "ALTERAR PRODUTO", "IGNORAR NOME" };
			UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.PLAIN, 12)));

			return JOptionPane.showOptionDialog(panel, text, "Atualizar Produto", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE, new ImageIcon(new File("").getCanonicalPath() + "\\images\\edit.png"),
					options, options[0]);
		} catch (Exception e) {
			return JOptionPane.NO_OPTION;
		}
	}
}