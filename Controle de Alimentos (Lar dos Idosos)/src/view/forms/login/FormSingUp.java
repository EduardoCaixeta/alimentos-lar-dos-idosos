package view.forms.login;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.SettingsController;
import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.Login;
import view.forms.utils.SendEmailConfirm;
import view.panels.login.PanelConfirmEmail;
import view.panels.login.PanelSingUp;

public class FormSingUp {

	private PanelSingUp panel;
	private UsersController controllerUser;
	private JFrame frame;
	private String passwordDesenvolver;
	private String emailPrimitive, senhaPrimitive;
	private Database dS;

	@SuppressWarnings("deprecation")
	public FormSingUp(Database dU, JFrame frame, String passwordDesenvolver, Database dS, String emailPrimitive,
			String senhaPrimitive) throws Exception {
		this.panel = new PanelSingUp();
		this.dS = dS;
		this.emailPrimitive = emailPrimitive;
		this.senhaPrimitive = senhaPrimitive;
		controllerUser = new UsersController(dU);
		this.passwordDesenvolver = passwordDesenvolver;
		this.frame = frame;

		try {
			panel.getTxtUsuario().addActionListener(e -> {
				if (panel.getTxtUsuario().getText().isEmpty())
					panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				else {
					panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
					panel.getTxtSenha().requestFocus();
				}
			});
			panel.getTxtSenha().addActionListener(e -> {
				if (panel.getTxtSenha().getText().isEmpty())
					panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				else {
					panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
					panel.getTxtConfSenha().requestFocus();
				}
			});
			panel.getTxtConfSenha().addActionListener(e -> {
				if (panel.getTxtConfSenha().getText().isEmpty())
					panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				else {
					panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
					panel.getTxtEmail().requestFocus();
				}
			});
			panel.getTxtEmail().addActionListener(e -> singUp_Click());

			panel.getButtonSingUp().addActionListener(e -> singUp_Click());
			panel.getButtonSingUp().setMnemonic(KeyEvent.VK_S);
			panel.getButtonSingUp().addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						panel.getButtonSingUp().doClick();
					}
				}
			});

			panel.getButtonVoltar().addActionListener(e -> voltar_Click());
			panel.getButtonVoltar().setMnemonic(KeyEvent.VK_S);
			panel.getButtonVoltar().addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						panel.getButtonVoltar().doClick();
					}
				}
			});
		} catch (Exception e) {
			JOptionPane.showConfirmDialog(frame, e.getMessage());
		}

	}

	public void voltar_Click() {

		Login.setPanel(frame, new FormLogin(controllerUser.getDatabase(), frame, passwordDesenvolver, dS,
				emailPrimitive, senhaPrimitive).getPanel());
	}

	String codigo;
	public void singUp_Click() {
		try {
			Users user = getDadosSingUp();
			codigo = geraCod();
			sendCod(panel.getTxtEmail().getText(), codigo);

			@SuppressWarnings("serial")
			PanelConfirmEmail confirm = new PanelConfirmEmail(frame, panel.getTxtEmail().getText()) {
				public void confirm_Click() {
					try {
						this.getTxtCod().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
						if (codigo.equals(this.getTxtCod().getText())) {
							user.setEmail(panel.getTxtEmail().getText());
							controllerUser.create(user);
							JOptionPane.showMessageDialog(frame, "Cadastro realizado com sucesso.",
									"CADASTRO REALIZADO.", JOptionPane.INFORMATION_MESSAGE);
							frame.getContentPane().removeAll();
							this.cancel_Click();
							voltar_Click();
							
						} else {
							this.getTxtCod().setText("");
							this.getTxtCod().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));

						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado.",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
				
				public void send_Click()
				{
					try {
						codigo = geraCod();
						sendCod(panel.getTxtEmail().getText(), codigo);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado.",
								JOptionPane.INFORMATION_MESSAGE);
					}
				}

			};
			confirm.getLabelEmail().setText(panel.getTxtEmail().getText().toLowerCase());
		} catch (Exception e) {
			if (e.getMessage().toUpperCase().equals("~") == false)
				JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado.",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	
	public boolean isValiidadEmail(String email) {

		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public boolean isGmail(String email) {
		String host = email.substring(email.length() - 10, email.length());
		if (host.toUpperCase().equals("@GMAIL.COM"))
			return true;
		else
			return false;
	}

	public void sendCod(String email, String cod) throws Exception {
		new Thread(new Runnable() {
			public void run() {
				try {
					SettingsController controllerSettings = new SettingsController(dS);
					if (controllerSettings.isEmailSetado() == false) {
						controllerSettings.getSettings().setEmailPrograma(emailPrimitive);
						controllerSettings.getSettings().setSenhaEmailPrograma(senhaPrimitive);
						controllerSettings.update();

					}

					if (controllerSettings.isBackupSetado() == false) {
						controllerSettings.getSettings().setEmailBacukp(emailPrimitive);
						controllerSettings.update();
					}
					new SendEmailConfirm(email, cod, dS);
				} catch (Exception e) {
					JOptionPane.showConfirmDialog(frame, e.getMessage(), null, JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}).start();;
	}
	
	public String geraCod()
	{
		String cod = "";
		for(int i = 0;  i < 5; i++)
		{
			cod += String.valueOf(geraChar());
		}
		return cod;
	}

	public Character geraChar() {
			int alg = new Random().nextInt();
			Character c = String.valueOf(alg % 10).charAt(0);
			if(c<'0' || c>'9') c = geraChar();

		
		return c;
	}

	public void confirmEmail(PanelConfirmEmail confirm, String codigo, Users user) {

		confirm.getTxtCod().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (codigo.equals(confirm.getTxtCod().getText())) {
			user.setEmail(panel.getTxtEmail().getText());
			confirm.cancel_Click();
		} else {
			confirm.getTxtCod().setText("");
			confirm.getTxtCod().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));

		}

	}

	@SuppressWarnings("deprecation")
	public Users getDadosSingUp() throws Exception {
		Users user = new Users();
		panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtConfSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtEmail().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (panel.getTxtUsuario().getText().trim().isEmpty() == false) {
			if (panel.getTxtUsuario().getText().length() >= 4 && panel.getTxtUsuario().getText().length() <= 15) {
				if (controllerUser.existUsername(panel.getTxtUsuario().getText()) == false) {
					user.setUsername(panel.getTxtUsuario().getText());
					if (panel.getTxtSenha().getText().isEmpty() == false) {
						if (panel.getTxtSenha().getText().length() >= 5
								&& panel.getTxtSenha().getText().length() <= 12) {
							if (panel.getTxtConfSenha().getText().isEmpty() == false) {
								if (panel.getTxtConfSenha().getText().equals(panel.getTxtSenha().getText())) {
									user.setPassword(panel.getTxtSenha().getText());
									if (panel.getTxtEmail().getText().trim().isEmpty() == false) {
										if (isValiidadEmail(panel.getTxtEmail().getText().trim())) {

											if (controllerUser
													.existEmail(panel.getTxtEmail().getText().trim()) == false) {
												if (isGmail(panel.getTxtEmail().getText())) {
													return user;
												} else {
													panel.getTxtEmail().setBorder(
															BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
													panel.getTxtEmail().requestFocus();
													throw new Exception("O endere�o de email n�o � um GMAIL.");
												}

											} else {
												panel.getTxtEmail().setBorder(
														BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
												panel.getTxtEmail().requestFocus();
												panel.getTxtEmail().setText("");
												throw new Exception("Endere�o de email j� cadastrado.");

											}
										} else {
											panel.getTxtEmail()
													.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
											panel.getTxtEmail().requestFocus();
											throw new Exception("Endere�o de email inv�lido.");
										}
									} else {
										panel.getTxtEmail()
												.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
										panel.getTxtEmail().requestFocus();
										throw new Exception("~");
									}
								} else {
									panel.getTxtConfSenha()
											.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
									panel.getTxtConfSenha().requestFocus();
									panel.getTxtConfSenha().setText("");
									throw new Exception("As senhas n�o conferem.");
								}
							} else {
								panel.getTxtConfSenha()
										.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
								panel.getTxtConfSenha().requestFocus();
								throw new Exception("~");
							}
						} else {
							panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							panel.getTxtSenha().requestFocus();
							panel.getTxtSenha().setText("");
							panel.getTxtConfSenha().setText("");
							throw new Exception("A senha deve conter de 5 a 12 caracteres.");
						}
					} else {
						panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
						panel.getTxtSenha().requestFocus();
						throw new Exception("~");
					}
				} else {
					panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					panel.getTxtUsuario().requestFocus();
					throw new Exception("Nome de usu�rio n� cadastrado.");
				}
			} else {
				panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				throw new Exception("O nome de usu�rio deve conter entre 4 e 15 caracteres.");
			}
		} else

		{
			panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			panel.getTxtUsuario().requestFocus();
			throw new Exception("~");
		}
	}

	public PanelSingUp getPanel() {
		return panel;
	}

	public void setPanel(PanelSingUp panel) {
		this.panel = panel;
	}

	public UsersController getControllerUser() {
		return controllerUser;
	}

	public void setControllerUser(UsersController controllerUser) {
		this.controllerUser = controllerUser;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
