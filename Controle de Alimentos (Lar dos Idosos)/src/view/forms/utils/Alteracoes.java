package view.forms.utils;

import controller.*;
import model.entity.*;
import java.util.List;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import java.awt.Color;
import view.forms.FormConsulta;

public class Alteracoes {
	private ProdutosController controllerProdutos;
	private ValidadeController controllerValidade;
	private List<Validade> listVldd;
	private List<Validade> valoresTable;
	private Consulta_Alterar dialogAltere;

	public Alteracoes(Consulta_Alterar dialogAltere, FormConsulta panelConsulta) {
		this.dialogAltere = dialogAltere;
		try {
			controllerProdutos = new ProdutosController(panelConsulta.getdP());
			controllerValidade = new ValidadeController(panelConsulta.getControllerValidade().getDatabase());
			listVldd = controllerValidade.readAll();
			valoresTable = panelConsulta.getPanel().getModelTable().getValidades();
		} catch (Exception e) {

		}

	}

	public void altere() {
		dialogAltere.getTxtStatus().setText("");
		try {
			Validade newValidade = new Validade();
			Validade oldValidade = dialogAltere.getUpdateValidade();

			newValidade.setId(oldValidade.getId());
			newValidade.setCodBar(dialogAltere.getTxtCodBar().getText());
			newValidade.setNome(dialogAltere.getTxtNome().getText());
			newValidade.setQuantidade(Integer.valueOf(dialogAltere.getSpinnerQntd().getValue().toString()));
			checkEndDate();
			newValidade.setEndDate(stringToDate(dialogAltere.getTxtEndDate().getText()));

			Produtos oldProduto = controllerProdutos.loadForCodBar(oldValidade.getCodBar());
			Produtos newProduto = new Produtos();
			newProduto.setId(oldProduto.getId());
			newProduto.setNome(oldProduto.getNome());
			newProduto.setCodBar(oldProduto.getCodBar());
			boolean upProduto = false;

			if (oldProduto.getNome().equals(newValidade.getNome()) == false) {
				newProduto.setNome(newValidade.getNome());
				upProduto = true;
			}

			if (oldProduto.getCodBar().equals(newValidade.getCodBar()) == false) {
				if (controllerProdutos.existCodBar(newValidade.getCodBar())) {
					Produtos existenteProduto = existProduto(newValidade.getCodBar());

					if ((existenteProduto.getNome().equals(newProduto.getNome()) == false)) {
						int atualize = new OptionPaneFonts().getOptionPaneExistProduto(dialogAltere, existenteProduto);
						if (atualize == JOptionPane.YES_OPTION) {
							for (int i = 0; i < listVldd.size(); i++) {
								existenteProduto.setCodBar(newValidade.getCodBar());
								existenteProduto.setNome(newProduto.getNome());
								controllerProdutos.update(existenteProduto);
								if (listVldd.get(i).getCodBar().equals(oldProduto.getCodBar())
										|| listVldd.get(i).getCodBar().equals(newValidade.getCodBar())) {
									listVldd.get(i).setProduto(existenteProduto);
									controllerValidade.update(listVldd.get(i));
								}

							}
							for (int i = 0; i < valoresTable.size(); i++) {
								if (valoresTable.get(i).getCodBar().equals(oldProduto.getCodBar())
										|| valoresTable.get(i).getCodBar().equals(newValidade.getCodBar())) {
									valoresTable.get(i).setProduto(existenteProduto);
								}
							}
							newProduto.setCodBar(newValidade.getCodBar());
						} else {
							upProduto = true;
							newValidade.setCodBar(oldValidade.getCodBar());
							newProduto.setCodBar(newValidade.getCodBar());
							dialogAltere.getTxtCodBar().setText(newValidade.getCodBar());
						}
					} else {
						upProduto = true;
						newProduto.setCodBar(existenteProduto.getCodBar());
					}
				} else {
					newProduto.setCodBar(newValidade.getCodBar());
					controllerProdutos.create(newProduto);
					upProduto = true;
				}
			}
			if (upProduto) {
				int atualize = new OptionPaneFonts().getOptionPaneEdit(dialogAltere, oldValidade, newValidade);
				if (atualize == JOptionPane.YES_OPTION) {
					if (oldProduto.getCodBar().equals(newProduto.getCodBar()) == false) {
						for (int i = 0; i < listVldd.size(); i++) {
							if (listVldd.get(i).getCodBar().equals(oldProduto.getCodBar())
									|| listVldd.get(i).getCodBar().equals(newProduto.getCodBar())) {
								listVldd.get(i).setProduto(newProduto);
								controllerValidade.update(listVldd.get(i));
							}
						}
						for (int i = 0; i < valoresTable.size(); i++) {
							if (valoresTable.get(i).getCodBar().equals(oldProduto.getCodBar())
									|| valoresTable.get(i).getCodBar().equals(newProduto.getCodBar())) {
								valoresTable.get(i).setProduto(newProduto);
							}
						}
					} else {
						for (int i = 0; i < listVldd.size(); i++) {

							if (listVldd.get(i).getCodBar().equals(oldProduto.getCodBar())) {
								listVldd.get(i).setProduto(newProduto);
								controllerValidade.update(listVldd.get(i));
							}

						}
						for (int i = 0; i < valoresTable.size(); i++) {
							if (valoresTable.get(i).getCodBar().equals(oldProduto.getCodBar())) {
								valoresTable.get(i).setProduto(newProduto);
							}
						}
					}
					controllerProdutos.update(newProduto);
					newValidade.setProduto(newProduto);
				} else {
					if (atualize == JOptionPane.NO_OPTION) {
						newValidade.setProduto(newProduto);
						valoresTable.set(valoresTable.indexOf(oldValidade), newValidade);
					}
				}
			} else
				valoresTable.set(valoresTable.indexOf(oldValidade), newValidade);
			controllerValidade.update(newValidade);
			dialogAltere.close();

		} catch (Exception e) {
			dialogAltere.getTxtStatus().setText(e.getMessage());
		}

	}

	public Date stringToDate(String string) throws Exception {
		Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
		Matcher matcher = dataPadrao.matcher(string);
		if (matcher.matches()) {
			int dia = Integer.parseInt(matcher.group(1));
			int mes = Integer.parseInt(matcher.group(2)) - 1;
			int ano = Integer.parseInt(matcher.group(3));
			return (new GregorianCalendar(ano, mes, dia)).getTime();
		} else
			throw new Exception(" Data final inv�lida.");
	}

	public void checkEndDate() throws Exception {
		dialogAltere.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.BLACK));
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(stringToDate(dialogAltere.getTxtEndDate().getText()));

		try {
			Calendar rgCalendar = Calendar.getInstance();
			endCalendar.add(Calendar.MONTH, 0);
			analiseEndDate(dialogAltere.getTxtEndDate().getText());
			if (endCalendar.before(rgCalendar)) {
				throw new Exception(" Data Inv�lida");
			}
		} catch (Exception e) {
			dialogAltere.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception(e.getMessage());
		}
	}

	public Produtos existProduto(String codBar) throws Exception {
		try {
			if (dialogAltere.getTxtCodBar().getText().isEmpty() == false) {
				List<Produtos> produtosCadastrados = controllerProdutos.readAll();
				for (int i = 0; i < produtosCadastrados.size(); i++) {
					if (produtosCadastrados.get(i).getCodBar().equals(codBar)) {
						return produtosCadastrados.get(i);
					}
				}
				dialogAltere.getTxtNome().setText("");
				throw new Exception("C�digo de barras n�o cadastrado.");
			} else {
				dialogAltere.getTxtCodBar().requestFocus();
				throw new Exception(" Insira o C�digo de Barras.");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void analiseEndDate(String text) throws Exception {
		try {
			int dia = Integer.parseInt(text.substring(0, 2));
			int mes = Integer.parseInt(text.substring(3, 5));
			int ano = Integer.parseInt(text.substring(6, 10));

			int[] meses31 = { 4, 6, 9, 11 };

			if (1 > dia || dia > 31)
				throw new Exception("Dia inv�lido");

			if (1 > mes || mes > 12)
				throw new Exception("M�s inv�lido");
			if (dia == 31) {
				for (int i = 0; i < 4; i++) {
					if (mes == meses31[i]) {
						throw new Exception("Dia inv�lido");
					}
				}
			}
			if (mes == 2) {
				boolean ehBissexto = false;
				if ((ano % 400) == 0) {
					ehBissexto = true;
				} else {
					if (((ano % 4) == 0) && ((ano % 100) != 0)) {
						ehBissexto = true;
					}
				}

				if (ehBissexto) {
					if (dia > 29)
						throw new Exception("Dia inv�lido");
				} else {
					if (dia > 28)
						throw new Exception("Dia inv�lido");
				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}
}
