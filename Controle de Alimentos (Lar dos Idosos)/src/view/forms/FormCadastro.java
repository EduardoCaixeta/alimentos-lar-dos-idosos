package view.forms;

import view.panels.PanelCadastro;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

import controller.*;
import model.Database;
import model.entity.*;
import view.panels.utils.TratamentoDate;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.awt.Color;
import view.forms.utils.*;

public class FormCadastro implements TratamentoDate {
	private PanelCadastro panel;
	private ProdutosController controllerProdutos;
	private ValidadeController controllerValidade;
	private Database dbProduto, dbValidade;
	private String state;
	private List<Validade> valoresTable;
	private List<Validade> listVldd;
	private JFrame frame;

	public FormCadastro(Database dbP, Database dbV, JFrame frame) {
		panel = new PanelCadastro();
		this.frame = frame;
		this.dbProduto = dbP;
		this.dbValidade = dbV;
		try {

			controllerProdutos = new ProdutosController(dbProduto);
			controllerValidade = new ValidadeController(dbValidade);
			listVldd = controllerValidade.readAll();
			valoresTable = new ArrayList<Validade>();
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage().toUpperCase());
		}

		panel.getButtonAdd().addActionListener(e -> add_Click());
		panel.getButtonAdd().setMnemonic(KeyEvent.VK_S);
		panel.getButtonAdd().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonAdd().doClick();
				}
			}
		});

		panel.getButtonCancel().addActionListener(e -> cancel_Click());
		panel.getButtonCancel().setMnemonic(KeyEvent.VK_S);
		panel.getButtonCancel().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonCancel().doClick();
				}
			}
		});

		panel.getButtonConfirm().addActionListener(e -> confirm_Click());
		panel.getButtonConfirm().setMnemonic(KeyEvent.VK_S);
		panel.getButtonConfirm().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonConfirm().doClick();
				}
			}
		});

		panel.getButtonPesquisa().addActionListener(e -> pesquisa_Click(panel.getTxtCodBar().getText()));
		panel.getButtonPesquisa().setMnemonic(KeyEvent.VK_S);
		panel.getButtonPesquisa().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonPesquisa().doClick();

				}
			}
		});

		panel.getButtonEdit().addActionListener(e -> edit_Click());
		panel.getButtonEdit().setMnemonic(KeyEvent.VK_S);
		panel.getButtonEdit().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonEdit().doClick();
				}
			}
		});

		panel.getButtonDelete().addActionListener(e -> delete_Click());
		panel.getButtonDelete().setMnemonic(KeyEvent.VK_S);
		panel.getButtonDelete().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonDelete().doClick();
				}
			}
		});


		panel.getTable().getSelectionModel().addListSelectionListener(e -> transmit_Click());

		panel.getTxtCodBar().addActionListener(e -> codBar_Enter(panel.getTxtCodBar().getText()));

		panel.getTxtNome().addActionListener(e -> nome_Enter());

		panel.getTxtEndDate().addActionListener(e -> endDate_Enter());
		
		panel.getSpinnerQntd().addKeyListener(new KeyAdapter() {
			@SuppressWarnings("deprecation")
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonConfirm().nextFocus();
					panel.getButtonConfirm().doClick();
					
				}
			}
		});
		inicioState();

	}


	public void codBar_Enter(String codBar) {
		panel.getTxtStatus().setText("");
		if (codBar.isEmpty() == false) {
			pesquisa_Click(codBar);
			panel.getTxtCodBar().setBorder(null);
			if (panel.getTxtNome().getText().isEmpty() == false) {
				panel.getTxtNome().setBorder(null);
			}
		} else
			panel.getTxtCodBar().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
	}

	public void nome_Enter() {
		panel.getTxtStatus().setText("");
		if (panel.getTxtNome().getText().isEmpty() == false) {
			panel.getTxtNome().setBorder(null);
			panel.getTxtEndDate().requestFocus();
		} else
			panel.getTxtNome().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
	}

	@SuppressWarnings("deprecation")
	public void endDate_Enter() {
		panel.getTxtStatus().setText("");
		if (panel.getTxtEndDate().getText().isEmpty() == false) {
			try {
				checkEndDate();
				panel.getTxtEndDate().nextFocus();
				
			} catch (Exception e) {
				panel.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				panel.getTxtEndDate().requestFocus();
				panel.getTxtStatus().setText(e.getMessage().toUpperCase());
			}
		} else {
			panel.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			panel.getTxtStatus().setText(" Insira a Data de Validade.");
		}
	}

	public void add_Click() {
		panel.getTxtStatus().setText("");
		clearTextFields();
		panel.getSpinnerQntd().setValue((Object)1);
		panel.getTable().setEnabled(false);
		panel.getButtonAdd().setEnabled(false);
		panel.getButtonConfirm().setVisible(true);
		panel.getButtonCancel().setEnabled(true);
		panel.getButtonEdit().setEnabled(false);
		panel.getButtonPesquisa().setEnabled(true);
		panel.getSpinnerQntd().setEnabled(true);
		panel.getTxtCodBar().setEditable(true);
		panel.getTxtCodBar().requestFocus();
		panel.getTxtNome().setEditable(true);
		panel.getTxtEndDate().setEditable(true);
		panel.getButtonDelete().setEnabled(false);
		if (panel.getTable().getSelectedRowCount() != 0)
			panel.noSelect();
		state = "add";
	}

	public void confirm_Click() {
		panel.getTxtStatus().setText("");
		if (state == "add") {
			try {
				Validade validade = getValidade();
				if (controllerProdutos.existCodBar(validade.getCodBar()) == false) {
					try {
						Produtos produto = new Produtos();
						produto.setCodBar(validade.getCodBar());
						produto.setNome(validade.getNome());
						controllerProdutos.create(produto);
						panel.getTxtStatus().setText(" Produto Cadastrado.");
					} catch (Exception e) {
						panel.getTxtStatus().setText(e.getMessage().toUpperCase());
					}
				}

				Produtos produto = existProduto(validade.getCodBar());
				int atualize;
				if (validade.getNome().equals(produto.getNome()) == false) {
					atualize = new OptionPaneFonts().getOptionPaneUpdateProduto(panel, produto, validade);
					if (atualize == JOptionPane.YES_OPTION) {
						produto.setNome(validade.getNome());
						controllerProdutos.update(produto);
						panel.getTxtStatus().setText("Produto Atualizado.");

						for (int i = 0; i < listVldd.size(); i++) {

							if (listVldd.get(i).getCodBar().equals(produto.getCodBar())) {
								listVldd.get(i).setProduto(produto);
								controllerValidade.update(listVldd.get(i));
							}

						}
						for (int i = 0; i < valoresTable.size(); i++) {
							if (valoresTable.get(i).getCodBar().equals(produto.getCodBar())) {
								valoresTable.get(i).setProduto(produto);
							}
						}
					}
				}

				if ((controllerValidade.existEquals(validade) == -1)) {
					controllerValidade.create(validade);
				} else
					validade.setId(controllerValidade.existEquals(validade));
				valoresTable.add(validade);
				if (panel.getTxtStatus().getText().equals(" Produto Atualizado.")
						|| panel.getTxtStatus().getText().equals(" Produto Cadastrado."))
					panel.getTxtStatus().setText(panel.getTxtStatus().getText() + " Validade Cadastrada.");
				else
				{
					panel.getTxtStatus().setText(" Validade Cadastrada.");
				}
				clearTextFields();
				panel.getSpinnerQntd().setValue((Object) 1);
				updateTable();
				panel.getTxtCodBar().requestFocus();
			} catch (Exception e) {
				panel.getTxtStatus().setText(e.getMessage().toUpperCase());
			}
		} else {
			panel.getTxtStatus().setText("");
			try {
				Validade newValidade = getValidade();
				Validade oldValidade = valoresTable.get(panel.getTable().getSelectedRow());

				newValidade.setId(oldValidade.getId());
				checkEndDate();

				Produtos oldProduto = controllerProdutos.loadForCodBar(oldValidade.getCodBar());
				Produtos newProduto = new Produtos();
				newProduto.setId(oldProduto.getId());
				newProduto.setNome(oldProduto.getNome());
				newProduto.setCodBar(oldProduto.getCodBar());
				boolean upProduto = false;

				if (oldProduto.getNome().equals(newValidade.getNome()) == false) {
					newProduto.setNome(newValidade.getNome());
					upProduto = true;
				}

				if (oldProduto.getCodBar().equals(newValidade.getCodBar()) == false) {
					if (controllerProdutos.existCodBar(newValidade.getCodBar())) {
						Produtos existenteProduto = existProduto(newValidade.getCodBar());

						if ((existenteProduto.getNome().equals(newProduto.getNome()) == false)) {
							int atualize = new OptionPaneFonts().getOptionPaneExistProduto(panel, existenteProduto);
							if (atualize == JOptionPane.YES_OPTION) {
								for (int i = 0; i < listVldd.size(); i++) {
									existenteProduto.setCodBar(newValidade.getCodBar());
									existenteProduto.setNome(newProduto.getNome());
									controllerProdutos.update(existenteProduto);
									if (listVldd.get(i).getCodBar().equals(oldProduto.getCodBar())
											|| listVldd.get(i).getCodBar().equals(newValidade.getCodBar())) {
										listVldd.get(i).setProduto(existenteProduto);
										controllerValidade.update(listVldd.get(i));
									}

								}
								for (int i = 0; i < valoresTable.size(); i++) {
									if (valoresTable.get(i).getCodBar().equals(oldProduto.getCodBar())
											|| valoresTable.get(i).getCodBar().equals(newValidade.getCodBar())) {
										valoresTable.get(i).setProduto(existenteProduto);
									}
								}
								newProduto.setCodBar(newValidade.getCodBar());
							} else {
								upProduto = true;
								newValidade.setCodBar(oldValidade.getCodBar());
								newProduto.setCodBar(newValidade.getCodBar());
								panel.getTxtCodBar().setText(newValidade.getCodBar());
							}
						} else {
							upProduto = true;
							newProduto.setCodBar(existenteProduto.getCodBar());
						}
					} else {
						newProduto.setCodBar(newValidade.getCodBar());
						controllerProdutos.create(newProduto);
						upProduto = true;
					}
				}
				if (upProduto) {
					int atualize = new OptionPaneFonts().getOptionPaneEdit(panel, oldValidade, newValidade);
					if (atualize == JOptionPane.YES_OPTION) {
						if (oldProduto.getCodBar().equals(newProduto.getCodBar()) == false) {
							for (int i = 0; i < listVldd.size(); i++) {
								if (listVldd.get(i).getCodBar().equals(oldProduto.getCodBar())
										|| listVldd.get(i).getCodBar().equals(newProduto.getCodBar())) {
									listVldd.get(i).setProduto(newProduto);
									controllerValidade.update(listVldd.get(i));
								}
							}
							for (int i = 0; i < valoresTable.size(); i++) {
								if (valoresTable.get(i).getCodBar().equals(oldProduto.getCodBar())
										|| valoresTable.get(i).getCodBar().equals(newProduto.getCodBar())) {
									valoresTable.get(i).setProduto(newProduto);
								}
							}
						} else {
							for (int i = 0; i < listVldd.size(); i++) {

								if (listVldd.get(i).getCodBar().equals(oldProduto.getCodBar())) {
									listVldd.get(i).setProduto(newProduto);
									controllerValidade.update(listVldd.get(i));
								}

							}
							for (int i = 0; i < valoresTable.size(); i++) {
								if (valoresTable.get(i).getCodBar().equals(oldProduto.getCodBar())) {
									valoresTable.get(i).setProduto(newProduto);
								}
							}
						}
						controllerProdutos.update(newProduto);
						newValidade.setProduto(newProduto);
					} else {
						if (atualize == JOptionPane.NO_OPTION) {
							newValidade.setProduto(newProduto);
							valoresTable.set(valoresTable.indexOf(oldValidade), newValidade);
						}
					}
				} else
					valoresTable.set(valoresTable.indexOf(oldValidade), newValidade);
				updateTable();
				controllerValidade.update(newValidade);
			} catch (Exception e) {
				panel.getTxtStatus().setText(e.getMessage().toUpperCase());
			}
			cancel_Click();
		}
	}

	public void cancel_Click() {
		panel.getTxtStatus().setText("");
		panel.getButtonAdd().setEnabled(true);
		panel.getTable().setEnabled(true);
		panel.getButtonConfirm().setVisible(false);
		panel.getButtonCancel().setEnabled(false);
		panel.getButtonEdit().setEnabled(false);
		panel.getButtonPesquisa().setEnabled(false);
		if (panel.getModelTable().getRowCount() == 0) {
			panel.getButtonEdit().setEnabled(false);
			panel.getButtonDelete().setEnabled(false);
		} else {
			panel.getButtonEdit().setEnabled(true);
			panel.getButtonDelete().setEnabled(true);
		}
		panel.getSpinnerQntd().setEnabled(false);
		panel.getTxtCodBar().setEditable(false);
		panel.getTxtNome().setEditable(false);
		panel.getTxtEndDate().setEditable(false);
		if (panel.getTable().getSelectedRowCount() != 0)
			panel.noSelect();
		panel.getTxtCodBar().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtNome().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getSpinnerQntd().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.requestFocus();
		clearTextFields();
	}

	public void pesquisa_Click(String codBar) {
		panel.getTxtStatus().setText("");
		try {
			panel.getTxtEndDate().setBorder(null);

			if (panel.getTxtCodBar().getText().isEmpty() == false) {
				try {
					Produtos produto = existProduto(codBar);
					panel.getTxtNome().setText(produto.getNome());
					panel.getSpinnerQntd().setValue((Object) 1);
					panel.getTxtEndDate().requestFocus();
					panel.getTxtNome().setBorder(null);
				} catch (Exception e) {
					panel.getTxtNome().requestFocus();
					panel.getTxtNome().setBorder(null);
					throw new Exception(e.getMessage().toUpperCase());
				}

			} else {
				panel.getTxtCodBar().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				throw new Exception("Insira o c�dido de barras");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage().toUpperCase());
		}
	}

	public void edit_Click() {
		panel.getTxtStatus().setText("");
		if (panel.getTable().getSelectedRow() != -1
				&& panel.getTable().getValueAt(panel.getTable().getSelectedRow(), 0) != null) {
			panel.getButtonAdd().setEnabled(false);
			panel.getButtonConfirm().setVisible(true);
			panel.getButtonCancel().setEnabled(true);
			panel.getButtonEdit().setEnabled(false);
			panel.getButtonPesquisa().setEnabled(true);
			panel.getTxtCodBar().setEditable(true);
			panel.getSpinnerQntd().setEnabled(true);
			panel.getTxtNome().setEditable(true);
			panel.getTxtEndDate().setEditable(true);
			panel.getTable().setEnabled(false);
			panel.getButtonDelete().setEnabled(false);
			state = "edit";
		} else {
			panel.getTxtStatus().setText(" Selecione um cadastro");
		}
	}

	@SuppressWarnings("serial")
	public void delete_Click() {
		panel.getTxtStatus().setText("");
		try {
			if (panel.getTable().getSelectedRowCount() != 0) {

				List<Validade> selecionados = new ArrayList<Validade>();
				for (int i = 0; i < panel.getTable().getSelectedRowCount(); i++) {
					int p = panel.getTable().getSelectedRows()[i];
					if (panel.getTable().getValueAt(p, 0) != null)
						selecionados.add(valoresTable.get(p));
				}
				if (selecionados.size() != 0) {
					@SuppressWarnings("unused")
					OptionPaneDelete delete;
					delete = new OptionPaneDelete(selecionados, frame) {
						@Override
						public void delete_Click() {
							removerValidades(selecionados);
							close();
							panel.noSelect();
						}

						@Override
						public void cancel_Click() {
							close();
							panel.noSelect();
						}
					};
				} else {
					throw new Exception("Selecione um cadastro");
				}
			} else {
				throw new Exception("Selecione um cadastro");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage().toUpperCase());
		}
	}

	public void removerValidades(List<Validade> selecionados) {
		try {
			for (int i = 0; i < selecionados.size(); i++) {
				valoresTable.remove(valoresTable.indexOf(selecionados.get(i)));
				controllerValidade.delete(selecionados.get(i));
			}
			updateTable();
			clearTextFields();
			if (panel.getTable().getValueAt(0, 0) == null) {
				panel.getButtonEdit().setEnabled(false);
				panel.getButtonDelete().setEnabled(false);
			}
		}

		catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage().toUpperCase());
		}
	}

	public void transmit_Click() {
		panel.getTxtStatus().setText("");
		if (valoresTable.size() != 0) {
			try {
				Validade validade = getValidadeToTable();
				panel.getTxtCodBar().setText(validade.getCodBar());
				panel.getTxtNome().setText(validade.getNome());
				panel.getTxtEndDate().setText(validade.printEndDate());
				panel.getSpinnerQntd().setValue((Object) validade.getQuantidade());
			} catch (Exception e) {
				clearTextFields();
			}

		}
	}

	public Produtos existProduto(String codBar) throws Exception {
		try {
			if (panel.getTxtCodBar().getText().isEmpty() == false) {
				List<Produtos> produtosCadastrados = controllerProdutos.readAll();
				for (int i = 0; i < produtosCadastrados.size(); i++) {
					if (produtosCadastrados.get(i).getCodBar().equals(codBar)) {
						return produtosCadastrados.get(i);
					}
				}
				panel.getTxtNome().setText("");
				throw new Exception("C�digo de barras n�o cadastrado.");
			} else {
				panel.getTxtCodBar().requestFocus();
				throw new Exception(" Insira o C�digo de Barras.");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}
	}

	public JPanel getPanel() {
		return panel;
	}

	public PanelCadastro getPanelCadastro() {
		return panel;
	}
	public void updateTable() {
		if (valoresTable.size() >= panel.getTable().getRowCount()) {
			for (int i = 0; i < valoresTable.size(); i++) {
				if (i < panel.getTable().getRowCount())
					panel.getModelTable().removeRow(i);
				addToTable(valoresTable.get(i));
			}
		} else {
			for (int i = 0; i < valoresTable.size(); i++) {
				if (i < panel.getTable().getRowCount())
					panel.getModelTable().removeRow(i);
				addToTable(valoresTable.get(i));
			}
			int p = panel.getTable().getRowCount();
			for (int i = valoresTable.size(); i < p; i++) {
				panel.getModelTable().removeRow(panel.getTable().getRowCount() - 1);
			}
		}
	}

	public void inicioState() {
		panel.initComponents();
		clearTextFields();
		panel.getButtonAdd().setEnabled(true);
		panel.getButtonDelete().setEnabled(false);
		panel.getButtonConfirm().setVisible(false);
		panel.getButtonCancel().setEnabled(false);
		panel.getButtonEdit().setEnabled(false);
		panel.getButtonPesquisa().setEnabled(false);
		panel.getSpinnerQntd().setEnabled(false);
		panel.getTxtCodBar().setEditable(false);
		panel.getTxtNome().setEditable(false);
		panel.getTxtEndDate().setEditable(false);
		panel.getTable().setEnabled(true);
		panel.getTxtCodBar().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtNome().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getSpinnerQntd().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
	}

	private void clearTextFields() {
		panel.getTxtCodBar().setText("");
		panel.getTxtNome().setText("");
		panel.getTxtEndDate().setText("");
		panel.getSpinnerQntd().setValue((Object) 0);
	}

	public Date stringToDate(String string) throws Exception {
		Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
		Matcher matcher = dataPadrao.matcher(string);
		if (matcher.matches()) {
			int dia = Integer.parseInt(matcher.group(1));
			int mes = Integer.parseInt(matcher.group(2));
			int ano = Integer.parseInt(matcher.group(3));
			return (new GregorianCalendar(ano, mes, dia)).getTime();
		} else
			throw new Exception(" Data final inv�lida.");
	}

	public boolean depoisQue(Calendar date1, Calendar date2) {
		if ((0 < date1.get(Calendar.MONTH) && date1.get(Calendar.MONTH) < 13) == false)
			return true;
		int[] mes31 = { 4, 6, 9, 11 };
		for (int i = 0; i < 4; i++) {
			if (date1.get(Calendar.MONTH) == mes31[i]) {
				if ((0 < date1.get(Calendar.DAY_OF_MONTH) && date1.get(Calendar.DAY_OF_MONTH) < 32) == false)
					return true;
			}
		}

		if (date1.get(Calendar.MONTH) == 2) {
			if ((0 < date1.get(Calendar.DAY_OF_MONTH) && date1.get(Calendar.DAY_OF_MONTH) < 29) == false)
				return true;
		} else {
			if ((0 < date1.get(Calendar.DAY_OF_MONTH) && date1.get(Calendar.DAY_OF_MONTH) < 31) == false)
				return true;
		}

		if (date1.get(Calendar.YEAR) > date2.get(Calendar.YEAR))
			return true;
		if (date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR)) {
			if (date1.get(Calendar.MONTH) > date2.get(Calendar.MONTH))
				return true;
			if (date1.get(Calendar.MONTH) == date2.get(Calendar.MONTH))
				if (date1.get(Calendar.DAY_OF_MONTH) >= date2.get(Calendar.DAY_OF_MONTH))
					return true;
		}
		return false;
	}

	public Validade getValidade() throws Exception {
		Produtos produto = new Produtos();
		Validade validade = new Validade();
		panel.getTxtCodBar().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtNome().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getSpinnerQntd().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (panel.getTxtCodBar().getText().isEmpty() == false) {
			produto.setCodBar(panel.getTxtCodBar().getText());
		} else {
			panel.getTxtCodBar().requestFocus();
			panel.getTxtCodBar().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception(" Insira o c�digo de barras.");
		}

		if (panel.getTxtNome().getText().isEmpty() == false) {
			produto.setNome(panel.getTxtNome().getText());
		} else {
			panel.getTxtNome().requestFocus();
			panel.getTxtNome().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception(" Insira o nome.");
		}
			validade.setQuantidade(Integer.valueOf((int)panel.getSpinnerQntd().getValue()));
		

		validade.setProduto(produto);

		if (panel.getTxtEndDate().getText().trim().isEmpty() == false) {

			checkEndDate();
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(stringToDate(panel.getTxtEndDate().getText()));
			endDate.add(Calendar.MONTH, -1);
			validade.setEndDate(endDate.getTime());
		} else {
			panel.getTxtEndDate().requestFocus();
			panel.getTxtEndDate().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception(" Insira a data de validade.");
		}
		return validade;
	}

	public void checkEndDate() throws Exception {
		panel.getTxtEndDate().setBorder(null);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(stringToDate(panel.getTxtEndDate().getText()));

		try {
			Calendar rgCalendar = Calendar.getInstance();
			endCalendar.add(Calendar.MONTH, -1);
			analiseEndDate(panel.getTxtEndDate().getText());
			if (endCalendar.before(rgCalendar)) {
				throw new Exception(" Data Inv�lida");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}
	}

	public void analiseEndDate(String text) throws Exception {
		try {
			int dia = Integer.parseInt(text.substring(0, 2));
			int mes = Integer.parseInt(text.substring(3, 5));
			int ano = Integer.parseInt(text.substring(6, 10));

			int[] meses31 = { 4, 6, 9, 11 };

			if (1 > dia || dia > 31)
				throw new Exception("Dia inv�lido");

			if (1 > mes || mes > 12)
				throw new Exception("M�s inv�lido");
			if (dia == 31) {
				for (int i = 0; i < 4; i++) {
					if (mes == meses31[i]) {
						throw new Exception("Dia inv�lido");
					}
				}
			}
			if (mes == 2) {
				boolean ehBissexto = false;
				if ((ano % 400) == 0) {
					ehBissexto = true;
				} else {
					if (((ano % 4) == 0) && ((ano % 100) != 0)) {
						ehBissexto = true;
					}
				}

				if (ehBissexto) {
					if (dia > 29)
						throw new Exception("Dia inv�lido");
				} else {
					if (dia > 28)
						throw new Exception("Dia inv�lido");
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}

	}

	public void addToTable(Validade validade) {
		if (valoresTable.indexOf(validade) < panel.getTable().getRowCount()) {

			panel.getModelTable().insertRow(valoresTable.indexOf(validade),
					new Object[] { validade.getNome(), validade.getQuantidade(), validade.printEndDate() });
		} else {
			panel.getModelTable()
					.addRow(new Object[] { validade.getNome(), validade.getQuantidade(), validade.printEndDate() });
		}
	}


	public Validade getValidadeToTable() throws Exception {
		int o = panel.getTable().getSelectedRow();
		if (o != -1) {
			Validade validade = valoresTable.get(o);
			return validade;
		}
		throw new Exception();
	}

	public int diffInDays(String d1, String d2) {
		return -1;
	}

}
