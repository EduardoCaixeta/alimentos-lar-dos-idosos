package view.panels.login;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import view.format.Limite_digitos;
import view.panels.utils.UpperCaseField_Limitado;

@SuppressWarnings("serial")
public class PanelLogin extends JPanel {

	public PanelLogin() {
		initComponents();
		manipularComponents();
	}

	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon(new File("").getCanonicalPath()+ "\\images\\FundoLogin.jpeg").getImage(), 0, 0, this.getWidth(),this.getHeight(),this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initComponents() {

		txtUsuario = new UpperCaseField_Limitado(20,"[^a-zA-Z0-9_]",false,true) {
			@Override
			public String getText()
			{
				return super.getText().toUpperCase();
			}
		};
		labelUser = new JLabel();
		labelSenha = new JLabel();
		buttonLogin = new JButton();
		txtSenha = new JPasswordField();
		buttonSingUp = new JButton();
		buttonHelpPass = new JButton();
		
		txtSenha.setDocument(new Limite_digitos(15,""));
		txtUsuario.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtUsuario.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

		txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		labelUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelUser.setText("Usu�rio:");
		labelUser.setForeground(Color.WHITE);

		labelSenha.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelSenha.setText("Senha:");
		labelSenha.setForeground(Color.WHITE);

		buttonLogin.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
		buttonLogin.setText("Entrar");
		buttonLogin.setForeground(new Color(100,255,100));

		buttonHelpPass.setFont(new java.awt.Font("Arial", 1, 10)); // NOI18N
		buttonHelpPass.setForeground(new java.awt.Color(0, 0, 204));
		buttonHelpPass.setText("ESQUECI MINHA SENHA");
		buttonHelpPass.setForeground(Color.RED);

		buttonSingUp.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
		buttonSingUp.setText("Cadastrar");
		buttonSingUp.setForeground(Color.WHITE);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(38, Short.MAX_VALUE)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelUser)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelSenha)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                .addContainerGap(38, Short.MAX_VALUE))
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(buttonLogin)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(buttonSingUp)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(buttonHelpPass, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );
		layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGap(37, 37, 37)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(labelUser))
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelSenha)
	                    .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(20, 20, 20)
	                .addComponent(buttonLogin)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGap(17, 17, 17)
	                .addComponent(buttonSingUp)
	                .addGap(25, 25, 25)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(buttonHelpPass, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap())
	        );
	}// </editor-fold>

	public void manipularComponents()
    {
        Component components[] = getComponents();

        for (Component c: components)
        {
            if (c instanceof JTextField)
            {
                ((JTextField) c).setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            	((JTextField) c).setCursor(new Cursor(Cursor.TEXT_CURSOR));
            }
            
            if (c instanceof JButton)
            {
                ((JButton) c).setBorder(null);
                ((JButton) c).setContentAreaFilled(false);
                ((JButton) c).setCursor(new Cursor(Cursor.HAND_CURSOR));
                ((JButton) c).setDisabledIcon(null);
                ((JButton) c).setFocusCycleRoot(true);
                ((JButton) c).setFocusPainted(false);
            	
            }
        }   
    }
	
	// Variables declaration - do not modify
	private JButton buttonLogin;
	private JLabel labelUser;
	private JLabel labelSenha;
	private JPasswordField txtSenha;
	private JTextField txtUsuario;
	private JButton buttonSingUp;
	private JButton buttonHelpPass;
	// End of variables declaration

	public JButton getButtonLogin() {
		return buttonLogin;
	}

	public JButton getButtonHelpPass() {
		return buttonHelpPass;
	}

	public void setButtonHelpPass(JButton buttonHelpPass) {
		this.buttonHelpPass = buttonHelpPass;
	}

	public void setButtonLogin(JButton buttonLogin) {
		this.buttonLogin = buttonLogin;
	}

	public JLabel getLabelUser() {
		return labelUser;
	}

	public void setLabelUser(JLabel labelUser) {
		this.labelUser = labelUser;
	}

	public JLabel getLabelSenha() {
		return labelSenha;
	}

	public void setLabelSenha(JLabel labelSenha) {
		this.labelSenha = labelSenha;
	}

	public JPasswordField getTxtSenha() {
		return txtSenha;
	}

	public void setTxtSenha(JPasswordField txtSenha) {
		this.txtSenha = txtSenha;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public JButton getButtonSingUp() {
		return buttonSingUp;
	}

	public void setButtonSingUp(JButton buttonSingUp) {
		this.buttonSingUp = buttonSingUp;
	}
}
