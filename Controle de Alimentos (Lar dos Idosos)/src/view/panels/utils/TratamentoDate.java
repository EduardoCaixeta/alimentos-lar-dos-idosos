package view.panels.utils;

import java.util.Date;

public interface TratamentoDate {
	public abstract int diffInDays(String d1, String d2) throws Exception;

	public abstract Date stringToDate(String string) throws Exception;

	public void analiseEndDate(String text) throws Exception;

}
