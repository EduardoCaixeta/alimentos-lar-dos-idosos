package view.forms.utils;

import java.awt.Color;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;


@SuppressWarnings("serial")
public class OptionConfirmDoacao extends JPanel {

	public OptionConfirmDoacao(String cod, JFrame frame) {
		this.cod = cod;
		initComponents();
		this.bloq = frame;
		showFrame();
	}

	public void showFrame() {
		optionPane = new JDialog(bloq);
		optionPane.add(this);
		optionPane.setSize(340, 210);
		optionPane.setModal(true);
		optionPane.setLocationRelativeTo(bloq);
		optionPane.setResizable(false);
		optionPane.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		optionPane.setLocation(bloq.getX() + bloq.getWidth() / 2 - optionPane.getWidth() / 2,
				bloq.getY() + bloq.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}

	public void close() {
		optionPane.dispose();
	}
	
	public void confirm_Click() {}

	private void initComponents() {

		labelTitulo = new javax.swing.JLabel();
		labelPassword = new javax.swing.JLabel();
		labelCod = new javax.swing.JLabel();
		txtCod = new javax.swing.JTextField();
		txtPassword = new javax.swing.JPasswordField();
		buttonConfirm = new javax.swing.JButton();
		buttonCancel = new javax.swing.JButton();

		labelTitulo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
		labelTitulo.setText("CONFIRMAR DOA��O");

		labelPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelPassword.setText("Senha:");

		labelCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelCod.setText("C�digo de doa��o:");

		txtCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtCod.setText(cod);
		txtCod.setEditable(false);
		txtCod.setFocusable(false);
		txtCod.setBackground(new Color(240,240,240));

		buttonConfirm.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonConfirm.setText("CONFIRMAR");
		buttonConfirm.addActionListener(e -> confirm_Click());

		buttonCancel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonCancel.setText("CANCELAR");
		buttonCancel.addActionListener(e -> close());

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE).addComponent(labelTitulo)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup().addGap(39, 39, 39).addComponent(labelPassword)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 120,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createSequentialGroup().addComponent(labelCod)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, 140,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonConfirm).addGap(18, 18, 18).addComponent(buttonCancel)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap().addComponent(labelTitulo).addGap(21, 21, 21)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(labelCod)
						.addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelPassword).addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(21, 21, 21)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(buttonConfirm).addComponent(buttonCancel))
				.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JButton buttonConfirm;
	private javax.swing.JButton buttonCancel;
	private javax.swing.JLabel labelCod;
	private javax.swing.JLabel labelPassword;
	private javax.swing.JLabel labelTitulo;
	private javax.swing.JTextField txtCod;
	private javax.swing.JPasswordField txtPassword;
	private String cod;
	private JFrame bloq;
	private JDialog optionPane;
	// End of variables declaration
	public javax.swing.JButton getButtonConfirm() {
		return buttonConfirm;
	}

	public void setButtonConfirm(javax.swing.JButton buttonConfirm) {
		this.buttonConfirm = buttonConfirm;
	}

	public javax.swing.JButton getButtonCancel() {
		return buttonCancel;
	}

	public void setButtonCancel(javax.swing.JButton buttonCancel) {
		this.buttonCancel = buttonCancel;
	}

	public javax.swing.JLabel getLabelCod() {
		return labelCod;
	}

	public void setLabelCod(javax.swing.JLabel labelCod) {
		this.labelCod = labelCod;
	}

	public javax.swing.JLabel getLabelPassword() {
		return labelPassword;
	}

	public void setLabelPassword(javax.swing.JLabel labelPassword) {
		this.labelPassword = labelPassword;
	}

	public javax.swing.JLabel getLabelTitulo() {
		return labelTitulo;
	}

	public void setLabelTitulo(javax.swing.JLabel labelTitulo) {
		this.labelTitulo = labelTitulo;
	}

	public javax.swing.JTextField getTxtCod() {
		return txtCod;
	}

	public void setTxtCod(javax.swing.JTextField txtCod) {
		this.txtCod = txtCod;
	}

	public javax.swing.JPasswordField getTxtPassword() {
		return txtPassword;
	}

	public void setTxtPassword(javax.swing.JPasswordField txtPassword) {
		this.txtPassword = txtPassword;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public JFrame getBloq() {
		return bloq;
	}

	public void setBloq(JFrame bloq) {
		this.bloq = bloq;
	}

	public JDialog getOptionPane() {
		return optionPane;
	}

	public void setOptionPane(JDialog optionPane) {
		this.optionPane = optionPane;
	}
}
