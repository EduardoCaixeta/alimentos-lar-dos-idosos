package view;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.io.File;

import controller.SettingsController;
import controller.UsersController;
import model.Database;
import model.entity.Settings;
import model.entity.Users;
import view.forms.login.FormLogin;
import view.panels.PanelInicializando;

public class Login {

	private static final String PASSWORD_DESENVOLVER = "DENISEMOREIRACARDOSO";
	private static final String nameDBS = "settings.db";
	private static final String nameDBU = "users.db";
	public static final String nameApp = "Food Control";
	public static final String emailPrograma = "gerenciador.validades@gmail.com";
	public static final String senhaEmailPrograma = "gerenciador123";
	public boolean fristLog = true;
	JPanel singUp;
	private JPanel cards;
	JPanel login;
	JPanel helpPass;
	JFrame frame;
	public static Database dS = new Database(nameDBS);
	Database dU;
	public Users user;

	
	public Login() {
		try {
			dU = new Database(nameDBU);
			frame = new JFrame(nameApp);
			login = new FormLogin(dU, frame, PASSWORD_DESENVOLVER, dS, emailPrograma, senhaEmailPrograma).getPanel();
			frame.setSize(320, 250);
			frame.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent evt) {
					try {
						
						System.exit(0);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, e.getMessage());
					}
				}
			});
			setPanel(frame, login);
			frame.setVisible(true);
			
			centerForm(frame);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public static void setPanel(JFrame frame, JPanel panel) {
		try {
			Dimension d = frame.getSize();
			frame.getContentPane().removeAll();
			frame.add(panel);
			frame.pack();
			frame.setSize(d);
			frame.getAccessibleContext().setAccessibleParent(frame);
			frame.setResizable(false);
			try
			{
				Image iconeTitulo = new ImageIcon(new File("").getCanonicalPath()+"\\images\\ico.png").getImage();
				frame.setIconImage(iconeTitulo);
			}
			catch(Exception e)
			{
				
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
	}

	public void centerForm(JFrame frame) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int x = (dim.width - frame.getWidth()) / 2;
		int y = (dim.height - frame.getHeight()) / 2;

		frame.setLocation(x, y);
	}

	public static void main(String args[]) {

		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}

			}
			Settings settings = new SettingsController(new Database(nameDBS)).getSettings();
			if (settings.isStartLogin())
				new Login();
			else {
				Users user = new Users();
				user.setUsername(settings.getUsername());
				user.setPassword(settings.getPassword());
				Users userLog = new UsersController(new Database(nameDBU)).getUser(user.getUsername(), "username");
				if (userLog.getPassword().equals(user.getPassword())) {
					
					PanelInicializando panelInicio = new PanelInicializando();
					JFrame load = new JFrame();
					load.add(panelInicio);
					load.setUndecorated(true);
					load.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					load.pack();
					load.setSize(181, 68);
					App.centerForm(load);
					load.setVisible(true);
					
					JFrame frameApp = new JFrame(nameApp);
					try
					{
						frameApp.setIconImage(new ImageIcon(new File("").getCanonicalPath()+"\\images\\ico.png").getImage());
					}
					catch(Exception e)
					{
						
					}
					frameApp.setExtendedState(JFrame.MAXIMIZED_BOTH);
					App app = new App(userLog, frameApp, nameDBU, PASSWORD_DESENVOLVER, dS ,emailPrograma, senhaEmailPrograma );
					javax.swing.SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							try {
								app.start(frameApp);
								panelInicio.setCarregando(false);
								load.dispose();
							} catch (Exception e) {
								JOptionPane.showMessageDialog(null, e.getMessage(), "Algo deu errado",
										JOptionPane.INFORMATION_MESSAGE);
							}
						}
					});
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR FATAL: " + e.getMessage() + "\nContate o desenvolvedor.");
		}

	}

	public boolean isFristLog() {
		return fristLog;
	}

	public void setFristLog(boolean fristLog) {
		this.fristLog = fristLog;
	}

	public JPanel getSingUp() {
		return singUp;
	}

	public void setSingUp(JPanel singUp) {
		this.singUp = singUp;
	}

	public JPanel getCards() {
		return cards;
	}

	public void setCards(JPanel cards) {
		this.cards = cards;
	}

	public JPanel getLogin() {
		return login;
	}

	public void setLogin(JPanel login) {
		this.login = login;
	}

	public JPanel getHelpPass() {
		return helpPass;
	}

	public void setHelpPass(JPanel helpPass) {
		this.helpPass = helpPass;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public String getNameDBU() {
		return nameDBU;
	}

	public Database getdU() {
		return dU;
	}

	public void setdU(Database dU) {
		this.dU = dU;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}
}
