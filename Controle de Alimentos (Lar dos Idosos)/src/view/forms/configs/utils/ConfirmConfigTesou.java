package view.forms.configs.utils;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import controller.SettingsController;
import view.panels.PanelConfig;

@SuppressWarnings("serial")
public class ConfirmConfigTesou extends JPanel {

	private JFrame bloq;
	private JDialog optionPane;
	private String password;
	private SettingsController controllerSettings;
	private PanelConfig panel;

	public ConfirmConfigTesou(JFrame frame, String password, SettingsController controllerSettings, PanelConfig panel) {
		this.bloq = frame;
		this.password = password;
		this.controllerSettings =controllerSettings;
		this.panel = panel;
		initComponents();
		showFrame();
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(new Color(255, 200, 200));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() {
		optionPane = new JDialog(bloq);
		optionPane.add(this);
		optionPane.setSize(287, 144);
		optionPane.setModal(true);
		optionPane.setUndecorated(true);
		optionPane.setLocationRelativeTo(bloq);
		optionPane.setResizable(false);
		optionPane.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		optionPane.setLocation(bloq.getX() + bloq.getWidth() / 2 - optionPane.getWidth() / 2,
				bloq.getY() + bloq.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}

	public void close() {
		optionPane.dispose();
	}

	@SuppressWarnings("deprecation")
	public void confirm_Click() {
		try {
			if (this.getTxtPasswordUser().getText().isEmpty() == false) {
				this.getTxtPasswordUser().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
				if (this.getTxtPasswordUser().getText().equals(password)) {
					if (this.getTxtPasswordTesou().getText().isEmpty() == false) {
						this.getTxtPasswordTesou().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
						if (this.getTxtPasswordTesou().getText()
								.equals(controllerSettings.getSettings().getPasswordTesouraria())) {
							controllerSettings.getSettings()
									.setSendEmailTesouraria(panel.getCheckSendEmailTesouraria().isSelected());
							controllerSettings.update();
							panel.getTxtStatus().setText("Altera��o realizada com sucesso!");
							close();
						}
						else
						{
							this.getTxtPasswordTesou().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
							JOptionPane.showMessageDialog(optionPane, "Senha da tesouraria incorreta.");
							this.getTxtPasswordTesou().setText("");
						}
					}
					else
					{
						this.getTxtPasswordTesou().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					}
				} else {
					this.getTxtPasswordUser().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					JOptionPane.showMessageDialog(optionPane, "Senha do usu�rio incorreta.");
					this.getTxtPasswordUser().setText("");
				}
			} else {
				this.getTxtPasswordUser().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
			close();
		}
	}

	private void initComponents() {

		labelPasswordUser = new javax.swing.JLabel();
		txtPasswordUser = new javax.swing.JPasswordField();
		buttonConfirm = new javax.swing.JButton();
		buttonConfirm.setCursor(new Cursor(Cursor.HAND_CURSOR));
		buttonVoltar = new javax.swing.JButton();
		buttonVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));

		labelPasswordTesou = new javax.swing.JLabel();
		txtPasswordTesou = new javax.swing.JPasswordField();
		txtPasswordTesou.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

		labelPasswordUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelPasswordUser.setText("Senha do usu�rio:");

		txtPasswordUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtPasswordUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		
		buttonConfirm.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonConfirm.setText("CONFIRMAR");
		buttonConfirm.setFocusable(false);
		buttonConfirm.addActionListener(e -> confirm_Click());

		buttonVoltar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonVoltar.setText("CANCELAR");
		buttonVoltar.setFocusable(false);
		buttonVoltar.addActionListener(e -> close());

		labelPasswordTesou.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelPasswordTesou.setText("Senha da tesouraria:");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addGroup(layout.createSequentialGroup().addComponent(labelPasswordTesou)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtPasswordTesou, javax.swing.GroupLayout.PREFERRED_SIZE, 131,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createSequentialGroup().addComponent(labelPasswordUser)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtPasswordUser, javax.swing.GroupLayout.PREFERRED_SIZE, 140,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createSequentialGroup().addComponent(buttonConfirm).addGap(18, 18, 18)
										.addComponent(buttonVoltar).addGap(12, 12, 12)))
						.addContainerGap(24, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(25, 25, 25)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelPasswordUser)
								.addComponent(txtPasswordUser, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelPasswordTesou)
								.addComponent(txtPasswordTesou, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(23, 23, 23)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(buttonConfirm).addComponent(buttonVoltar))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JButton buttonConfirm;
	private javax.swing.JButton buttonVoltar;
	private javax.swing.JLabel labelPasswordTesou;
	private javax.swing.JLabel labelPasswordUser;
	private javax.swing.JPasswordField txtPasswordTesou;
	private javax.swing.JPasswordField txtPasswordUser;
	// End of variables declaration

	public JFrame getBloq() {
		return bloq;
	}

	public void setBloq(JFrame bloq) {
		this.bloq = bloq;
	}

	public JDialog getOptionPane() {
		return optionPane;
	}

	public void setOptionPane(JDialog optionPane) {
		this.optionPane = optionPane;
	}

	public javax.swing.JButton getButtonConfirm() {
		return buttonConfirm;
	}

	public void setButtonConfirm(javax.swing.JButton buttonConfirm) {
		this.buttonConfirm = buttonConfirm;
	}

	public javax.swing.JButton getButtonVoltar() {
		return buttonVoltar;
	}

	public void setButtonVoltar(javax.swing.JButton buttonVoltar) {
		this.buttonVoltar = buttonVoltar;
	}

	public javax.swing.JLabel getLabelPasswordTesou() {
		return labelPasswordTesou;
	}

	public void setLabelPasswordTesou(javax.swing.JLabel labelPasswordTesou) {
		this.labelPasswordTesou = labelPasswordTesou;
	}

	public javax.swing.JLabel getLabelPasswordUser() {
		return labelPasswordUser;
	}

	public void setLabelPasswordUser(javax.swing.JLabel labelPasswordUser) {
		this.labelPasswordUser = labelPasswordUser;
	}

	public javax.swing.JPasswordField getTxtPasswordTesou() {
		return txtPasswordTesou;
	}

	public void setTxtPasswordTesou(javax.swing.JPasswordField txtPasswordTesou) {
		this.txtPasswordTesou = txtPasswordTesou;
	}

	public javax.swing.JPasswordField getTxtPasswordUser() {
		return txtPasswordUser;
	}

	public void setTxtPasswordUser(javax.swing.JPasswordField txtPasswordUser) {
		this.txtPasswordUser = txtPasswordUser;
	}
}
