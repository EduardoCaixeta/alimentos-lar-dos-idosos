package view.panels;

import java.awt.Graphics;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JViewport;

@SuppressWarnings("serial")
public class PanelHelp extends javax.swing.JPanel {

	/**
	 * Creates new form PanelHelp
	 */
	public PanelHelp() {
		initComponents();
	}

	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon(new File("").getCanonicalPath() + "\\images\\FundoCadHelp.jpeg").getImage(), 0, 0,
					this.getWidth(), this.getHeight(), this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void inicioState() {
		jScrollPane1.getVerticalScrollBar().setValue(0);
		jScrollPane1.setWheelScrollingEnabled(true);
		jScrollPane1.setFocusable(true);
		jScrollPane1.requestFocus();
		jScrollPane1.getVerticalScrollBar().setUnitIncrement(10);
	}                      
	 private void initComponents() {

	        jScrollPane1 = new javax.swing.JScrollPane();
	        jPanel1 = new javax.swing.JPanel();
	        label1 = new javax.swing.JLabel();
	        jScrollPane2 = new javax.swing.JScrollPane();
	        txt1 = new javax.swing.JTextArea();
	        label2 = new javax.swing.JLabel();
	        jScrollPane3 = new javax.swing.JScrollPane();
	        txt2 = new javax.swing.JTextArea();
	        jScrollPane4 = new javax.swing.JScrollPane();
	        txt3 = new javax.swing.JTextArea();
	        label3 = new javax.swing.JLabel();
	        label4 = new javax.swing.JLabel();
	        jScrollPane5 = new javax.swing.JScrollPane();
	        txt4 = new javax.swing.JTextArea();
	        jScrollPane6 = new javax.swing.JScrollPane();
	        txt5 = new javax.swing.JTextArea();
	        label5 = new javax.swing.JLabel();
	        label6 = new javax.swing.JLabel();
	        jScrollPane7 = new javax.swing.JScrollPane();
	        txt6 = new javax.swing.JTextArea();
	        jScrollPane8 = new javax.swing.JScrollPane();
	        txt7 = new javax.swing.JTextArea();
	        label7 = new javax.swing.JLabel();
	        jScrollPane9 = new javax.swing.JScrollPane();
	        txt8 = new javax.swing.JTextArea();
	        label8 = new javax.swing.JLabel();
	        label9 = new javax.swing.JLabel();
	        jScrollPane10 = new javax.swing.JScrollPane();
	        txt9 = new javax.swing.JTextArea();
	        jSeparator1 = new javax.swing.JSeparator();
	        jSeparator2 = new javax.swing.JSeparator();
	        jSeparator4 = new javax.swing.JSeparator();
	        jSeparator5 = new javax.swing.JSeparator();
	        jSeparator6 = new javax.swing.JSeparator();
	        jSeparator7 = new javax.swing.JSeparator();
	        jSeparator8 = new javax.swing.JSeparator();
	        jSeparator9 = new javax.swing.JSeparator();

	        jScrollPane1.setAutoscrolls(true);
	        jScrollPane1.setDoubleBuffered(true);

	        jPanel1.setFocusable(false);
	        jPanel1.setPreferredSize(new java.awt.Dimension(650, 1770));

	        label1.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label1.setText("Como cadastrar uma produto e/ou uma nova validade?");
	        label1.setAlignmentY(10.0F);
	        label1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label1.setFocusable(false);
	        label1.setInheritsPopupMenu(false);
	        label1.setRequestFocusEnabled(false);
	        label1.setVerifyInputWhenFocusTarget(false);

	        jScrollPane2.setBorder(null);
	        jScrollPane2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane2.setFocusable(false);
	        jScrollPane2.setOpaque(false);
	        jScrollPane2.setRequestFocusEnabled(false);
	        jScrollPane2.setVerifyInputWhenFocusTarget(false);

	        txt1.setEditable(false);
	        txt1.setColumns(20);
	        txt1.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt1.setLineWrap(true);
	        txt1.setRows(13);
	        txt1.setTabSize(4);
	        txt1.setText("\tPara se cadastrar um produto o usu�rio deve cadastrar a validade deste, pois o FOOD CONTROL n�o registra produtos mas sim validades. A partir dessa validade registrada FOOD CONTROL ir� extrair o produto relacionado a ela e salv�-lo. Assim quando usu�rio for registrar uma nova validade para este produto, n�o ser� preciso inserir o seu nome novamente apenas a leitura do c�digo de barras.\n\tPara se registrar uma validade o usu�rio deve abrir o menu CADASTRAR\n e clicar no bot�o ADICIONAR com �cone + . Ao fazer isso, os campos ser�o liberados para digita��o e o usu�rio poder� inserir o do c�digo de barras e as demais informa��es. Vale ressaltar que a quantidade produtos deve ser no M�NIMO 1 para que a validade seja registrada e a data de vencimento deve ser de no m�nimo UM DIA a partir da data na qual est� se fazendo o registro da validade.\n\tAp�s preenchidas todas as informa��es, o usu�rio deve clicar no bot�o confirmar.\n\tCaso todas as informa��es estejam corretas e v�lidas, a validade ser� registrada e se o produto relacionado a esta validade ainda n�o estiver arquivado no aplicativo, ele tamb�m ser� salvo.\n\tUma vez  registrada, a validade ir� aparecer na tabela ao lado dos campos foram preenchidos. ");
	        txt1.setToolTipText("");
	        txt1.setWrapStyleWord(true);
	        txt1.setAlignmentY(1.0F);
	        txt1.setBorder(null);
	        txt1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt1.setDoubleBuffered(true);
	        txt1.setDragEnabled(true);
	        txt1.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt1.setMaximumSize(new java.awt.Dimension(2147483647, 518));
	        txt1.setOpaque(false);
	        txt1.setRequestFocusEnabled(false);
	        jScrollPane2.setViewportView(txt1);
	        jScrollPane2.getViewport().setOpaque(false);
	        jScrollPane2.setFocusable(false);
	        jScrollPane2.getViewport().setFocusable(false);
	        

	        label2.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label2.setText("Como alterar uma validade registrada?");
	        label2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label2.setFocusable(false);
	        label2.setInheritsPopupMenu(false);
	        label2.setRequestFocusEnabled(false);
	        label2.setVerifyInputWhenFocusTarget(false);

	        jScrollPane3.setBorder(null);
	        jScrollPane3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane3.setFocusable(false);
	        jScrollPane3.setOpaque(false);
	        jScrollPane3.setRequestFocusEnabled(false);
	        jScrollPane3.setVerifyInputWhenFocusTarget(false);

	        txt2.setEditable(false);
	        txt2.setColumns(20);
	        txt2.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt2.setLineWrap(true);
	        txt2.setRows(10);
	        txt2.setTabSize(4);
	        txt2.setText("\tH� duas formas de se alterar o registro de uma validade. \n\tA primeira delas �, ap�s cadastrar a validade, clicar no bot�o CANCELAR, com �cone (x), e em seguida clicar sobre a linha da tabela onde se encontra a validade qual se deseja alterar. \n\tO bot�o de alterar ser� habilitado e ao clicar nele os campos onde se encontram as especifica��es da validade registrada se tornaram edit�veis, permitindo que o usu�rio fa�a as altera��es necess�rias e ap�s isto clique no bot�o de CONFIRMA��O.\n\tCaso o usu�rio altere o nome do produto ser� pedido uma confirma��o se ele deseja alterar todos as validades registradas com este produto ou apenas para o registro de validade que est� sendo alterada. Caso o usu�rio insira um c�digo de barras relacionados com outro produto, o sistema ent�o ir� alert�-lo sobre tal equ�voco, uma vez que n�o pode haver dois produtos com mesmo c�digo de barra. \n\tSer� dado ent�o duas op��es ao usu�rio, uma delas � ignorar o c�digo de barras alterado e permanecer com c�digo de barras antigo e a outra � atualizar todas as validades, cujo c�digo est� inserido com o c�digo de barras antigo, para um o novo c�digo fazendo com que o produto com o antigo c�digo seja \"exclu�do\".");
	        txt2.setWrapStyleWord(true);
	        txt2.setAlignmentY(0.5F);
	        txt2.setBorder(null);
	        txt2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt2.setDoubleBuffered(true);
	        txt2.setDragEnabled(true);
	        txt2.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt2.setOpaque(false);
	        txt2.setRequestFocusEnabled(false);
	        jScrollPane3.setViewportView(txt2);
	        jScrollPane3.getViewport().setOpaque(false);
	        jScrollPane3.setFocusable(false);
	        jScrollPane3.getViewport().setFocusable(false);

	        jScrollPane4.setBorder(null);
	        jScrollPane4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane4.setFocusable(false);
	        jScrollPane4.setOpaque(false);
	        jScrollPane4.setRequestFocusEnabled(false);
	        jScrollPane4.setVerifyInputWhenFocusTarget(false);

	        txt3.setEditable(false);
	        txt3.setColumns(20);
	        txt3.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt3.setLineWrap(true);
	        txt3.setRows(18);
	        txt3.setTabSize(4);
	        txt3.setText("\tH� tr�s formas de se apagar um registro de validade.\n\tA primeira delas � apaga-l� no painel de cadastro, ap�s cadastrada-l�, clicando na linha da tabela onde se encontra o registro desta qualidade ap�s isso clicando no �cone lixeira.\n\tSer� aberto painel confirmando se voc� deseja realmente deletar a validade, caso confirmado a validade ser� exclu�da do programa.\n\tA segunda forma de se apagar um registro de validade � atrav�s do painel de consulta. Nele o usu�rio deve marcar a validade que desejada e clicar no �cone da lixeira. Ser� aberto ent�o um painel para que o usu�rio confirme a deleta��o. Uma vez confirmada, a validade ser� exclu�da do aplicativo.\n\tA terceira e �ltima forma para deletar um registro � zerando a quantidade de produtos. Para isso o usu�rio deve clicar com o bot�o esquerdo do mouse sobre a linha da tabela onde se encontra o registro e em seguida clicar com o bot�o direito do mouse sobre esta mesma linha tabela. Ser� aberto ent�o um painel com s�mbolo mais e menos ao clicar sobre o s�mbolo +, ser� adicionada uma quantidade a esta validade, e de forma an�loga ser� retirada uma quantidade quando o usu�rio clicar sobre o bot�o - .\n\t Um atalho para diminuir ou aumentar a quantidade de um produto � digitando no teclado o s�mbolo + ou s�mbolo menos respectivamente.\n\tUma vez zerada a quantidade de produtos de uma validade, este cadastro de validade ser� exclu�do.\n\tVale ressaltar que os cadastros de produtos n�o podem ser exclu�dos permanecem sempre dentro do programa");
	        txt3.setWrapStyleWord(true);
	        txt3.setAlignmentY(10.0F);
	        txt3.setBorder(null);
	        txt3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt3.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt3.setDoubleBuffered(true);
	        txt3.setDragEnabled(true);
	        txt3.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt3.setOpaque(false);
	        txt3.setRequestFocusEnabled(false);
	        jScrollPane4.setViewportView(txt3);
	        jScrollPane4.getViewport().setOpaque(false);
	        jScrollPane4.setFocusable(false);
	        jScrollPane4.getViewport().setFocusable(false);

	        label3.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label3.setText("Como apagar o registro de uma validade?");
	        label3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label3.setFocusable(false);
	        label3.setInheritsPopupMenu(false);
	        label3.setRequestFocusEnabled(false);
	        label3.setVerifyInputWhenFocusTarget(false);

	        label4.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label4.setText("Como registrar uma nova doa��o?");
	        label4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label4.setFocusable(false);
	        label4.setInheritsPopupMenu(false);
	        label4.setRequestFocusEnabled(false);
	        label4.setVerifyInputWhenFocusTarget(false);

	        jScrollPane5.setBorder(null);
	        jScrollPane5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane5.setFocusable(false);
	        jScrollPane5.setOpaque(false);
	        jScrollPane5.setRequestFocusEnabled(false);
	        jScrollPane5.setVerifyInputWhenFocusTarget(false);

	        txt4.setEditable(false);
	        txt4.setColumns(20);
	        txt4.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt4.setLineWrap(true);
	        txt4.setRows(19);
	        txt4.setTabSize(4);
	        txt4.setText("\tPara registrar uma nova doa��o o usu�rio deve clicar no menu DOA��ES. Ao fazer isso, ser� perguntado ao usu�rio se ele deseja cadastrar uma NOVA DOA��O ou CONSULTAR uma doa��o j� existente.\n\tNo caso em quest�o, o usu�rio deve clicar no bot�o \"NOVA DOA��O\". Ap�s isso, ser� aberto um formul�rio onde ele dever� preencher conforme as especifica��es da doa��o. \n\tCaso o tipo de doa��o n�o se encontre em entre os dispon�veis no combo, usu�rio dever� escolher a op��o OUTROS e ent�o digitar o tipo de doa��o no campo ao lado do combo. Em seguida dever� ele preencher quem � o doador e em seguida digitar o seu nome para que seja guardada a informa��o de quem recebeu esta doa��o. \n\tAdemais, os outros campos n�o necessitam de explica��o.\n\tAp�s preechido os dados, o usu�rio poder� perceber a presen�a de um check abaixo do campo onde se digita a descri��o da doa��o. \n\tEste check serve para mostrar-se um e-mail com as informa��es da doa��o ser� enviado ou n�o para a tesouraria.\n\tAo clicar no bot�o salvar ser� aberto um novo painel onde o usu�rio dever� inserir a senha de login para que ent�o a doa��o possa ser registrada.\n\t� de extrema import�ncia que o usu�rio confira todos os dados da doa��o pois n�o � poss�vel alterar nenhum dado de doa��o. \n\tUma vez inserida a senha correta a doa��o ser� registrada e um e-mail poderar ou n�o, variando de acordo com checkbox, ser� enviado a tesouraria.");
	        txt4.setWrapStyleWord(true);
	        txt4.setAlignmentY(10.0F);
	        txt4.setBorder(null);
	        txt4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt4.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt4.setDoubleBuffered(true);
	        txt4.setDragEnabled(true);
	        txt4.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt4.setOpaque(false);
	        txt4.setRequestFocusEnabled(false);
	        jScrollPane5.setViewportView(txt4);
	        jScrollPane5.getViewport().setOpaque(false);
	        jScrollPane5.setFocusable(false);
	        jScrollPane5.getViewport().setFocusable(false);

	        jScrollPane6.setBorder(null);
	        jScrollPane6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane6.setFocusable(false);
	        jScrollPane6.setOpaque(false);
	        jScrollPane6.setRequestFocusEnabled(false);
	        jScrollPane6.setVerifyInputWhenFocusTarget(false);

	        txt5.setEditable(false);
	        txt5.setColumns(20);
	        txt5.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt5.setLineWrap(true);
	        txt5.setRows(5);
	        txt5.setTabSize(4);
	        txt5.setText("\tPara se consultar uma doa��oo usu�rio deve clicar no menu doa��es e em seguida no bot�o consultar doa��es. Ser� aberto ent�o um painel onde se encontra todas as doa��es j� registradas.\n\tPara exibir mais detalhes sobre cada doa��o, o usu�rio deve marcar uma ou mais linhas e em seguida clicar com o bot�o direito do mouse, ou nos tr�s pontinhos acima da tabela. Ser� aberto um novo painel naveg�vel com as especifica��es de cada uma.");
	        txt5.setWrapStyleWord(true);
	        txt5.setAlignmentY(10.0F);
	        txt5.setBorder(null);
	        txt5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt5.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt5.setDoubleBuffered(true);
	        txt5.setDragEnabled(true);
	        txt5.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt5.setOpaque(false);
	        txt5.setRequestFocusEnabled(false);
	        jScrollPane6.setViewportView(txt5);
	        jScrollPane6.getViewport().setOpaque(false);
	        jScrollPane6.setFocusable(false);
	        jScrollPane6.getViewport().setFocusable(false);

	        label5.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label5.setText("Como consultar uma doa��o?");
	        label5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label5.setFocusable(false);
	        label5.setInheritsPopupMenu(false);
	        label5.setRequestFocusEnabled(false);
	        label5.setVerifyInputWhenFocusTarget(false);

	        label6.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label6.setText("Como alterar o nome de usu�rio?");
	        label6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label6.setFocusable(false);
	        label6.setInheritsPopupMenu(false);
	        label6.setRequestFocusEnabled(false);
	        label6.setVerifyInputWhenFocusTarget(false);

	        jScrollPane7.setBorder(null);
	        jScrollPane7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane7.setFocusable(false);
	        jScrollPane7.setInheritsPopupMenu(true);
	        jScrollPane7.setOpaque(false);
	        jScrollPane7.setRequestFocusEnabled(false);
	        jScrollPane7.setVerifyInputWhenFocusTarget(false);

	        txt6.setEditable(false);
	        txt6.setColumns(20);
	        txt6.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt6.setLineWrap(true);
	        txt6.setRows(3);
	        txt6.setTabSize(3);
	        txt6.setText("\tPara se alterar o nome de usu�rio deve clicar no menu CONFIGURA��ES e em seguida no campo informa��es de conta deve-se clicar que bot�o ALTERAR NOME de usu�rio. Ser� aberto ent�o um painel onde o usu�rio dever� preencher o seu novo nome e sua senha para que o cadastro possa ser atualizado.");
	        txt6.setWrapStyleWord(true);
	        txt6.setAlignmentY(10.0F);
	        txt6.setBorder(null);
	        txt6.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
	        txt6.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt6.setDoubleBuffered(true);
	        txt6.setDragEnabled(true);
	        txt6.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt6.setOpaque(false);
	        txt6.setRequestFocusEnabled(false);
	        jScrollPane7.setViewportView(txt6);
	        jScrollPane7.getViewport().setOpaque(false);
	        jScrollPane7.setFocusable(false);
	        jScrollPane7.getViewport().setFocusable(false);

	        jScrollPane8.setBorder(null);
	        jScrollPane8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane8.setFocusable(false);
	        jScrollPane8.setInheritsPopupMenu(true);
	        jScrollPane8.setOpaque(false);
	        jScrollPane8.setRequestFocusEnabled(false);
	        jScrollPane8.setVerifyInputWhenFocusTarget(false);

	        txt7.setEditable(false);
	        txt7.setColumns(20);
	        txt7.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt7.setLineWrap(true);
	        txt7.setRows(3);
	        txt7.setTabSize(4);
	        txt7.setText("\tParece alterar a senha, o usu�rio deve clicar no menu CONFIGURA��ES e em seguida na aba informa��es de conta usu�rio deve clicar em ALTERAR SENHA. Ser� aberta ent�o um novo painel onde o usu�rio dever� preencher de acordo com os dados que s�o pedidos e, caso esteja tudo de acordo com padr�o, a senha ser� atualizada.");
	        txt7.setWrapStyleWord(true);
	        txt7.setAlignmentY(10.0F);
	        txt7.setBorder(null);
	        txt7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt7.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt7.setDoubleBuffered(true);
	        txt7.setDragEnabled(true);
	        txt7.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt7.setOpaque(false);
	        txt7.setRequestFocusEnabled(false);
	        jScrollPane8.setViewportView(txt7);
	        jScrollPane8.getViewport().setOpaque(false);
	        jScrollPane8.setFocusable(false);
	        jScrollPane8.getViewport().setFocusable(false);

	        label7.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label7.setText("Como alterar a senha?");
	        label7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label7.setFocusable(false);
	        label7.setRequestFocusEnabled(false);
	        label7.setVerifyInputWhenFocusTarget(false);

	        jScrollPane9.setBorder(null);
	        jScrollPane9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jScrollPane9.setFocusable(false);
	        jScrollPane9.setInheritsPopupMenu(true);
	        jScrollPane9.setOpaque(false);
	        jScrollPane9.setRequestFocusEnabled(false);
	        jScrollPane9.setVerifyInputWhenFocusTarget(false);

	        txt8.setEditable(false);
	        txt8.setColumns(20);
	        txt8.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt8.setLineWrap(true);
	        txt8.setRows(3);
	        txt8.setTabSize(4);
	        txt8.setText("\tPara se alterar o e-mail usu�rio deve clicar no menu CONFIRGURA��ES na aba informa��es de conta em seguida dever� clicar em ALTERAR EMAIL. Ser� aberta ent�o painel onde o usu�rio dever� inserir seu novo e-mail e sua senha.");
	        txt8.setWrapStyleWord(true);
	        txt8.setAlignmentY(10.0F);
	        txt8.setBorder(null);
	        txt8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt8.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt8.setDoubleBuffered(true);
	        txt8.setDragEnabled(true);
	        txt8.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt8.setOpaque(false);
	        txt8.setRequestFocusEnabled(false);
	        jScrollPane9.setViewportView(txt8);
	        jScrollPane9.getViewport().setOpaque(false);
	        jScrollPane9.setFocusable(false);
	        jScrollPane9.getViewport().setFocusable(false);

	        label8.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label8.setText("Como alterar o email?");
	        label8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        label8.setFocusable(false);
	        label8.setRequestFocusEnabled(false);
	        label8.setVerifyInputWhenFocusTarget(false);

	        label9.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
	        label9.setText("Como alterar o email da tesouraria?");
	        label9.setFocusable(false);
	        label9.setRequestFocusEnabled(false);
	        label9.setVerifyInputWhenFocusTarget(false);

	        jScrollPane10.setBorder(null);
	        jScrollPane10.setFocusable(false);
	        jScrollPane10.setInheritsPopupMenu(true);
	        jScrollPane10.setOpaque(false);
	        jScrollPane10.setRequestFocusEnabled(false);
	        jScrollPane10.setVerifyInputWhenFocusTarget(false);

	        txt9.setEditable(false);
	        txt9.setColumns(20);
	        txt9.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
	        txt9.setLineWrap(true);
	        txt9.setRows(5);
	        txt9.setTabSize(4);
	        txt9.setText("\tPara se alterar o email da tesouraria, deve-se ir no painel de CONFIGURA��ES e clicar no bot�o ATALHOS DO DESENVOLVEDOR. Ser� aberto um painel onde o usu�rio deve selecionar a op��o \"setEmailTesouraria\" no combo. Em seguida, inserir o GMAIL da tesouraria do campo ARGUMETOS e, ap�s, a senha da tesouraria.\n\tVale ressaltar que a senha da tesouraria n�o � a senha do desenolvedor.");
	        txt9.setWrapStyleWord(true);
	        txt9.setAlignmentY(0.5F);
	        txt9.setBorder(null);
	        txt9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        txt9.setDisabledTextColor(new java.awt.Color(0, 0, 0));
	        txt9.setDoubleBuffered(true);
	        txt9.setDragEnabled(true);
	        txt9.setMargin(new java.awt.Insets(10, 7, 2, 2));
	        txt9.setOpaque(false);
	        txt9.setRequestFocusEnabled(false);
	        jScrollPane10.setViewportView(txt9);
	        jScrollPane10.getViewport().setOpaque(false);
	        jScrollPane10.setFocusable(false);
	        jScrollPane10.getViewport().setFocusable(false);

	        jSeparator1.setInheritsPopupMenu(true);
	        jSeparator1.setRequestFocusEnabled(false);
	        jSeparator1.setVerifyInputWhenFocusTarget(false);
	        jSeparator1.setOpaque(false);

	        jSeparator2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator2.setInheritsPopupMenu(true);
	        jSeparator2.setRequestFocusEnabled(false);
	        jSeparator2.setVerifyInputWhenFocusTarget(false);
	        jSeparator2.setOpaque(false);

	        jSeparator4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator4.setRequestFocusEnabled(false);
	        jSeparator4.setVerifyInputWhenFocusTarget(false);
	        jSeparator4.setOpaque(false);

	        jSeparator5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator5.setInheritsPopupMenu(true);
	        jSeparator5.setRequestFocusEnabled(false);
	        jSeparator5.setVerifyInputWhenFocusTarget(false);
	        jSeparator5.setOpaque(false);

	        jSeparator6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator6.setRequestFocusEnabled(false);
	        jSeparator6.setVerifyInputWhenFocusTarget(false);
	        jSeparator6.setOpaque(false);

	        jSeparator7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator7.setRequestFocusEnabled(false);
	        jSeparator7.setVerifyInputWhenFocusTarget(false);
	        jSeparator7.setOpaque(false);

	        jSeparator8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator8.setRequestFocusEnabled(false);
	        jSeparator8.setVerifyInputWhenFocusTarget(false);
	        jSeparator8.setOpaque(false);

	        jSeparator9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
	        jSeparator9.setRequestFocusEnabled(false);
	        jSeparator9.setVerifyInputWhenFocusTarget(false);
	        jSeparator9.setOpaque(false);

	        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
	        jPanel1.setLayout(jPanel1Layout);
	        jPanel1Layout.setHorizontalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(42, 42, 42)
	                .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addGap(186, 186, 186))
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(42, 42, 42)
	                .addComponent(label2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addGap(310, 310, 310))
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane4)
	                .addContainerGap())
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane3)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane2)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane5)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane6)
	                .addContainerGap())
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane7)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane8)
	                .addContainerGap())
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane9)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(jScrollPane10)
	                .addContainerGap())
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(42, 42, 42)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(label3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addGap(2, 2, 2)
	                        .addComponent(label9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addGap(46, 46, 46))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(label8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addGap(152, 152, 152))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(label6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addGap(64, 64, 64))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(label7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addGap(146, 146, 146))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(label5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addGap(88, 88, 88))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(label4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addGap(54, 54, 54)))
	                .addGap(286, 286, 286))
	            .addComponent(jSeparator1)
	            .addComponent(jSeparator2)
	            .addComponent(jSeparator4)
	            .addComponent(jSeparator5)
	            .addComponent(jSeparator6)
	            .addComponent(jSeparator7, javax.swing.GroupLayout.Alignment.TRAILING)
	            .addComponent(jSeparator8)
	            .addComponent(jSeparator9)
	        );
	        jPanel1Layout.setVerticalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(16, 16, 16)
	                .addComponent(label1)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label2)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label3)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label4)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label5)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label6)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label7)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label8)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(0, 0, 0)
	                .addComponent(label9)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addContainerGap())
	        );
	        JViewport view = new JViewport();
	        jPanel1.setOpaque(false);
	        view.setView(jPanel1);
	        view.setOpaque(false);
	        jScrollPane1.setViewportView(view);
	        jScrollPane1.getViewport().setOpaque(false);
	        jScrollPane1.setOpaque(false);
	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
	        this.setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
	        );
	    }// </editor-fold>                        


	    // Variables declaration - do not modify                     
	    private javax.swing.JPanel jPanel1;
	    private javax.swing.JScrollPane jScrollPane1;
	    private javax.swing.JScrollPane jScrollPane10;
	    private javax.swing.JScrollPane jScrollPane2;
	    private javax.swing.JScrollPane jScrollPane3;
	    private javax.swing.JScrollPane jScrollPane4;
	    private javax.swing.JScrollPane jScrollPane5;
	    private javax.swing.JScrollPane jScrollPane6;
	    private javax.swing.JScrollPane jScrollPane7;
	    private javax.swing.JScrollPane jScrollPane8;
	    private javax.swing.JScrollPane jScrollPane9;
	    private javax.swing.JSeparator jSeparator1;
	    private javax.swing.JSeparator jSeparator2;
	    private javax.swing.JSeparator jSeparator4;
	    private javax.swing.JSeparator jSeparator5;
	    private javax.swing.JSeparator jSeparator6;
	    private javax.swing.JSeparator jSeparator7;
	    private javax.swing.JSeparator jSeparator8;
	    private javax.swing.JSeparator jSeparator9;
	    private javax.swing.JLabel label1;
	    private javax.swing.JLabel label2;
	    private javax.swing.JLabel label3;
	    private javax.swing.JLabel label4;
	    private javax.swing.JLabel label5;
	    private javax.swing.JLabel label6;
	    private javax.swing.JLabel label7;
	    private javax.swing.JLabel label8;
	    private javax.swing.JLabel label9;
	    private javax.swing.JTextArea txt1;
	    private javax.swing.JTextArea txt2;
	    private javax.swing.JTextArea txt3;
	    private javax.swing.JTextArea txt4;
	    private javax.swing.JTextArea txt5;
	    private javax.swing.JTextArea txt6;
	    private javax.swing.JTextArea txt7;
	    private javax.swing.JTextArea txt8;
	    private javax.swing.JTextArea txt9;
	    // End of variables declaration                   
	}