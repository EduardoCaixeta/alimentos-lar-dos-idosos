package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;

import model.entity.Validade;

import java.util.List;
import java.util.ArrayList;

public class ValidadeDao {
	public static Database database;
	public static Dao<Validade, Integer> dao;
	public Validade validadeCarregada;
	private List<Validade> validadesCarregados;

	public ValidadeDao(Database database) {
		ValidadeDao.setDatabase(database);
		validadesCarregados = new ArrayList<Validade>();
	}

	public static void setDatabase(Database database) {
		ValidadeDao.database = database;
		try {
			dao = DaoManager.createDao(database.getConnection(), Validade.class);
			TableUtils.createTableIfNotExists(database.getConnection(), Validade.class);
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public void create(Validade validade) throws Exception {
		int nrows = 0;
		try {
			nrows = dao.create(validade);
			if (nrows == 0)
				throw new SQLException("Erro ao cadastrar o validade");
			validadeCarregada = validade;
			validadesCarregados.add(validade);
		} catch (SQLException e) {
			throw new Exception();
		}
	}

	public List<Validade> readAll() throws Exception {
		validadesCarregados = dao.queryForAll();
		if (validadesCarregados.size() > 0) {
			int i = 0;
			int k = validadesCarregados.size();
			while (i < k) {
				if (validadesCarregados.get(i).getQuantidade() == 0) {
					delete(validadesCarregados.get(i));
				}
				i++;
			}
			validadesCarregados = dao.queryForAll();
		}
		return validadesCarregados;
	}

	public Validade loadForId(int id) throws Exception {
		return dao.queryForId(id);
	}

	public void update(Validade validade) throws Exception {
		dao.update(validade);
	}

	public int existEquals(Validade validade) throws Exception {
		validadesCarregados = dao.queryForAll();
		for (int i = 0; i < validadesCarregados.size(); i++) {
			if (validade.getCodBar().equals(validadesCarregados.get(i).getCodBar())) {
				if (validade.getNome().equals(validadesCarregados.get(i).getNome()))

				{
					if (validade.getEndDate().equals(validadesCarregados.get(i).getEndDate()))
						return validadesCarregados.get(i).getId();
					;
				}
			}
		}
		return -1;
	}

	public void insertQntd(Validade validade, int more) throws Exception {
		validade.setQuantidade(validade.getQuantidade() + more);

	}

	public void delete(Validade validade) throws Exception {
		dao.deleteById(validade.getId());
	}
}