package model.entity;

import com.j256.ormlite.field.DatabaseField;

public class Users {
	@DatabaseField(generatedId = true)
	int id;

	@DatabaseField
	String username;

	@DatabaseField
	String password;

	@DatabaseField
	String email;

	public int getId() {
		return 3*id+142;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
