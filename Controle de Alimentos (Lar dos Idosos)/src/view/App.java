package view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.UIManager;

import controller.SettingsController;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import view.forms.*;
import view.forms.utils.BackupGmail;
import view.panels.PanelDoacoes;
import view.panels.PanelHelp;
import view.panels.PanelHome;
import model.Database;
import model.entity.Settings;
import model.entity.Users;

public class App {

	private MenuBarra menu;
	private JPanel form;
	private String codUser;
	private String nameDBU;
	private Users user;
	private JPanel cards;
	private String nameDBP;
	private String nameDBV;
	private String nameDBD;
	private String nameDBS;
	private String passwordDesenvolver;
	private String emailPrimitive;
	private String senhaPrimitive;
	private Database dS;
	PanelHome home;

	FormCadastro formCadastro;
	JPanel cadastrar;

	FormConsulta formConsulta;
	JPanel consulta;

	PanelDoacoes formDoacao;
	JPanel doacao;

	FormConfig formConfig;
	JPanel config;

	PanelHelp help;
	public JFrame frame;

	@SuppressWarnings("deprecation")
	public App(Users user, JFrame frame, String nameDBU, String passwordDesenvolver, Database dS,
			String emailPrimitive, String senhaPrimitive) throws Exception {

		this.user = user;
		this.nameDBS = dS.getName();
		this.dS = dS;
		this.passwordDesenvolver = passwordDesenvolver;
		this.nameDBU = nameDBU;
		this.emailPrimitive = emailPrimitive;
		this.senhaPrimitive = senhaPrimitive;
		setDados();

		SettingsController controllerSettings = new SettingsController(new Database(nameDBS));
		if (controllerSettings.isEmailSetado() == false) {
			controllerSettings.getSettings().setEmailPrograma(emailPrimitive);
			controllerSettings.getSettings().setSenhaEmailPrograma(senhaPrimitive);
			controllerSettings.update();

		}

		if (controllerSettings.isBackupSetado() == false) {
			controllerSettings.getSettings().setEmailBacukp(emailPrimitive);
			controllerSettings.update();
		}

		for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("Windows".equals(info.getName())) {
				UIManager.setLookAndFeel(info.getClassName());
				break;
			}
		}

		menu = new MenuBarra();
		for (Component but : menu.getComponents()) {
			if (but instanceof JButton)
				((JButton) but).addActionListener(e -> show(((JButton) but).getLabel()));
		}

		this.frame = frame;

		home = new PanelHome(frame);

		formCadastro = new FormCadastro(new Database(nameDBP), new Database(nameDBV), frame);
		cadastrar = formCadastro.getPanel();

		formConsulta = new FormConsulta(new Database(nameDBV), new Database(nameDBP), nameDBS, frame, codUser, this);
		consulta = formConsulta.getPanel();

		formDoacao = new PanelDoacoes(new Database(nameDBD), new Database(nameDBS), frame, user, this);
		doacao = formDoacao.getPanel();

		formConfig = new FormConfig(frame, user, passwordDesenvolver, nameDBU, nameDBV, nameDBP, nameDBD, nameDBS);
		config = formConfig.getPanel();

		help = new PanelHelp();
		frame.setMinimumSize(new Dimension(724, 564));
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				try {
					frame.dispose();

					SettingsController controllerSettings = new SettingsController(new Database(nameDBS));
					Settings settings = controllerSettings.getSettings();
					if (settings.getEmailBacukp().isEmpty() == false) {
						BackupGmail backup = new BackupGmail(settings.getNomeEmpresa().toUpperCase() + " - BAKCUP", user.getId(),
								controllerSettings.getDatabase().getName());
						backup.conectaEmail(settings.getEmailPrograma(), settings.getSenhaEmailPrograma(),
								"BACKUP DE DATABASES");
						backup.enviaEmailHtml(settings.getEmailBacukp(), "Email de backup para arquivos database");
					}

					System.exit(0);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});

	}

	
	
	public void start(JFrame frame) throws Exception {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		App demo = new App(user, frame, nameDBU, passwordDesenvolver, dS, emailPrimitive, senhaPrimitive);
		demo.addComponentToPane(frame.getContentPane());

		frame.pack();
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		centerForm(frame);
		frame.setVisible(true);
		
	}

	@SuppressWarnings("deprecation")
	public void addComponentToPane(Container pane) {

		cards = new JPanel(new CardLayout());
		cards.add(home, menu.getButtonHome().getLabel());
		cards.add(cadastrar, menu.getButtonCadastrar().getLabel());
		cards.add(consulta, menu.getButtonConsulta().getLabel());
		cards.add(doacao, menu.getButtonDoacao().getLabel());
		cards.add(config, menu.getButtonConfig().getLabel());
		cards.add(help, menu.getButtonHelp().getLabel());

		pane.add(menu, BorderLayout.PAGE_START);
		pane.add(cards, BorderLayout.CENTER);
	}

	@SuppressWarnings("deprecation")
	public void show(String iten) {
		CardLayout cl = (CardLayout) (cards.getLayout());
		if (iten.equals(menu.getButtonCadastrar().getLabel()))
			formCadastro.inicioState();
		else if (iten.equals(menu.getButtonConsulta().getLabel()))
			formConsulta.inicioState();
		else if (iten.equals(menu.getButtonHome().getLabel()))
			home.inicioState();
		else if (iten.equals(menu.getButtonDoacao().getLabel()))
			formDoacao.inicioState();
		else if (iten.equals(menu.getButtonConfig().getLabel()))
			formConfig.inicioState();
		else if (iten.equals(menu.getButtonHelp().getLabel()))
			help.inicioState();

		cl.show(cards, iten);
	}

	public void setDados() {
		this.codUser = String.valueOf(user.getId());
		nameDBP = "Prod" + codUser + ".db";
		nameDBV = "Val" + codUser + ".db";
		nameDBD = "Donation" + codUser + ".db";

	}

	public static void centerForm(JFrame frame) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		int x = (dim.width - frame.getWidth()) / 2;
		int y = (dim.height - frame.getHeight()) / 2;

		frame.setLocation(x, y);
	}

	public MenuBarra getMenuBarra() {
		return this.menu;
	}

	public String getCodUser() {
		return codUser;
	}

	public void setCodUser(String codUser) {
		this.codUser = codUser;
	}

	public JPanel getForm() {
		return form;
	}

	public void setMenuBarra(MenuBarra menuBarra) {
		this.menu = menuBarra;
	}

	public String getNameDBU() {
		return nameDBU;
	}

	public void setNameDBU(String nameDBU) {
		this.nameDBU = nameDBU;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getNameDBP() {
		return nameDBP;
	}

	public String getNameDBV() {
		return nameDBV;
	}

	public String getNameDBD() {
		return nameDBD;
	}

	public void setNameDBD(String nameDBD) {
		this.nameDBD = nameDBD;
	}

	public void setNameDBP(String nameDBP) {
		this.nameDBP = nameDBP;
	}

	public void setNameDBV(String nameDBV) {
		this.nameDBV = nameDBV;
	}

}
