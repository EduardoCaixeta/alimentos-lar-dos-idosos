package view.forms.utils;

import java.io.File;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;

import controller.SettingsController;
import model.Database;
import model.entity.Settings;

public class SendEmailConfirm {

	private String email;
	private Settings settings;
	private String cod;
	public SendEmailConfirm(String email, String cod, Database dS) throws Exception
	{
		this.email = email;
		this.cod = cod;
		settings = new SettingsController(dS).getSettings();
		enviaEmailHtml();
	}
	
	@SuppressWarnings("deprecation")
	public HtmlEmail conectaEmail() throws Exception {
		String hostname = "smtp.gmail.com";
		String username = settings.getEmailPrograma();
		String password = settings.getSenhaEmailPrograma();
		HtmlEmail email = new HtmlEmail();
		email.setHostName(hostname);
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator(username, password));
		email.setTLS(true);
		email.setFrom(username, "Food Control");
		email.setDebug(true);

		email.setCharset(HtmlEmail.ISO_8859_1);
		Properties props = new Properties();
		props.setProperty(hostname, "smtp");
		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		props.setProperty("mail.smtp.port", "" + 587);
		props.setProperty("mail.smtp.starttls.enable", "true");
		Session mailSession = Session.getInstance(props, new DefaultAuthenticator(username, password));
		email.setMailSession(mailSession);
		return email;
	}

	public void enviaEmailHtml() throws Exception {

		HtmlEmail mensagem = conectaEmail();
		mensagem.addTo(email);
		mensagem.setSubject("BEM VINDO(A) AO FOOD CONTROLL.");

		File img = new File("images//velhos.png");
		mensagem.setHtmlMsg("<html>" + "<head>" + "<center>" + "<p> " + "<font face=\"Comics San\" size=\"5\">"
				+ "&emsp;&emsp;CONFIRME SEU ENDER�O DE EMAIL" + "</font>" + "<br /> " + "</p>" + "</center>" + "<center>"
				+ "<p>" + "<img src=cid:" + mensagem.embed(img) + ">" + "</p>" + "</center>" + "<head>" + "<body>"
				+ "<left>" + "<p>" + "<font face=\"Arial\" size=\"2\">"
				+ "&emsp;&emsp;Seu c�digo de confirma��o �:"
				+ "</br>" + "</p>" + "</font>" + "<p>"
				+ "</p><p><hr></p><p>&emsp;&emsp;&emsp;&emsp; <font face=\"Arial\" size=\"3\" C�digo da doa��o: <\font>"
				+ "<font face=\"Arial\" size=\"3\" color = \"#00009b\" > <b>"
				+ cod + "</b></font><hr>"

				+ "</p>" + "</left>" + "</body>" + "</html>");
		mensagem.buildMimeMessage();
		mensagem.setDebug(true);
		Message m = mensagem.getMimeMessage();
		Transport transport = mensagem.getMailSession().getTransport("smtp");
		transport.connect(mensagem.getHostName(), settings.getEmailPrograma(), settings.getSenhaEmailPrograma());
		transport.sendMessage(m, m.getAllRecipients());

	}
	
}
