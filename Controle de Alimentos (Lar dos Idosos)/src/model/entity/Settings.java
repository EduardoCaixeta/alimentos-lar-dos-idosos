package model.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "settings")
public class Settings {

	@DatabaseField(generatedId = true)
	int id;
	
	@DatabaseField
	String emailPrograma;
	
	@DatabaseField
	String senhaEmailPrograma;
	
	@DatabaseField
	String emailTesouraria;
	
	@DatabaseField
	String emailBacukp;
	
	@DatabaseField
	boolean sendEmailTesouraria;
	
	@DatabaseField
	boolean startLogin;
	
	@DatabaseField
	String username;
	
	@DatabaseField
	String password;
	
	@DatabaseField
	String nomeEmpresa;
	
	@DatabaseField
	String helpEmail;

	@DatabaseField
	int diasAlertaVencimento;
	
	 @DatabaseField
	 String passwordTesouraria;
	   
	
	public String getPasswordTesouraria() {
		return passwordTesouraria;
	}

	public void setPasswordTesouraria(String passwordTesouraria) {
		this.passwordTesouraria = passwordTesouraria;
	}

	public int getId() {  
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDiasAlertaVencimento() {
		return diasAlertaVencimento;
	}

	public void setDiasAlertaVencimento(int diasAlertaVencimento) {
		this.diasAlertaVencimento = diasAlertaVencimento;
	}

	public String getEmailPrograma() {
		return emailPrograma;
	}

	public void setEmailPrograma(String emailPrograma) {
		this.emailPrograma = emailPrograma;
	}

	public String getSenhaEmailPrograma() {
		return senhaEmailPrograma;
	}

	public void setSenhaEmailPrograma(String senhaEmailPrograma) {
		this.senhaEmailPrograma = senhaEmailPrograma;
	}

	public String getEmailTesouraria() {
		return emailTesouraria;
	}

	public void setEmailTesouraria(String emailTesouraria) {
		this.emailTesouraria = emailTesouraria;
	}

	public String getEmailBacukp() {
		return emailBacukp;
	}

	public void setEmailBacukp(String emailBacukp) {
		this.emailBacukp = emailBacukp;
	}

	public boolean isSendEmailTesouraria() {
		return sendEmailTesouraria;
	}

	public void setSendEmailTesouraria(boolean sendEmailTesouraria) {
		this.sendEmailTesouraria = sendEmailTesouraria;
	}

	public boolean isStartLogin() {
		return startLogin;
	}

	public void setStartLogin(boolean startLogin) {
		this.startLogin = startLogin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getHelpEmail() {
		return helpEmail;
	}

	public void setHelpEmail(String helpEmail) {
		this.helpEmail = helpEmail;
	}
	
	
	
	
	
	
}
