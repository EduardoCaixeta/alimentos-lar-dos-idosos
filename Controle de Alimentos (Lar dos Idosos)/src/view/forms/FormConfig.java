package view.forms;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;

import controller.DoacaoController;
import controller.ProdutosController;
import controller.SettingsController;
import controller.ValidadeController;
import model.Database;
import model.entity.Settings;
import model.entity.Users;
import view.Login;
import view.forms.configs.ConfigAltEmail;
import view.forms.configs.ConfigAltPassword;
import view.forms.configs.ConfigAltUsername;
import view.forms.configs.ConfigAtalhos;
import view.forms.configs.utils.ConfirmConfigTesou;
import view.forms.configs.utils.ConfirmConfigsAlt;
import view.forms.utils.BackupGmail;
import view.forms.utils.CreatePDF;
import view.panels.PanelConfig;
import view.panels.utils.Ordenacao;

public class FormConfig {
	private PanelConfig panel;
	private ProdutosController controllerProduto;
	private ValidadeController controllerValidade;
	private JFrame frame;
	private String nameDBU, passwordDesenvolver;
	private Users user;
	private Settings settings;
	private SettingsController controllerSettings;
	private DoacaoController controllerDoacao;

	public FormConfig(JFrame frame, Users user, String passwordDesenvolver, String nameDBU, String nameDBV,
			String nameDBP, String nameDBD, String nameDBS) {
		this.panel = new PanelConfig();
		this.passwordDesenvolver = passwordDesenvolver;
		this.frame = frame;
		this.nameDBU = nameDBU;

		this.user = user;
		try {
			controllerSettings = new SettingsController(new Database(nameDBS));
			panel.getSpinnerDays().setModel(
					new SpinnerNumberModel(controllerSettings.getSettings().getDiasAlertaVencimento(), 15, null, 1));
			controllerProduto = new ProdutosController(new Database(nameDBP));
			controllerValidade = new ValidadeController(new Database(nameDBV));
			controllerDoacao = new DoacaoController(new Database(nameDBD));
			settings = controllerSettings.getSettings();
			panel.getCheckSendEmailTesouraria().setSelected(controllerSettings.getSettings().isSendEmailTesouraria());
			panel.getCheckStartLogin().setSelected(controllerSettings.getSettings().isStartLogin());

		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
		panel.getButtonAltUsername().addActionListener(e -> altUsername_Click());
		panel.getButtonAltUsername().setMnemonic(KeyEvent.VK_S);
		panel.getButtonAltUsername().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonAltUsername().doClick();
				}
			}
		});

		panel.getButtonAltPass().addActionListener(e -> altPassword_Click());
		panel.getButtonAltPass().setMnemonic(KeyEvent.VK_S);
		panel.getButtonAltPass().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonAltPass().doClick();
				}
			}
		});

		panel.getButtonAltEmail().addActionListener(e -> altEmail_Click());
		panel.getButtonAltEmail().setMnemonic(KeyEvent.VK_S);
		panel.getButtonAltEmail().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonAltEmail().doClick();
				}
			}
		});

		panel.getButtonPDFVali().addActionListener(e -> pdfVali_Click());
		panel.getButtonPDFVali().setMnemonic(KeyEvent.VK_S);
		panel.getButtonPDFVali().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonPDFVali().doClick();
				}
			}
		});

		panel.getButtonPDFProduto().addActionListener(e -> pdfProd_Click());
		panel.getButtonPDFProduto().setMnemonic(KeyEvent.VK_S);
		panel.getButtonPDFProduto().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonPDFVali().doClick();
				}
			}
		});

		panel.getButtonBackup().addActionListener(e -> backup_Click());
		panel.getButtonBackup().setMnemonic(KeyEvent.VK_S);
		panel.getButtonBackup().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonBackup().doClick();
				}
			}
		});

		panel.getButtonDesenvolver().addActionListener(e -> desenvolver_Click());
		panel.getButtonDesenvolver().setMnemonic(KeyEvent.VK_S);
		panel.getButtonDesenvolver().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonDesenvolver().doClick();
				}
			}
		});

		panel.getButtonPDFDonations().addActionListener(e -> pdfDonations_Click());

		panel.getButtonSave().addActionListener(e -> saveAdicionais_Click());

		panel.getButtonDeslogar().addActionListener(e -> deslog_Click());

		setInformacoes();

		seeQnts();
	}

	public void setInformacoes() {
		try {
			Settings setting = controllerSettings.getSettings();
			panel.getTxtEmailBackup().setText(setting.getEmailBacukp());
			panel.getTxtEmailPrograma().setText(settings.getEmailPrograma());
			if (settings.getEmailTesouraria().isEmpty() == false)
				panel.getTxtEmailTesouraria().setText(settings.getEmailTesouraria());
			else
				panel.getTxtEmailTesouraria().setText("N�O INFORMADO");
			panel.getTxtNameEmpresa().setText(settings.getNomeEmpresa());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public void deslog_Click() {
		try {
			controllerSettings.getSettings().setStartLogin(true);
			controllerSettings.getSettings().setUsername("");
			controllerSettings.getSettings().setPassword("");
			controllerSettings.update();
			frame.dispose();
			Login.main(null);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public void saveAdicionais_Click() {
		try {
			panel.getTxtStatus().setText("");
			boolean alt = false;
			if ((int) panel.getSpinnerDays().getValue() != controllerSettings.getSettings().getDiasAlertaVencimento()
					|| panel.getCheckStartLogin().isSelected() != controllerSettings.getSettings().isStartLogin()) {
				alt = true;
				new ConfirmConfigsAlt(frame, user, controllerSettings, panel);
			}
			if (panel.getCheckSendEmailTesouraria().isSelected() != controllerSettings.getSettings()
					.isSendEmailTesouraria()) {
				alt = true;
				new ConfirmConfigTesou(frame, user.getPassword(), controllerSettings, panel);
			}
			if (alt == false)
				throw new Exception("Nenhuma altera��o detectada.");
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}
	}

	public void desenvolver_Click() {
		try {
			new ConfigAtalhos(frame, controllerSettings, user, passwordDesenvolver, this);
		} catch (Exception e) {

			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public void backup_Click() {
		this.settings = controllerSettings.getSettings();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					int send = JOptionPane.showConfirmDialog(frame, "Deseja fazer o backup dos arquivos Database?",
							"BACUKP", JOptionPane.YES_NO_OPTION);
					if (send == JOptionPane.YES_OPTION) {
						BackupGmail backup = new BackupGmail(settings.getNomeEmpresa().toUpperCase() + " - BAKCUP", user.getId(),
								controllerSettings.getDatabase().getName());
						backup.conectaEmail(settings.getEmailPrograma(), settings.getSenhaEmailPrograma(),
								"BACKUP DE DATABASES");
						backup.enviaEmailHtml(settings.getEmailBacukp(), "Email de backup para arquivos database");
						throw new Exception("BACKUP realizado com sucesso");
					}
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, e.getMessage());
				}

			}
		}).start();;

	}

	public void seeQnts() {
		try {
			panel.getTxtQntProd().setText(String.valueOf(controllerProduto.readAll().size()));
			panel.getTxtQntVali().setText(String.valueOf(controllerValidade.readAll().size()));
			panel.getTxtQntDoacoes().setText(String.valueOf(controllerDoacao.readAll().size()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage().toUpperCase(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void inicioState()
	{
		seeQnts();
		setInformacoes();
	}
	
	public void altUsername_Click() {
		try {
			new ConfigAltUsername(frame, user, nameDBU);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage().toUpperCase(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void altPassword_Click() {
		try {
			new ConfigAltPassword(frame, user, nameDBU);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage().toUpperCase(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void altEmail_Click() {
		try {
			new ConfigAltEmail(frame, user, nameDBU);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage().toUpperCase(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void pdfVali_Click() {
		try {
			new CreatePDF().criarValidades(frame, new Ordenacao().ordeneData(controllerValidade.readAll()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public void pdfProd_Click() {
		try {
			new CreatePDF().criarProdutos(frame, new Ordenacao().ordeneABC(controllerProduto.readAll()));

		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public void pdfDonations_Click() {
		try {
			new CreatePDF().criarDoacao(frame, new Ordenacao().ordeneDoacoes(controllerDoacao.readAll()));

		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage());
		}
	}

	public PanelConfig getPanel() {
		return panel;
	}

	public void setPanel(PanelConfig panel) {
		this.panel = panel;
	}

}
