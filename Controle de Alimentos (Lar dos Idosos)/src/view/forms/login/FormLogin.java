package view.forms.login;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.UsersController;
import model.Database;
import model.entity.Users;
import view.App;
import view.Login;
import view.panels.PanelInicializando;
import view.panels.login.PanelLogin;

public class FormLogin {

	private PanelLogin panel;
	private UsersController controllerUser;
	private JFrame frame;
	private Database dS;
	private String passwordDesenvolver;
	private String emailPrimitive, senhaPrimitive;
	public FormLogin(Database dU, JFrame frame, String passwordDesenvolver, Database dS, String emailPrimitive, String senhaPrimitive) {
		try {
			this.panel = new PanelLogin();
			this.dS = dS;
			this.emailPrimitive= emailPrimitive;
			this.senhaPrimitive = senhaPrimitive;
			this.passwordDesenvolver = passwordDesenvolver;
			controllerUser = new UsersController(dU);
			this.frame = frame;

			panel.getTxtUsuario().addActionListener(e -> {
				if (panel.getTxtUsuario().getText().isEmpty())
					panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				else {
					panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
					panel.getTxtSenha().requestFocus();
				}
			});
			panel.getTxtSenha().addActionListener(e -> login_Click());

			if(controllerUser.qntRegistros()>0)
				panel.getButtonSingUp().setEnabled(false);
			
			panel.getButtonLogin().addActionListener(e -> login_Click());
			panel.getButtonLogin().setMnemonic(KeyEvent.VK_S);
			panel.getButtonLogin().addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						panel.getButtonLogin().doClick();
					}
				}
			});

			panel.getButtonSingUp().addActionListener(e -> singUp_Click());
			panel.getButtonSingUp().setMnemonic(KeyEvent.VK_S);
			panel.getButtonSingUp().addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						panel.getButtonSingUp().doClick();
					}
				}
			});
			panel.getButtonHelpPass().addActionListener(e -> helpPass_Click());
			panel.getButtonHelpPass().setMnemonic(KeyEvent.VK_S);
			panel.getButtonHelpPass().addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						panel.getButtonHelpPass().doClick();
					}
				}
			});
		} catch (Exception e) {
			JOptionPane.showConfirmDialog(frame, e.getMessage());
		}

	}

	public void helpPass_Click() {
		try {
			Login.setPanel(frame, new FormHelpPass(controllerUser.getDatabase(), frame, passwordDesenvolver,dS, emailPrimitive,senhaPrimitive).getPanel());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void clearTextFields() {
		Component components[] = panel.getComponents();

		for (Component c : components) {
			if (c instanceof JTextField)
				((JTextField) c).setText("");
		}
	}

	public void login_Click() {
		try {
			Users user = getDadosLogin();
			if (controllerUser.existUsers(user) == false) {
				clearTextFields();
				panel.getTxtUsuario().requestFocus();
				throw new Exception("Usu�rio e/ou senha incorreto(s).\nTente novamente.");

			} else {
				frame.setResizable(true);
				frame.dispose();
				PanelInicializando panelInicio = new PanelInicializando();
				JFrame load = new JFrame();
				load.add(panelInicio);
				load.setUndecorated(true);
				load.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				load.pack();
				load.setSize(181, 68);
				App.centerForm(load);
				load.setVisible(true);
				JFrame frameApp = new JFrame(Login.nameApp);
				try
				{
					frameApp.setIconImage(new ImageIcon(new File("").getCanonicalPath()+"\\images\\ico.png").getImage());
				}
				catch(Exception e)
				{
					
				}
				frameApp.setExtendedState(JFrame.MAXIMIZED_BOTH);
				App app = new App(controllerUser.getUser(user.getUsername(), "username"), frameApp,
						controllerUser.getDatabase().getName(), passwordDesenvolver, dS, emailPrimitive,senhaPrimitive);
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							app.start(frameApp);
							panelInicio.setCarregando(false);
							load.dispose();
						} catch (Exception e) {
							JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado",
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}

	}

	public void singUp_Click() {
		try {
			Login.setPanel(frame, new FormSingUp(controllerUser.getDatabase(), frame, passwordDesenvolver, dS, emailPrimitive,senhaPrimitive).getPanel());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, e.getMessage(), "Algo deu errado",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	@SuppressWarnings("deprecation")
	public Users getDadosLogin() throws Exception {
		Users username = new Users();
		panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		if (panel.getTxtUsuario().getText().trim().isEmpty() == false) {
			username.setUsername(panel.getTxtUsuario().getText());
			if (panel.getTxtSenha().getText().isEmpty() == false) {
				username.setPassword(panel.getTxtSenha().getText());
				return username;
			} else {
				panel.getTxtSenha().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				panel.getTxtSenha().requestFocus();
				throw new Exception("Insira a senha.");
			}
		} else {
			panel.getTxtUsuario().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			panel.getTxtUsuario().requestFocus();
			throw new Exception("Insira o usu�rio.");
		}
	}

	public PanelLogin getPanel() {
		return panel;
	}

	public void setPanel(PanelLogin panel) {
		this.panel = panel;
	}

	public UsersController getControllerUser() {
		return controllerUser;
	}

	public void setControllerUser(UsersController controllerUser) {
		this.controllerUser = controllerUser;
	}

}
