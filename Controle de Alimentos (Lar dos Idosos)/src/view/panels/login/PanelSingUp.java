package view.panels.login;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import view.format.Limite_digitos;
import view.panels.utils.UpperCaseField_Limitado;

@SuppressWarnings("serial")
public class PanelSingUp extends JPanel {

	/**
	 * Creates new form PanelSingUp
	 */
	public PanelSingUp() {
		initComponents();
		
		manipularComponents();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon(new File("").getCanonicalPath()+ "\\images\\FundoLogin.jpeg").getImage(), 0, 0, this.getWidth(),this.getHeight(),this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initComponents() {

		txtUsuario = new UpperCaseField_Limitado(20,"[^a-zA-Z0-9_]",false,true){
			@Override
			public String getText()
			{
				return super.getText().toUpperCase();
			}
		};
		labelUser = new javax.swing.JLabel();
		labelUser.setForeground(Color.WHITE);
		labelSenha = new javax.swing.JLabel();
		labelSenha.setForeground(Color.WHITE);
		txtConfSenha = new javax.swing.JPasswordField();
		buttonSingUp = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};;
		buttonSingUp.setForeground(Color.WHITE);
		labelPassCheck = new javax.swing.JLabel();
		labelPassCheck.setForeground(Color.WHITE);
		labelEmail = new javax.swing.JLabel();
		labelEmail.setForeground(Color.WHITE);
		txtSenha = new javax.swing.JPasswordField();
		txtEmail = new javax.swing.JTextField();
        buttonVoltar = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);
				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};;
		buttonVoltar.setText("VOLTAR");
		buttonVoltar.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.RED));
        buttonVoltar.setContentAreaFilled(false);
        buttonVoltar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonVoltar.setFocusable(false);
        buttonVoltar.setForeground(Color.WHITE);
        buttonVoltar.setBackground(Color.RED);
        buttonVoltar.setOpaque(true);

        txtSenha.setDocument(new Limite_digitos(15,""));
		
		txtConfSenha.setDocument(new Limite_digitos(15,""));
		txtUsuario.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		labelUser.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelUser.setText("Usu�rio:");

		labelSenha.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelSenha.setText("Senha:");

		buttonSingUp.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
		buttonSingUp.setText("Cadastrar");
        buttonSingUp.setForeground(Color.WHITE);

		buttonVoltar.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N

		
		labelPassCheck.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelPassCheck.setText("Confirme a senha:");

		labelEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEmail.setText("Email:");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(labelPassCheck)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(txtConfSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(labelSenha)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(labelUser)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                        .addGap(0, 0, Short.MAX_VALUE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addGroup(layout.createSequentialGroup()
	                                .addGap(105, 105, 105)
	                                .addComponent(buttonSingUp))
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(labelEmail)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)))
	                .addContainerGap())
	            .addGroup(layout.createSequentialGroup()
	                .addComponent(buttonVoltar)
	                .addGap(0, 0, Short.MAX_VALUE))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelUser)
	                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelSenha)
	                    .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelPassCheck)
	                    .addComponent(txtConfSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelEmail)
	                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addComponent(buttonSingUp)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
	                .addComponent(buttonVoltar))
	        );
	}// </editor-fold>
	
	public void manipularComponents()
    {
        Component components[] = getComponents();

        for (Component c: components)
        {
            if (c instanceof JTextField)
            {
                ((JTextField) c).setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            	((JTextField) c).setCursor(new Cursor(Cursor.TEXT_CURSOR));
            	((JTextField) c).addKeyListener(new KeyAdapter() {
        			public void keyPressed(KeyEvent e) {
        				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
        					buttonVoltar.doClick();
        				}
        			}
        		});
            }
            if (c instanceof JButton)
            {
                ((JButton) c).setBorder(null);
                ((JButton) c).setContentAreaFilled(false);
                ((JButton) c).setCursor(new Cursor(Cursor.HAND_CURSOR));
                ((JButton) c).setDisabledIcon(null);
                ((JButton) c).setFocusCycleRoot(true);
                ((JButton) c).setFocusPainted(false);
            	
            }
        }   
    }
	
	// Variables declaration - do not modify
	private javax.swing.JButton buttonSingUp;
    private javax.swing.JButton buttonVoltar;
	private javax.swing.JLabel labelEmail;
	private javax.swing.JLabel labelPassCheck;
	private javax.swing.JLabel labelSenha;
	private javax.swing.JLabel labelUser;
	private javax.swing.JPasswordField txtConfSenha;
	private javax.swing.JTextField txtEmail;
	private javax.swing.JPasswordField txtSenha;
	private javax.swing.JTextField txtUsuario;
	// End of variables declaration
	public javax.swing.JButton getButtonSingUp() {
		return buttonSingUp;
	}

	public void setButtonSingUp(javax.swing.JButton buttonSingUp) {
		this.buttonSingUp = buttonSingUp;
	}

	public javax.swing.JButton getButtonVoltar() {
		return buttonVoltar;
	}

	public void setButtonVoltar(javax.swing.JButton buttonVoltar) {
		this.buttonVoltar = buttonVoltar;
	}

	public javax.swing.JLabel getLabelEmail() {
		return labelEmail;
	}

	public void setLabelEmail(javax.swing.JLabel labelEmail) {
		this.labelEmail = labelEmail;
	}

	public javax.swing.JLabel getLabelPassCheck() {
		return labelPassCheck;
	}

	public void setLabelPassCheck(javax.swing.JLabel labelPassCheck) {
		this.labelPassCheck = labelPassCheck;
	}

	public javax.swing.JLabel getLabelSenha() {
		return labelSenha;
	}

	public void setLabelSenha(javax.swing.JLabel labelSenha) {
		this.labelSenha = labelSenha;
	}

	public javax.swing.JLabel getLabelUser() {
		return labelUser;
	}

	public void setLabelUser(javax.swing.JLabel labelUser) {
		this.labelUser = labelUser;
	}

	public javax.swing.JPasswordField getTxtConfSenha() {
		return txtConfSenha;
	}

	public void setTxtConfSenha(javax.swing.JPasswordField txtConfSenha) {
		this.txtConfSenha = txtConfSenha;
	}

	public javax.swing.JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(javax.swing.JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public javax.swing.JPasswordField getTxtSenha() {
		return txtSenha;
	}

	public void setTxtSenha(javax.swing.JPasswordField txtSenha) {
		this.txtSenha = txtSenha;
	}

	public javax.swing.JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(javax.swing.JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}
}