package model.entity;


import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import java.util.Date;
import java.text.SimpleDateFormat;
	
@DatabaseTable (tableName = "validades")
public class Validade
{

    @DatabaseField(generatedId = true)
    int id;
    
    @DatabaseField
    String codBar;
    
    @DatabaseField
    String nome;
    
    @DatabaseField
    int quantidade;
    
    @DatabaseField(dataType=DataType.DATE)
    public Date endDate;  
    
    public String printEndDate() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(endDate.getTime());
    }

    
    public void setProduto(Produtos produto)
    {
        codBar = produto.getCodBar();
        nome = produto.getNome();
    }

    
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie codBar*/
    public String getCodBar(){
        return this.codBar;
    }//end method getCodBar

    /**SET Method Propertie codBar*/
    public void setCodBar(String codBar){
        this.codBar = codBar;
    }//end method setCodBar

    /**GET Method Propertie nome*/
    public String getNome(){
        return this.nome;
    }//end method getNome

    /**SET Method Propertie nome*/
    public void setNome(String nome){
        this.nome = nome;
    }//end method setNome


    /**GET Method Propertie endDate*/
    public Date getEndDate(){
        return this.endDate;
    }//end method getEndDate

    /**SET Method Propertie CheckDate*/
    public void setEndDate(Date endDate){
        this.endDate = endDate;
    }//end method setEndDate

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
    
    //End GetterSetterExtension Source Code
//!
}
