package view.forms.login;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

import view.panels.login.PanelHelpPass;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

public class RememberPassEmail implements Runnable {

	private String remetente = "gerenciador.validades@gmail.com";
	private String password = "gerenciador123";
	private String usuario = "CV";
	private String assunto = "RECUPER��O DE DADOS";
	private String destino;
	private String host;
	private String server;
	private String mensagem;
	private PanelHelpPass panel;
	public RememberPassEmail(String destino, String user, String passwordUser, FormHelpPass form) throws Exception {
		this.destino = destino;
		this.panel = form.getPanel();
		for (int i = 0; i < destino.length(); i++) {
			if (destino.charAt(i) == '@') {
				server = destino.substring(i);
				break;
			}
		}
		if (server.toUpperCase().equals("@GMAIL.COM"))
			host = "smtp.gmail.com";
		else
			throw new Exception("O email n�o � um GMAIL.");
		mensagem = "Ol�, " + user + ". <br>Este email foi utilizado para recupera��o de usu�rio e senha da plataforma"
				+ " Controle de Validade. Segue abaixo os dados de cadastro da �NICA conta com este endere�o de email."
				+ "<br><br>Usu�rio: " + user + ".<br>Senha: " + passwordUser
				+ ".<br/><br>Caso n�o tenha sido voc� a pedir"
				+ " recupera��o de dados. Por favor desconsidere este email.<br><br><br>N�O RESPONDA ESTE EMAIL.";
	}

	@Override
	public void run() {
		try {
			panel.getButtonSend().setEnabled(false);
			panel.getTxtStatus().setText("Enviando email. Aguarde...");
			envioSimples(usuario, assunto, mensagem, destino, host);
			panel.getTxtStatus().setText("");
			JOptionPane.showMessageDialog(null,"Um email com o nome de usu�rio e "
					+ "senha foi enviado para seu endere�o de email.\nVERIFIQUE SUA CAIXA DE SPAM.",
					"Email enviado com sucesso.", 
	                JOptionPane.INFORMATION_MESSAGE);
			panel.getButtonSend().setEnabled(true);
			panel.getButtonVoltar().doClick();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,e.getMessage(),
					"Algo deu errado", 
	                JOptionPane.INFORMATION_MESSAGE);
		}
	}


	public void envioSimples(String nomeRemetente, String assunto, String mensagem, String destinatario, String host)
			throws Exception {
		try {
			boolean sessionDebug = true;

			Properties props = System.getProperties();
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.required", "true");
			props.put("mail.smtp.ssl.trust", host);

			Session mailSession = Session.getDefaultInstance(props, null);
			mailSession.setDebug(sessionDebug);
			Message msg = new MimeMessage(mailSession);
			msg.setFrom(new InternetAddress(remetente, nomeRemetente));
			msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
			msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse("gerenciador.validades@gmail.com"));
			msg.setSubject(assunto);
			msg.setSentDate(new Date());
			msg.setContent(mensagem, "text/html;charset=UTF-8");

			Transport transport = mailSession.getTransport("smtp");
			transport.connect(host, remetente, password);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();

			System.out.println("Enviado com Sucesso");
		} catch (MessagingException ex) {
			throw new Exception("Erro ao enviar email. Voc� est� sem conex�o.");
		} catch (UnsupportedEncodingException ex) {
			throw new Exception("Destinat�rio Inv�lido.");
		}
	}
}