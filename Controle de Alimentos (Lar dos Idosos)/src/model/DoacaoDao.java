package model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;

import model.entity.Doacao;

public class DoacaoDao {
	public static Database database;
	public static Dao<Doacao, Integer> dao;
	public Doacao doacaoCarregada;
	private List<Doacao> doacoesCarregados;

	public DoacaoDao(Database database) {
		DoacaoDao.setDatabase(database);
		doacoesCarregados = new ArrayList<Doacao>();
	}

	public static void setDatabase(Database database) {
		ValidadeDao.database = database;
		try {
			dao = DaoManager.createDao(database.getConnection(), Doacao.class);
			TableUtils.createTableIfNotExists(database.getConnection(), Doacao.class);
		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public void create(Doacao doacao) throws Exception {
		int nrows = 0;
		nrows = dao.create(doacao);
		if (nrows == 0)
			throw new SQLException("Erro ao cadastrar a doa��o");
		doacaoCarregada = doacao;
		doacoesCarregados.add(doacao);
		
	}

	public List<Doacao> readAll() throws Exception {
		doacoesCarregados = dao.queryForAll();

		return doacoesCarregados;
	}
}
