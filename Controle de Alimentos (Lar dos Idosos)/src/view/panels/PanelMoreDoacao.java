package view.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import model.entity.Doacao;
import view.format.Limite_digitos;

@SuppressWarnings("serial")
public class PanelMoreDoacao extends javax.swing.JPanel {

	public PanelMoreDoacao(JFrame frame, List<Doacao> doacoes) throws Exception {
		this.doacoes = doacoes;
		this.bloq = frame;
		index = 0;
		initComponents();
		showDoacao();
		showFrame();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(200, 200, 255));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() {
		optionPane = new JDialog(bloq);
		optionPane.add(this);
		optionPane.setSize(300, 460);
		optionPane.setModal(true);
		optionPane.setLocationRelativeTo(bloq);
		optionPane.setResizable(false);
		optionPane.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		optionPane.setLocation(bloq.getX() + bloq.getWidth() / 2 - optionPane.getWidth() / 2,
				bloq.getY() + bloq.getHeight() / 2 - optionPane.getHeight() / 2 - 15);
		optionPane.setVisible(true);
		bloq.setFocusable(false);

	}

	public void close() {
		optionPane.dispose();
	}

	private void initComponents() throws Exception {

		buttonPrevious = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonNext = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		labelCod = new javax.swing.JLabel();
		txtCod = new javax.swing.JTextField();
		labelTipo = new javax.swing.JLabel();
		txtTipo = new javax.swing.JTextField();
		labelDoador = new javax.swing.JLabel();
		txtDoador = new javax.swing.JTextField();
		labelReceptor = new javax.swing.JLabel();
		labelData = new javax.swing.JLabel();
		txtReceptor = new javax.swing.JTextField();
		labelQntd = new javax.swing.JLabel();
		txtData = new javax.swing.JTextField();
		txtQntd = new javax.swing.JTextField();
		labelDesc = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		txtDesc = new javax.swing.JTextArea();
		labelCount = new javax.swing.JLabel();
		buttonFrist = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonEnd = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonVoltar = new javax.swing.JButton() {
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};

		labelCod.setText("C�digo:");

		labelTipo.setText("Tipo:");

		labelDoador.setText("Doador:");

		labelReceptor.setText("Receptor:");

		labelData.setText("Data:");

		labelQntd.setText("Quantidade:");

		labelDesc.setText("Descri��o:");

		jScrollPane1.setBorder(null);

		txtDesc.setColumns(20);
		txtDesc.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtDesc.setDocument(new Limite_digitos(120, ""));
		txtDesc.setRows(5);
		txtDesc.setTabSize(3);
		txtDesc.setWrapStyleWord(true);
		txtDesc.setDragEnabled(true);
		txtDesc.setLineWrap(true);
		txtDesc.setMargin(new java.awt.Insets(3, 3, 2, 2));

		jScrollPane1.setViewportView(txtDesc);

		buttonPrevious
				.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//rewind_24px.png")); // NOI18N
		buttonPrevious.addActionListener(e_ -> previous_Click());

		buttonNext.setIcon(
				new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//fast_forward_24px.png")); // NOI18N
		buttonNext.addActionListener(e -> next_Click());

		buttonFrist.setIcon(
				new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//skip_to_start_24px.png")); // NOI18N
		buttonFrist.addActionListener(e -> frist_Click());

		buttonEnd.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//end_24px.png")); // NOI18N
		buttonEnd.addActionListener(e -> end_Click());

		buttonVoltar.setText("  SAIR");
		buttonVoltar.addActionListener(e -> close());

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addComponent(buttonVoltar)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(buttonFrist).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(buttonPrevious).addGap(23, 23, 23).addComponent(buttonNext)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(buttonEnd)
						.addGap(0, 11, Short.MAX_VALUE))
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addGroup(layout.createSequentialGroup().addComponent(labelTipo)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 135,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGroup(layout.createSequentialGroup().addComponent(labelCod)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtCod))
										.addGroup(layout.createSequentialGroup().addComponent(labelDoador)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtDoador))
										.addGroup(layout.createSequentialGroup().addComponent(labelReceptor)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtReceptor))
										.addGroup(layout.createSequentialGroup().addComponent(labelQntd)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtQntd, javax.swing.GroupLayout.PREFERRED_SIZE, 73,
														javax.swing.GroupLayout.PREFERRED_SIZE))
										.addComponent(labelDesc).addComponent(jScrollPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createSequentialGroup().addComponent(labelData)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 85,
												javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(labelCount)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelCod).addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelTipo).addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelDoador)
								.addComponent(txtDoador, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelReceptor)
								.addComponent(txtReceptor, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelData).addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(labelQntd).addComponent(txtQntd, javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(18, 18, 18).addComponent(labelDesc)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(buttonNext).addComponent(buttonPrevious).addComponent(buttonEnd)
								.addComponent(buttonFrist))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(labelCount)
						.addGap(6, 6, 6).addComponent(buttonVoltar)));
		configureFields();
	}// </editor-fold>

	public void configureFields() {
		Component[] components = getComponents();
		for (Component c : components) {
			if (c instanceof JTextField) {
				((JTextField) c).setBackground(new Color(240, 240, 240));
				((JTextField) c).setEditable(false);
				((JTextField) c).setCursor(new Cursor(Cursor.TEXT_CURSOR));
				((JTextField) c).setFont(new Font("Arial", 0, 12));

			} else if (c instanceof JButton) {
				((JButton) c).setBorder(null);

				((JButton) c).setFont(new Font("Arial", 0, 12));
				if (((JButton) c).getText().trim().toLowerCase().equals("sair")) {

					((JButton) c).setFont(new Font("Arial", 0, 13));
				}
				((JButton) c).setForeground(Color.RED);
				((JButton) c).setEnabled(true);
				((JButton) c).setContentAreaFilled(false);
				((JButton) c).setFocusable(false);
			} else if (c instanceof JLabel) {
				((JLabel) c).setFont(new Font("Arial", 0, 12));
			}

		}
		txtDesc.setBackground(new Color(240, 240, 240));
		txtDesc.setEditable(false);
		txtDesc.setCursor(new Cursor(Cursor.TEXT_CURSOR));
	}

	public void next_Click() {
		index++;
		showDoacao();
	}

	public void end_Click() {
		index = doacoes.size() - 1;
		showDoacao();
	}

	public void previous_Click() {
		index--;
		showDoacao();
	}

	public void frist_Click() {
		index = 0;
		showDoacao();
	}

	public void showDoacao() {
		txtCod.setText(doacoes.get(index).getCodigo());
		txtData.setText(doacoes.get(index).printData());
		txtDesc.setText(doacoes.get(index).getDesc());
		txtDoador.setText(doacoes.get(index).getDoador());
		txtQntd.setText(doacoes.get(index).getQnt());
		txtReceptor.setText(doacoes.get(index).getReceptor());
		txtTipo.setText(doacoes.get(index).getTipo());
		if (index == 0) {
			if (buttonFrist.isEnabled() || buttonPrevious.isEnabled()) {
				buttonFrist.setEnabled(false);
				buttonPrevious.setEnabled(false);
			}
		} else {
			if ((buttonFrist.isEnabled() || buttonPrevious.isEnabled()) == false) {
				buttonFrist.setEnabled(true);
				buttonPrevious.setEnabled(true);
			}
		}
		if (index == doacoes.size() - 1) {
			if (buttonNext.isEnabled() || buttonEnd.isEnabled()) {
				buttonEnd.setEnabled(false);
				buttonNext.setEnabled(false);
			}
		} else {
			if ((buttonNext.isEnabled() || buttonEnd.isEnabled()) == false) {
				buttonEnd.setEnabled(true);
				buttonNext.setEnabled(true);
			}
		}
		labelCount.setText(index + 1 + "/" + doacoes.size());
	}

	// Variables declaration - do not modify
	private javax.swing.JButton buttonEnd;
	private javax.swing.JButton buttonFrist;
	private javax.swing.JButton buttonNext;
	private javax.swing.JButton buttonPrevious;
	private javax.swing.JButton buttonVoltar;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JLabel labelCod;
	private javax.swing.JLabel labelCount;
	private javax.swing.JLabel labelData;
	private javax.swing.JLabel labelDesc;
	private javax.swing.JLabel labelDoador;
	private javax.swing.JLabel labelQntd;
	private javax.swing.JLabel labelReceptor;
	private javax.swing.JLabel labelTipo;
	private javax.swing.JTextField txtCod;
	private javax.swing.JTextField txtData;
	private javax.swing.JTextArea txtDesc;
	private javax.swing.JTextField txtDoador;
	private javax.swing.JTextField txtQntd;
	private javax.swing.JTextField txtReceptor;
	private javax.swing.JTextField txtTipo;
	private List<Doacao> doacoes;
	private int index;
	private JFrame bloq;
	private JDialog optionPane;
	// End of variables declaration
}
