
package view.panels.utils;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.entity.Validade;
import model.Database;
import controller.ValidadeController;

@SuppressWarnings("serial")
public class ValidadeTableModel extends AbstractTableModel {

	private String colunas[] = {"Id", "Cod Barras", "Nome", "Quantidade", "Validade" };
	private List<Validade> validades;
	private final int COLUNA_ID = 0;
	private final int COLUNA_CODBAR = 1;
	private final int COLUNA_NOME = 2;
	private final int COLUNA_QNTD = 3;
	private final int COLUNA_ENDDATE = 4;
	int rows;

	public ValidadeTableModel(String codUser) {
		try {
			validades = new Ordenacao().ordeneData(new ValidadeController(new Database("Val" + codUser + ".db")).readAll());
			rows = this.validades.size();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	// retorna se a célula é edit�vel ou n�o
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	// retorna o total de itens(que virar�o linhas) da nossa lista
	@Override
	public int getRowCount() {
		return validades.size();
	}

	// retorna o total de colunas da tabela
	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	// retorna o nome da coluna de acordo com seu indice
	@Override
	public String getColumnName(int indice) {
		return colunas[indice];
	}

	// determina o tipo de dado da coluna conforme seu indice
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case COLUNA_ID:
			return Integer.class;
		case COLUNA_CODBAR:
			return String.class;
		case COLUNA_NOME:
			return String.class;
		case COLUNA_QNTD:
			return Integer.class;
		case COLUNA_ENDDATE:
			return String.class;
		default:
			return String.class;
		}
	}

	// preenche cada célula da tabela
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Validade validade = this.validades.get(rowIndex);

		switch (columnIndex) {
		case COLUNA_ID:
			return rowIndex + 1;
		case COLUNA_CODBAR:
			return validade.getCodBar();
		case COLUNA_NOME:
			return "  " + validade.getNome();
		case COLUNA_QNTD:
			return validade.getQuantidade();
		case COLUNA_ENDDATE:
			return validade.printEndDate();
		}
		return null;
	}
	// altera o valor do objeto de acordo com a célula editada
	// e notifica a alteraç�o da tabela, para que ela seja atualizada na tela

	public void setValidades(List<Validade> validades) throws Exception {
		try {
			this.validades = new Ordenacao().ordeneData(validades);
			fireTableRowsUpdated(0, validades.size());
			if (validades.size() < rows) {
				fireTableRowsDeleted(validades.size(), rows);
			} else {
				fireTableRowsInserted(rows, validades.size());
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void updateValidade() throws Exception {
		try {
			this.validades = new Ordenacao().ordeneData(this.validades);
			fireTableRowsUpdated(0, validades.size());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void removeValidade(Validade validadeRemove) {
		validades.remove(validadeRemove);
		fireTableRowsDeleted(validades.size() + 1, validades.size() + 1);

	}

	public List<Validade> getValidades() {
		return this.validades;
	}

}
