package view.panels.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.entity.Doacao;
import model.entity.Produtos;
import model.entity.Validade;

public class Ordenacao implements TratamentoDate {

	public void analiseEndDate(String text) throws Exception {

	};

	public List<Validade> ordeneData(List<Validade> validades) throws Exception {
		Validade aux = new Validade();
		for (int i = 0; i < validades.size() - 1; i++) {
			for (int j = i + 1; j < validades.size(); j++) {
				int diff = diffInDays(validades.get(i).printEndDate(), validades.get(j).printEndDate());
				if (diff < 0) {
					aux = validades.get(j);
					validades.set(j, validades.get(i));
					validades.set(i, aux);
				}
			}
		}
		return validades;

	}

	public List<Doacao> ordeneDoacoes(List<Doacao> doacoes) throws Exception {
		Doacao aux = new Doacao();
		for (int i = 0; i < doacoes.size() - 1; i++) {
			for (int j = i + 1; j < doacoes.size(); j++) {
				int diff = diffInDays(doacoes.get(i).printData(), doacoes.get(j).printData());
				if (diff > 0) {
					aux = doacoes.get(j);
					doacoes.set(j, doacoes.get(i));
					doacoes.set(i, aux);
				}
			}
		}
		return doacoes;

	}
	
	public List<Produtos> ordeneABC(List<Produtos> produtos) throws Exception {
		Produtos aux = new Produtos();
		for (int i = 0; i < produtos.size() - 1; i++) {
			for (int j = i; j < produtos.size(); j++) {
				if (produtos.get(i).getNome().charAt(0) > produtos.get(j).getNome().charAt(0)) {
					aux = produtos.get(i);
					produtos.set(i, produtos.get(j));
					produtos.set(j, aux);
				}
			}
		}
		return produtos;
	}

	public int diffInDays(String d1, String d2) throws Exception {

		int MILLIS_IN_DAY = 86400000;

		Calendar c1 = Calendar.getInstance();
		c1.setTime(stringToDate(d1));
		c1.set(Calendar.MILLISECOND, 0);
		c1.add(Calendar.MONTH, -1);
		c1.set(Calendar.SECOND, 0);
		c1.set(Calendar.MINUTE, 0);
		c1.set(Calendar.HOUR_OF_DAY, 0);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(stringToDate(d2));
		c2.set(Calendar.MILLISECOND, 0);
		c2.add(Calendar.MONTH, -1);
		c2.set(Calendar.SECOND, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.HOUR_OF_DAY, 0);
		int diff = (int) ((c2.getTimeInMillis() - c1.getTimeInMillis()) / MILLIS_IN_DAY);

		return diff;

	}

	public Date stringToDate(String string) throws Exception {

		Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
		Matcher matcher = dataPadrao.matcher(string);
		if (matcher.matches()) {
			int dia = Integer.parseInt(matcher.group(1));
			int mes = Integer.parseInt(matcher.group(2));
			int ano = Integer.parseInt(matcher.group(3));
			return (new GregorianCalendar(ano, mes, dia)).getTime();
		} else
			throw new Exception(" Data final inv�lida.");

	}
}
