package view.forms.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.swing.JOptionPane;

import org.apache.commons.mail.DefaultAuthenticator;

import controller.SettingsController;
import model.Database;
import model.entity.Settings;

import org.apache.commons.mail.*;

public class BackupGmail {

	private String cliente;
	private HtmlEmail email;
	private int codUser;
	private Settings settings;
	public BackupGmail(String cliente, int codUser, String nameDBS) throws Exception {
		this.cliente = cliente;
		this.codUser = codUser;
		this.settings = new SettingsController(new Database(nameDBS)).getSettings();
	}


	@SuppressWarnings("deprecation")
	public void conectaEmail(String username, String password, String titulo)
			throws EmailException, NoSuchProviderException, MessagingException {
		String hostname = "smtp.gmail.com";
		String emailOrigem = settings.getEmailPrograma();
		HtmlEmail email = new HtmlEmail();
		email.setHostName(hostname);
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator(username, password));
		email.setTLS(true);
		email.setFrom(emailOrigem, titulo);
		email.setDebug(true);
		email.setCharset(HtmlEmail.ISO_8859_1);
		Properties props = new Properties();
		props.setProperty(hostname, "smtp");
		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.port", "" + 587);
		props.setProperty("mail.smtp.starttls.enable", "true");
		Session mailSession = Session.getInstance(props, new DefaultAuthenticator(username, password));
		email.setMailSession(mailSession);
		this.email = email;
	}

	public void enviaEmailHtml(String destino, String texto) {
		try {
			email.addTo(destino);
			email.setSubject(
					cliente + " " + new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));
			File validadeDB = new File("Val"+codUser+".db");
			File produtoDB = new File("Prod"+codUser+".db");
			File donationsDB = new File("Donation"+codUser+".db");
			File settingsDB = new File("settings.db");
			email.setHtmlMsg("<html><left><p>" + "<db src=cid:" + email.embed(validadeDB) + email.embed(produtoDB) + email.embed(donationsDB) + email.embed(settingsDB) + ">"
					+ texto + "</p></left></html>");
			email.buildMimeMessage();
			Message m = email.getMimeMessage();
			Transport transport = email.getMailSession().getTransport("smtp");
			transport.connect(email.getHostName(), settings.getEmailPrograma(), settings.getSenhaEmailPrograma());
			transport.sendMessage(m, m.getAllRecipients());
			transport.close();
			System.out.println("E-mail enviado para: " + destino);
		} catch (Exception ex) {
			System.out.print(ex.getMessage());
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Algo deu errado", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
