package controller;

import model.Database;
import model.SettingsDao;
import model.entity.Settings;

public class SettingsController {

	private SettingsDao dao;
	private Database db;
	
	public SettingsController(Database database) throws Exception
	{
		this.dao = new SettingsDao(database);
		this.db = database;
	}
	
	public boolean isEmailSetado()
	{
		return dao.isEmailSetado();
	}
	
	public boolean isBackupSetado()
	{
		return dao.isBackupSetado();
	}
	
	public Database getDatabase()
	{
		return this.db;
	}
	
	public boolean isSetado()
	{
		return dao.isSetado();
	}
	
	public Settings getSettings()
	{
		return dao.getSettings();
	}
	
	public void update() throws Exception
	{
		dao.update();
	}
}
