package view.forms.configs;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import controller.SettingsController;
import model.entity.Users;
import view.forms.FormConfig;

@SuppressWarnings("serial")
public class ConfigAtalhos extends JPanel {

	private SettingsController controllerSettings;
	private String password;
	private Users user;
	private JDialog optionPane;
	private JFrame bloq;
	private FormConfig form;
	public ConfigAtalhos(JFrame frame, SettingsController controller, Users user, String passwordDesenvolver, FormConfig form) {
		controllerSettings = controller;
		this.form = form;
		this.password = passwordDesenvolver;
		this.user = user;
		this.bloq = frame;
		initComponents();
		showFrame();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(210, 210, 210));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}

	public void showFrame() {
		optionPane = new JDialog(bloq);
		optionPane.add(this);
		optionPane.setSize(400, 280);
		optionPane.setModal(true);
		optionPane.setLocationRelativeTo(bloq);
		optionPane.setResizable(false);
		optionPane.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		optionPane.setLocation(bloq.getX() + bloq.getWidth() / 2 - optionPane.getWidth() / 2,
				bloq.getY() + bloq.getHeight() / 2 - optionPane.getHeight() / 2);
		optionPane.setVisible(true);
	}

	private void setEmailProgram(String email, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setEmailPrograma(email);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Email do programa setado.");
		} else
		{
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void setEmailPasswordProgram(String password, String senha) throws Exception {
		if (senha.equals(this.password)) {
			controllerSettings.getSettings().setSenhaEmailPrograma(password);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Senha do email (programa) setada.");
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void setEmailTesouraria(String emailTesouraria, String senha) throws Exception {
		if (senha.equals(password) || senha.equals(controllerSettings.getSettings().getPasswordTesouraria())) {
			controllerSettings.getSettings().setEmailTesouraria(emailTesouraria);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Email da tesouraria setado.");
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void setEmailBackup(String email, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setEmailBacukp(email);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Email de backup setado.");
		}else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void isEmailTesouraria(String send, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setSendEmailTesouraria(Boolean.valueOf(send));
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			if (Boolean.valueOf(send))
				throw new Exception("Enviar email a tesouraria setado.");
			else
				throw new Exception("N�o enviar email a tesouraria setado.");
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void isStartLogin(String start, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setStartLogin(Boolean.valueOf(start));
			if (Boolean.valueOf(start) == false) {
				controllerSettings.getSettings().setUsername(user.getUsername());
				controllerSettings.getSettings().setPassword(user.getPassword());

				controllerSettings.update();
				form.setInformacoes();
				txtArgumento.setText("");
				txtPassowrdDesenvolverdor.setText("");
				throw new Exception("Iniciar app sem login setado.");
			} else {

				controllerSettings.update();
				form.setInformacoes();
				txtArgumento.setText("");
				txtPassowrdDesenvolverdor.setText("");
				throw new Exception("Iniciar em login setado.");
			}
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void setNomeEmpresa(String name, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setNomeEmpresa(name);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Nome da empresa setado.");
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}
	private void setEmailHelp(String emailHelp, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setEmailTesouraria(emailHelp);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Email de ajuda setado.");
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	private void setPasswordTesouraria(String passwordTesouraria, String senha) throws Exception {
		if (senha.equals(password)) {
			controllerSettings.getSettings().setPasswordTesouraria(passwordTesouraria);
			controllerSettings.update();
			form.setInformacoes();
			txtArgumento.setText("");
			txtPassowrdDesenvolverdor.setText("");
			throw new Exception("Senha da tesouraria setada.");
		} else
		{
			txtPassowrdDesenvolverdor.setText("");
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
			throw new Exception("Senha incorreta. Aguarde o desenvolvedor.");
		}
	}

	@SuppressWarnings("deprecation")
	public void ok_Click() {
		try {
			txtArgumento.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			if (txtArgumento.getText().trim().isEmpty() == false) {

				if (txtPassowrdDesenvolverdor.getText().trim().isEmpty() == false) {
					switch (comboFuncao.getSelectedIndex()) {
					case 0:
						setEmailProgram(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 1:
						setEmailPasswordProgram(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 2:
						setEmailTesouraria(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 3:
						setEmailBackup(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 4:
						isEmailTesouraria(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 5:
						isStartLogin(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 6:
						setNomeEmpresa(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 7:
						setEmailHelp(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					case 8:
						setPasswordTesouraria(txtArgumento.getText(), txtPassowrdDesenvolverdor.getText());
						break;
					}
				} else {
					txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					throw new Exception("Insira a senha.");
				}
			} else {
				txtArgumento.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				throw new Exception("Insira o argumento.");
			}
		} catch (Exception e) {
			txtStatus.setText(e.getMessage());
		}
	}

	private void initComponents() {

		labelFuncao = new javax.swing.JLabel();
		txtArgumento = new javax.swing.JTextField();
		labelArgumento = new javax.swing.JLabel();
		comboFuncao = new javax.swing.JComboBox<>();
		labelPasswordDesenvoledor = new javax.swing.JLabel();
		txtPassowrdDesenvolverdor = new javax.swing.JPasswordField();
		buttonConfirm = new javax.swing.JButton();
		txtStatus = new javax.swing.JTextField();
		labelTitulo = new javax.swing.JLabel();

		labelFuncao.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelFuncao.setText("Fun��o:");

		txtArgumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		txtArgumento.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		txtPassowrdDesenvolverdor.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
		labelArgumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelArgumento.setText("Argumento:");

		comboFuncao.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		comboFuncao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "setEmailProgram",
				"setEmailPasswordProgram", "setEmailTesouraria", "setEmailBackup", "isEmailTesouraria", "isStartLogin",
				"setNomeEmpresa", "setEmailHelp", "setPasswordTesouraria" }));
		comboFuncao.setFocusable(false);
		comboFuncao.setCursor(new Cursor(Cursor.HAND_CURSOR));
		labelPasswordDesenvoledor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelPasswordDesenvoledor.setText("Senha do desenvolvedor:");

		buttonConfirm.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonConfirm.setText("OK");
		buttonConfirm.addActionListener(e -> ok_Click());
		buttonConfirm.setFocusable(false);
		buttonConfirm.setCursor(new Cursor(Cursor.HAND_CURSOR));

		txtStatus.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtStatus.setBorder(null);
		txtStatus.setOpaque(false);
		txtStatus.setEditable(false);

		labelTitulo.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
		labelTitulo.setText("ATALHOS DO DESENVOLVEDOR");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(3, 3, 3)
						.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 246,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(layout.createSequentialGroup().addContainerGap(60, Short.MAX_VALUE).addComponent(labelTitulo)
						.addContainerGap(60, Short.MAX_VALUE))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(layout
								.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
										.createSequentialGroup().addComponent(labelPasswordDesenvoledor)
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(txtPassowrdDesenvolverdor, javax.swing.GroupLayout.PREFERRED_SIZE,
												118, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout
												.createSequentialGroup().addComponent(labelFuncao)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
												.addComponent(comboFuncao, javax.swing.GroupLayout.PREFERRED_SIZE, 174,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout
												.createSequentialGroup().addComponent(labelArgumento)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
												.addComponent(txtArgumento, javax.swing.GroupLayout.PREFERRED_SIZE, 191,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)))))
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(buttonConfirm)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(layout
				.createSequentialGroup().addContainerGap().addComponent(labelTitulo).addGap(21, 21, 21)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelFuncao).addComponent(comboFuncao, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelArgumento).addComponent(txtArgumento, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(18, 18, 18)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
						.addComponent(labelPasswordDesenvoledor).addComponent(txtPassowrdDesenvolverdor,
								javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE))
				.addGap(25, 25, 25).addComponent(buttonConfirm).addGap(18, 18, 18)
				.addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
				.addGap(0, 0, Short.MAX_VALUE)));
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JButton buttonConfirm;
	private javax.swing.JComboBox<String> comboFuncao;
	private javax.swing.JLabel labelArgumento;
	private javax.swing.JLabel labelFuncao;
	private javax.swing.JLabel labelPasswordDesenvoledor;
	private javax.swing.JLabel labelTitulo;
	private javax.swing.JTextField txtArgumento;
	private javax.swing.JPasswordField txtPassowrdDesenvolverdor;
	private javax.swing.JTextField txtStatus;
	// End of variables declaration
}
