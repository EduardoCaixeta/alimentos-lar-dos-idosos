package view.panels;

import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class PanelInicializando extends javax.swing.JPanel {

	/**
	 * Creates new form PanelInicializando
	 */
	public PanelInicializando() {
		initComponents();
	}

	public void paintComponent(Graphics g) {
		g.setColor(new Color(255, 200, 200));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());

	}
	
	Thread thread;
	boolean carregando = true;

	private void initComponents() {

		labelCarregando = new javax.swing.JLabel();

		labelCarregando.setText("CARREGANDO...");

		thread = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					int i = 0;
					while (carregando) {
						if (i == 0) {
							labelCarregando.setText("CARREGANDO ...");
							i = 1;
							Thread.sleep(500);
						} else if (i == 1) {
							labelCarregando.setText("CARREGANDO .");
							i = 2;
							Thread.sleep(500);
						} else if (i == 2) {
							labelCarregando.setText("CARREGANDO ..");
							i = 0;
							Thread.sleep(500);
						}
					}

				}
				catch (Exception e) {
					thread.interrupt();
				}
			}
		});
thread.start();
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap(74, Short.MAX_VALUE)
						.addComponent(labelCarregando).addContainerGap(80, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addGap(26, 26, 26).addComponent(labelCarregando)
						.addContainerGap(28, Short.MAX_VALUE)));
	}// </editor-fold>

	// Variables declaration - do not modify
	private javax.swing.JLabel labelCarregando;
	// End of variables declaration

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public boolean isCarregando() {
		return carregando;
	}

	public void setCarregando(boolean carregando) {
		this.carregando = carregando;
	}

	public javax.swing.JLabel getLabelCarregando() {
		return labelCarregando;
	}

	public void setLabelCarregando(javax.swing.JLabel labelCarregando) {
		this.labelCarregando = labelCarregando;
	}
}