package controller;

import java.util.List;
import model.entity.Produtos;
import model.ProdutosDao;
import model.Database;

public class ProdutosController {
	private ProdutosDao dao;
	private Database db;

	public ProdutosController(Database database) throws Exception {
		this.dao = new ProdutosDao(database);
		this.db = database;
	}

	public Database getDatabase() {
		return db;
	}

	public void create(Produtos produto) throws Exception {
		dao.create(produto);
	}

	public List<Produtos> readAll() throws Exception {
		return dao.readAll();
	}

	public Produtos loadForCodBar(String codBar) throws Exception {
		return dao.loadForCodBar(codBar);
	}

	public void update(Produtos produto) throws Exception {
		dao.update(produto);
	}

	public boolean existCodBar(String codBar) throws Exception {
		return dao.existCodBar(codBar);
	}

	public void delete(Produtos produto) throws Exception {
		dao.delete(produto);
	}
}
