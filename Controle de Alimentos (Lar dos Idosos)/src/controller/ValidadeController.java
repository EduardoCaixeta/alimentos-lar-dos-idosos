package controller;

import java.util.List;
import model.entity.*;
import model.ValidadeDao;
import model.Database;

public class ValidadeController
{
     private ValidadeDao dao;
     private Database db;
     public ValidadeController (Database database)
    {
        this.dao = new ValidadeDao(database);
        this.db = database;
    }
    
     public Database getDatabase()
 	{
 		return db;
 	}
     
    public void create (Validade validade) throws Exception
    {
        dao.create(validade);
    }
    
    public List<Validade> readAll() throws Exception
    {
        return dao.readAll();
    }
    
     public void update (Validade validade)throws Exception
    {
        dao.update(validade);
    }
    
     public void delete (Validade validade) throws Exception
    {
        dao.delete(validade);
    }
    
    public int existEquals(Validade validade) throws Exception
    {
        return dao.existEquals(validade);
    }

    public Validade loadForId(int id) throws Exception
    {
        return dao.loadForId(id);
    }
}
