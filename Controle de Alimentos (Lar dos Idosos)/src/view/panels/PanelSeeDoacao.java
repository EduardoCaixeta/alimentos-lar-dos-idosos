package view.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.MaskFormatter;

import model.Database;
import view.format.Limite_digitos;
import view.panels.utils.DoacaoTableModel;
import view.panels.utils.UpperCaseField_Limitado;

@SuppressWarnings("serial")
public class PanelSeeDoacao extends javax.swing.JPanel {

	/**
	 * Creates new form PanelSeeDoacao
	 */
	Database dD;
	public PanelSeeDoacao(Database dD) {
		this.dD = dD;
		initComponents();
	}


	@Override
	public void paintComponent(Graphics g) {
		try {
			g.drawImage(new ImageIcon("FundoAnalise.jpeg").getImage(), 0, 0, this);
			System.out.print("\n\n\n\n"+new File("").getCanonicalPath() + "FundoAnalise.jpeg");
		} catch (IOException e) {
			System.out.print(e.getMessage());
		}

	}
	public void initComponents() {

		labelCod = new javax.swing.JLabel();
		labelTipo = new javax.swing.JLabel();
		labelReceptor = new javax.swing.JLabel();
		labelData = new javax.swing.JLabel();
		txtCod = new javax.swing.JTextField(){
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		comboTipo = new javax.swing.JComboBox<>();
		labelEspecifique = new javax.swing.JLabel();
		txtTipo = new UpperCaseField_Limitado(40, "", false, true) {
			String text;

			@Override
			public void setEnabled(boolean enabled) {
				super.setEnabled(enabled);
				if (enabled == false) {
					super.setBackground(new Color(190, 190, 190));
					if (super.getText().isEmpty() == false)
						text = super.getText();
					super.setText("");
					super.setBorder(null);
				} else {
					super.setBackground(Color.WHITE);
					super.setText(text);
					super.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));

				}

				labelEspecifique.setVisible(enabled);

			}

			@Override
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}
		};
		txtReceptor = new UpperCaseField_Limitado(35, "[^a-zA-Z ]", false, true) {
			public void setEditable(boolean edit) {
				super.setEditable(edit);
				if (edit) {
					super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
					super.setOpaque(true);
				} else {
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					super.setOpaque(false);
				}
			}

		};
		try {
			txtData = new JFormattedTextField(new MaskFormatter("##/##/####")) {
				public void setEditable(boolean edit) {
					super.setEditable(edit);
					if (edit) {
						super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
						super.setOpaque(true);
					} else {
						super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
						super.setOpaque(false);
					}
				}
			};
			txtData.setFont(new java.awt.Font("Arial", 0, 12));
			txtData.setHorizontalAlignment(SwingConstants.RIGHT);
		} catch (ParseException e) {
			txtData = new JTextField();
			txtData.setDocument(new Limite_digitos(10, "[^0-9|[/]]"));
			txtData.setFont(new java.awt.Font("Arial", 0, 12));
		}
		buttonPesquisa = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		scrollPane = new javax.swing.JScrollPane();
		table = new javax.swing.JTable();
		txtCountRows = new javax.swing.JLabel();
		buttonVoltar = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonAdd = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonMarcaAll = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonEdit = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		buttonMore = new javax.swing.JButton(){
			public void setEnabled(boolean edit) {
				super.setEnabled(edit);

				if (edit)
					super.setCursor(new Cursor(Cursor.HAND_CURSOR));
				else
					super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

		};
		progressBar = new javax.swing.JProgressBar();
		txtStatus = new javax.swing.JTextField();
model = new DoacaoTableModel(dD);
		
		labelCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelCod.setText("C�digo:");

		labelTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelTipo.setText("Tipo:");

		labelReceptor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelReceptor.setText("Receptor:");

		labelData.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelData.setText("Data:");

		txtCod.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		comboTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		comboTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Todos","Dinheiro", "Alimento", "Outros" }));
		comboTipo.setVerifyInputWhenFocusTarget(false);
		comboTipo.setFocusCycleRoot(false);
		comboTipo.setOpaque(false);
		comboTipo.setBackground(new Color(190, 190, 190));
		comboTipo.setFocusable(false);
		comboTipo.setBorder(null);
		comboTipo.addItemListener(e -> combo_Click());

		labelEspecifique.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		labelEspecifique.setText("Especifique:");

		txtTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		txtReceptor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		txtData.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonPesquisa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonVoltar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		buttonVoltar.setText("Voltar");

		table.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		cellRender = new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
				setBackground(new Color(204, 204, 204));
				return this;
			}
		};
		cellRender.setHorizontalAlignment(SwingConstants.CENTER);
		table.setFont(new Font("Arial", 0, 12)); // NOI18N
		table.setFocusCycleRoot(true);
		table.setRowHeight(19);
		table.setModel(model);
		table.setShowHorizontalLines(false);
		table.getColumnModel().getColumn(4).setPreferredWidth(75);
		table.getColumnModel().getColumn(3).setPreferredWidth(75);
		table.getColumnModel().getColumn(2).setPreferredWidth(344);
		table.getColumnModel().getColumn(1).setPreferredWidth(70);
		table.getColumnModel().getColumn(0).setPreferredWidth(90);
		table.getTableHeader().getColumnModel().getColumn(4).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(3).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(2).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(1).setHeaderRenderer(cellRender);
		table.getTableHeader().getColumnModel().getColumn(0).setHeaderRenderer(cellRender);
		table.getTableHeader().setResizingAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.setShowVerticalLines(false);
		table.setBorder(null);
		
		cellRender = new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

				try {
				
						if (isSelected) {
							setBackground(new Color(100, 100, 255));
						} else
							setBackground(new Color(200, 200, 255));

					
				} catch (Exception e) {

				}

				return this;
			}
		};
		cellRender.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(4).setCellRenderer(cellRender);
		table.getColumnModel().getColumn(3).setCellRenderer(cellRender);

		table.getColumnModel().getColumn(1).setCellRenderer(cellRender);
		table.setDefaultRenderer(Object.class, cellRender);
		
		table.getColumnModel().getColumn(0).setCellRenderer(cellRender);


		table.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

				try {
				
						if (isSelected) {
							setBackground(new Color(100, 100, 255));
						} else
							setBackground(new Color(200, 200, 255));

					
				} catch (Exception e) {

				}

				return this;
			}
		});
		table.setSelectionForeground(Color.BLACK);
		table.setIntercellSpacing(new Dimension(0, 0));
		
		scrollPane.setViewportView(table);

		txtCountRows.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
		txtCountRows.setFont(new java.awt.Font("Arial", 0, 12));

		buttonAdd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonEdit.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		buttonMore.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

		try {
			buttonMarcaAll
					.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\select.png")); // NOI18N
			buttonPesquisa
					.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\pesquisa.png")); // NOI18N
			buttonEdit.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\editlittle.png")); // NOI18N
			buttonAdd.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\addlittle.png")); // NOI18N
			buttonMore.setIcon(
					new javax.swing.ImageIcon(new File("").getCanonicalPath() + "\\images\\menu_vertical_16px.png")); // NOI18N
		} catch (Exception e) {
			txtStatus.setText(e.getMessage());
		}
		txtStatus.setEditable(false);
		txtStatus.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
		txtStatus.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
		txtStatus.setFocusable(false);
		txtStatus.setOpaque(false);
		txtStatus.setRequestFocusEnabled(false);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                .addGap(0, 178, Short.MAX_VALUE)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelTipo)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                        .addGap(37, 37, 37)
	                        .addComponent(labelEspecifique))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelCod)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelReceptor)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(txtReceptor, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(labelData)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(buttonPesquisa)
	                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addContainerGap(124, Short.MAX_VALUE))
	            .addGroup(layout.createSequentialGroup()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addContainerGap()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(scrollPane)
	                            .addGroup(layout.createSequentialGroup()
	                                .addComponent(buttonMarcaAll, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addGap(6, 6, 6)
	                                .addComponent(buttonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(buttonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                                .addComponent(buttonMore, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addGap(86, 86, 86)
	                                .addComponent(txtStatus))
	                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                                .addComponent(txtCountRows))))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(buttonVoltar)
	                        .addGap(0, 0, Short.MAX_VALUE)))
	                .addContainerGap())
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGap(12, 12, 12)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelCod)
	                    .addComponent(txtCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelTipo)
	                    .addComponent(comboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(labelEspecifique)
	                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(labelReceptor)
	                    .addComponent(txtReceptor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(buttonPesquisa, javax.swing.GroupLayout.Alignment.TRAILING)
	                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                        .addComponent(labelData)
	                        .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                .addGap(23, 23, 23)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                        .addComponent(buttonMore, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addComponent(buttonAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addComponent(buttonEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addComponent(buttonMarcaAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                    .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(progressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(txtCountRows, javax.swing.GroupLayout.Alignment.TRAILING))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(buttonVoltar))
	        );

		Component[] components = getComponents();

		for (Component c : components) {
			if (c instanceof JTextField) {
				((JTextField) c).setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
			} else if (c instanceof JButton) 
			{
				((JButton) c).setBorder(null);
				if(((JButton) c).getText().equals("Voltar")) ((JButton) c).setForeground(Color.RED);
				((JButton) c).setEnabled(true);
				((JButton) c).setContentAreaFilled(false);
				((JButton) c).setFocusable(false);
			}
		}
		
		txtCountRows.setText(model.getDoacoes().size()+" Doa��es cadastradas");
		scrollPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		txtStatus.setBorder(null);
		txtTipo.setEnabled(false);
	}// </editor-fold>

	public void combo_Click() {
		try {
			if (((String) comboTipo.getSelectedItem()).equals("Outros")) {
				txtTipo.setEnabled(true);
				txtTipo.requestFocus();
			} else {
				txtTipo.setEnabled(false);
			}

		} catch (Exception e) {

		}
	}
	
	// Variables declaration - do not modify
	private javax.swing.JButton buttonAdd;
	private javax.swing.JButton buttonEdit;
	private javax.swing.JButton buttonMore;
	private javax.swing.JButton buttonPesquisa;
	private javax.swing.JButton buttonVoltar;
	private javax.swing.JButton buttonMarcaAll;
	private javax.swing.JComboBox<String> comboTipo;
	private javax.swing.JLabel labelCod;
	private javax.swing.JLabel txtCountRows;
	private javax.swing.JLabel labelData;
	private javax.swing.JLabel labelEspecifique;
	private javax.swing.JLabel labelReceptor;
	private javax.swing.JLabel labelTipo;
	private javax.swing.JProgressBar progressBar;
	private javax.swing.JScrollPane scrollPane;
	private javax.swing.JTable table;
	private javax.swing.JTextField txtCod;
	private javax.swing.JTextField txtData;
	private javax.swing.JTextField txtReceptor;
	private javax.swing.JTextField txtStatus;
	private javax.swing.JTextField txtTipo;
	private DefaultTableCellRenderer cellRender;
	private DoacaoTableModel model;
	// End of variables declaration
	
	
	
	public Database getDD() {
		return dD;
	}

	public void setDD(Database dD) {
		this.dD = dD;
	}

	public javax.swing.JButton getButtonAdd() {
		return buttonAdd;
	}

	public void setButtonAdd(javax.swing.JButton buttonAdd) {
		this.buttonAdd = buttonAdd;
	}

	public javax.swing.JButton getButtonEdit() {
		return buttonEdit;
	}

	public void setButtonEdit(javax.swing.JButton buttonEdit) {
		this.buttonEdit = buttonEdit;
	}

	public javax.swing.JButton getButtonMore() {
		return buttonMore;
	}

	public void setButtonMore(javax.swing.JButton buttonMore) {
		this.buttonMore = buttonMore;
	}

	public javax.swing.JButton getButtonPesquisa() {
		return buttonPesquisa;
	}

	public void setButtonPesquisa(javax.swing.JButton buttonPesquisa) {
		this.buttonPesquisa = buttonPesquisa;
	}

	public javax.swing.JButton getButtonVoltar() {
		return buttonVoltar;
	}

	public void setButtonVoltar(javax.swing.JButton buttonVoltar) {
		this.buttonVoltar = buttonVoltar;
	}

	public javax.swing.JComboBox<String> getComboTipo() {
		return comboTipo;
	}

	public void setComboTipo(javax.swing.JComboBox<String> comboTipo) {
		this.comboTipo = comboTipo;
	}

	public javax.swing.JLabel getLabelCod() {
		return labelCod;
	}

	public void setLabelCod(javax.swing.JLabel labelCod) {
		this.labelCod = labelCod;
	}

	public javax.swing.JLabel getTxtCountRows() {
		return txtCountRows;
	}

	public void setTxtCountRows(javax.swing.JLabel txtCountRows) {
		this.txtCountRows = txtCountRows;
	}

	public javax.swing.JLabel getLabelData() {
		return labelData;
	}

	public void setLabelData(javax.swing.JLabel labelData) {
		this.labelData = labelData;
	}

	public javax.swing.JLabel getLabelEspecifique() {
		return labelEspecifique;
	}

	public void setLabelEspecifique(javax.swing.JLabel labelEspecifique) {
		this.labelEspecifique = labelEspecifique;
	}

	public javax.swing.JLabel getLabelReceptor() {
		return labelReceptor;
	}

	public void setLabelReceptor(javax.swing.JLabel labelReceptor) {
		this.labelReceptor = labelReceptor;
	}

	public javax.swing.JLabel getLabelTipo() {
		return labelTipo;
	}

	public void setLabelTipo(javax.swing.JLabel labelTipo) {
		this.labelTipo = labelTipo;
	}

	public javax.swing.JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(javax.swing.JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public javax.swing.JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(javax.swing.JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public javax.swing.JTable getTable() {
		return table;
	}

	public void setTable(javax.swing.JTable table) {
		this.table = table;
	}

	public javax.swing.JTextField getTxtCod() {
		return txtCod;
	}

	public void setTxtCod(javax.swing.JTextField txtCod) {
		this.txtCod = txtCod;
	}

	public javax.swing.JTextField getTxtData() {
		return txtData;
	}

	public void setTxtData(javax.swing.JTextField txtData) {
		this.txtData = txtData;
	}

	public javax.swing.JTextField getTxtReceptor() {
		return txtReceptor;
	}

	public void setTxtReceptor(javax.swing.JTextField txtReceptor) {
		this.txtReceptor = txtReceptor;
	}

	public javax.swing.JTextField getTxtStatus() {
		return txtStatus;
	}

	public void setTxtStatus(javax.swing.JTextField txtStatus) {
		this.txtStatus = txtStatus;
	}

	public javax.swing.JTextField getTxtTipo() {
		return txtTipo;
	}

	public void setTxtTipo(javax.swing.JTextField txtTipo) {
		this.txtTipo = txtTipo;
	}

	public DefaultTableCellRenderer getCellRender() {
		return cellRender;
	}

	public void setCellRender(DefaultTableCellRenderer cellRender) {
		this.cellRender = cellRender;
	}

	public DoacaoTableModel getModelTable() {
		return model;
	}

	public void setModelTable(DoacaoTableModel model) {
		this.model = model;
	}


	public javax.swing.JButton getButtonMarcaAll() {
		return buttonMarcaAll;
	}

	public void setButtonMarcaAll(javax.swing.JButton buttonMarcaAll) {
		this.buttonMarcaAll = buttonMarcaAll;
	}

}