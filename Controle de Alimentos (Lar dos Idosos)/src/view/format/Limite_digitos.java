package view.format;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

@SuppressWarnings("serial")
public class Limite_digitos extends PlainDocument 
{
    private int quantidadeMax;
    private String bloq;
    private boolean up = false;
    public Limite_digitos (int maxLen, String bloq)
    {
        super();
        quantidadeMax = maxLen;
        this.bloq = bloq;
    }
    
    public Limite_digitos (int maxLen, String bloq, boolean up)
    {
        super();
        this.up = up;
        quantidadeMax = maxLen;
        this.bloq = bloq;
    }
    
    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException
    {
        if(str == null || getLength() == quantidadeMax) return;
        if(up) str = str.toUpperCase();
        int totalquantia = (getLength() + str.length());
        if(totalquantia <= quantidadeMax)
        {
            super.insertString(offset,str.replaceAll(bloq,""),attr);
            return;
        }
    }    
}