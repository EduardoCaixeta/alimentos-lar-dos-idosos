package view.forms;

import javax.swing.JFrame;
import javax.swing.JPanel;
import model.Database;
import model.entity.Users;
import view.App;
import view.panels.PanelDoacoes;

public class FormDoacao {
	private PanelDoacoes panel;
	public FormDoacao(Database dD,Database dS, JFrame frame, Users user, App app) {
		this.panel = new PanelDoacoes(dD, dS,frame, user, app);
		
	}
	
	public void inicioState()
	{
		if(panel.getButtonVoltar() != null)
			panel.initComponents();
	}
	
	public JPanel getPanel() {
		return this.panel;
	}
}
