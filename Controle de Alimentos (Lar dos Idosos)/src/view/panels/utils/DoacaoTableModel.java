package view.panels.utils;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import controller.DoacaoController;
import model.Database;
import model.entity.Doacao;

@SuppressWarnings("serial")
public class DoacaoTableModel extends AbstractTableModel {

	private String colunas[] = {"C�digo",  "Tipo", "Descri��o", "Quantidade", "Data" };
	private List<Doacao> doacoes;
	private final int COLUNA_COD = 0;
	private final int COLUNA_TIPO = 1;
	private final int COLUNA_DESC = 2;
	private final int COLUNA_QNTD = 3;
	private final int COLUNA_DATA = 4;
	int rows;

	public DoacaoTableModel(Database dD) {
		try {
			doacoes = new Ordenacao().ordeneDoacoes(new DoacaoController(dD).readAll());
			rows = this.doacoes.size();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	// retorna se a célula é edit�vel ou n�o
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	// retorna o total de itens(que virar�o linhas) da nossa lista
	@Override
	public int getRowCount() {
		return doacoes.size();
	}

	// retorna o total de colunas da tabela
	@Override
	public int getColumnCount() {
		return colunas.length;
	}

	// retorna o nome da coluna de acordo com seu indice
	@Override
	public String getColumnName(int indice) {
		return colunas[indice];
	}

	// determina o tipo de dado da coluna conforme seu indice
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case COLUNA_COD:
			return String.class;
		case COLUNA_TIPO:
			return String.class;
		case COLUNA_DESC:
			return String.class;
		case COLUNA_QNTD:
			return String.class;
		case COLUNA_DATA:
			return String.class;
		default:
			return String.class;
		}
	}

	// preenche cada célula da tabela
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Doacao doacao = this.doacoes.get(rowIndex);

		switch (columnIndex) {
		case COLUNA_COD:
			return doacao.getCodigo();
		case COLUNA_DESC:
			return "   "+doacao.getDesc();
		case COLUNA_TIPO:
			return doacao.getTipo();
		case COLUNA_QNTD:
			return doacao.getQnt();
		case COLUNA_DATA:
			return doacao.printData();
		}
		return null;
	}
	
	public void setDoacoes(List<Doacao> doacoes) throws Exception {
		try {
			this.doacoes = new Ordenacao().ordeneDoacoes(doacoes);
			fireTableRowsUpdated(0, doacoes.size());
			if (doacoes.size() < rows) {
				fireTableRowsDeleted(doacoes.size(), rows);
			} else {
				fireTableRowsInserted(rows, doacoes.size());
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void updateValidade() throws Exception {
		try {
			this.doacoes = new Ordenacao().ordeneDoacoes(this.doacoes);
			fireTableRowsUpdated(0, doacoes.size());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}


	public List<Doacao> getDoacoes() {
		return this.doacoes;
	}

}
	// altera o valor do objeto de acordo com a célula editada
	// e notifica a alteraç�o da tabela, para que ela seja atualizada na tela
