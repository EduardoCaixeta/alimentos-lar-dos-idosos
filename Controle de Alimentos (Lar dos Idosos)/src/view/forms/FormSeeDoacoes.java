package view.forms;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.DoacaoController;
import model.Database;
import model.entity.Doacao;
import view.App;
import view.forms.utils.CreatePDF;
import view.panels.PanelMoreDoacao;
import view.panels.PanelSeeDoacao;

public class FormSeeDoacoes {
	private PanelSeeDoacao panel;
	private DoacaoController controllerDoacao;
	private List<Doacao> doacoes;
	private boolean selectAll;

	public FormSeeDoacoes(Database dD, JFrame frame, App app) {
		panel = new PanelSeeDoacao(dD);
		selectAll = false;
		controllerDoacao = new DoacaoController(dD);
		try {
			doacoes = controllerDoacao.readAll();
			if (doacoes.size() == 0) {
				panel.getButtonMarcaAll().setEnabled(false);
				panel.getButtonEdit().setEnabled(false);
				throw new Exception("N�o h� doa��es cadastradas.");
			}
		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}
		
		panel.getButtonAdd().addActionListener(e -> app.getMenuBarra().getButtonDoacao().doClick());
		panel.getButtonAdd().setMnemonic(KeyEvent.VK_S);
		panel.getButtonAdd().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonAdd().doClick();
				}
			}
		});


		panel.getButtonPesquisa().addActionListener(e -> pesquisa_Click());
		panel.getButtonPesquisa().setMnemonic(KeyEvent.VK_S);
		panel.getButtonPesquisa().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonPesquisa().doClick();
				}
			}
		});

		panel.getButtonMarcaAll().addActionListener(e -> marcaAll_Click());
		panel.getButtonMarcaAll().setMnemonic(KeyEvent.VK_S);
		panel.getButtonMarcaAll().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonMarcaAll().doClick();
				}
			}
		});

		// panel.getButtonEdit().addActionListener(e -> edit_Click());
		panel.getButtonEdit().setMnemonic(KeyEvent.VK_S);
		panel.getButtonEdit().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					panel.getButtonEdit().doClick();
				}
			}
		});

		panel.getTable().addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				try {
					panel.getTxtStatus().setText("");
					if (e.isMetaDown())
						if (panel.getTable().getSelectedRow() != -1) {
							int row = panel.getTable().rowAtPoint(e.getPoint());
							boolean select = false;
							for (int i = 0; i < panel.getTable().getSelectedRows().length; i++) {
								if (panel.getTable().getSelectedRows()[i] == row)
									select = true;
							}
							if (select) {
								List<Doacao> selecionados = new ArrayList<Doacao>();
								for (int i = 0; i < panel.getTable().getSelectedRows().length; i++) {
									selecionados.add(panel.getModelTable().getDoacoes()
											.get(panel.getTable().getSelectedRows()[i]));
								}
								new PanelMoreDoacao(frame, selecionados);
							}

						} else

							throw new Exception("Selecione ao menos um cadastro");

				} catch (Exception ex) {
					panel.getTxtStatus().setText(ex.getMessage());
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});
		
		panel.getTable().addKeyListener(new KeyAdapter() {
			@SuppressWarnings("deprecation")
			public void keyPressed(KeyEvent e) {
				if ((e.getKeyCode() == KeyEvent.VK_P) && (e.getModifiers() & KeyEvent.CTRL_MASK) != 0) {
					if (panel.getTable().getSelectedRow() != -1) {
						List<Doacao> doacoes = new ArrayList<Doacao>();
						for (int i = 0; i < panel.getTable().getSelectedRows().length; i++) {
							doacoes.add(
									panel.getModelTable().getDoacoes().get(panel.getTable().getSelectedRows()[i]));
						}
						new CreatePDF().criarDoacao(frame, doacoes);
					} else {
						panel.getTxtStatus().setText("Selecione um cadastro.");
					}
				}
			}
		});
		
	}

	public void inicioState()
	{
		panel.initComponents();
	}
	
	public void marcaAll_Click() {
		if (selectAll) {
			panel.getTable().clearSelection();
			selectAll = false;
		} else {
			panel.getTable().selectAll();
			selectAll = true;
		}

		panel.getTable().requestFocus();
	}

	public void pesquisa_Click() {
		panel.getTxtStatus().setText("");
		panel.getTable().clearSelection();
		List<Doacao> doacoesPesq = new ArrayList<Doacao>();
		try {
			if (panel.getTxtCod().getText().trim().isEmpty() == false) {
				for (Doacao doacao : doacoes) {
					if (doacao.getCodigo().equals(panel.getTxtCod().getText())) {
						doacoesPesq.add(doacao);
						break;
					}
				}
				if (doacoesPesq.size() > 0) {
					panel.getModelTable().setDoacoes(doacoesPesq);
					panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Doa��es cadastradas");
					throw new Exception("Doa��o encontrada.");
				} else {
					panel.getModelTable().setDoacoes(doacoes);
					panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Doa��es cadastradas");
					throw new Exception("Doa��o n�o encontrada.");
				}
			} else {
				Doacao doacaoPesq = new Doacao();
				if (((String) panel.getComboTipo().getSelectedItem()).equals("Outros")) {
					if (panel.getTxtTipo().getText().trim().isEmpty() == false) {
						doacaoPesq.setTipo(panel.getTxtTipo().getText());
						if (panel.getTxtReceptor().getText().trim().isEmpty() == false) {
							doacaoPesq.setReceptor(panel.getTxtReceptor().getText());

							String date = panel.getTxtData().getText().substring(0, 2);
							date += panel.getTxtData().getText().substring(3, 5);
							date += panel.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(panel.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						} else {
							String date = panel.getTxtData().getText().substring(0, 2);
							date += panel.getTxtData().getText().substring(3, 5);
							date += panel.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(panel.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						}
					} else {
						if (panel.getTxtReceptor().getText().trim().isEmpty() == false) {
							doacaoPesq.setReceptor(panel.getTxtReceptor().getText());

							String date = panel.getTxtData().getText().substring(0, 2);
							date += panel.getTxtData().getText().substring(3, 5);
							date += panel.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(panel.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						} else {
							String date = panel.getTxtData().getText().substring(0, 2);
							date += panel.getTxtData().getText().substring(3, 5);
							date += panel.getTxtData().getText().substring(6, 10);
							if (date.trim().isEmpty() == false) {
								checkDate();
								Calendar data = Calendar.getInstance();
								data.setTime(stringToDate(panel.getTxtData().getText()));
								data.add(Calendar.MONTH, -1);
								doacaoPesq.setData(data.getTime());
							}
						}
					}
				} else {
					doacaoPesq.setTipo((String) panel.getComboTipo().getSelectedItem());
					if (panel.getTxtReceptor().getText().trim().isEmpty() == false) {
						doacaoPesq.setReceptor(panel.getTxtReceptor().getText());

						String date = panel.getTxtData().getText().substring(0, 2);
						date += panel.getTxtData().getText().substring(3, 5);
						date += panel.getTxtData().getText().substring(6, 10);
						if (date.trim().isEmpty() == false) {
							checkDate();
							Calendar data = Calendar.getInstance();
							data.setTime(stringToDate(panel.getTxtData().getText()));
							data.add(Calendar.MONTH, -1);
							doacaoPesq.setData(data.getTime());
						}
					} else {
						String date = panel.getTxtData().getText().substring(0, 2);
						date += panel.getTxtData().getText().substring(3, 5);
						date += panel.getTxtData().getText().substring(6, 10);
						if (date.trim().isEmpty() == false) {
							checkDate();
							Calendar data = Calendar.getInstance();
							data.setTime(stringToDate(panel.getTxtData().getText()));
							data.add(Calendar.MONTH, -1);
							doacaoPesq.setData(data.getTime());
						}
					}

				}
				for (Doacao d : doacoes) {
					if (d.getTipo().equals(doacaoPesq.getTipo())
							|| ((String) panel.getComboTipo().getSelectedItem()).equals("Todos")) {
						if (doacaoPesq.getData() != null) {
							System.out.print(doacaoPesq.printData());
							System.out.print("\n" + d.printData() + "\n\n\n");

							if (d.printData().equals(doacaoPesq.printData())) {
								if (doacaoPesq.getReceptor() != null) {
									if (d.getReceptor().equals(doacaoPesq.getReceptor())) {
										doacoesPesq.add(d);
									}
								} else {
									doacoesPesq.add(d);
								}
							}
						} else {
							if (doacaoPesq.getReceptor() != null) {
								if (d.getReceptor().equals(doacaoPesq.getReceptor())) {
									doacoesPesq.add(d);
								}
							} else {
								doacoesPesq.add(d);
							}
						}

					}

				}
				if (doacoesPesq.size() > 0) {
					panel.getModelTable().setDoacoes(doacoesPesq);
					panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Doa��escadastradas");
					clearTextFields();
					throw new Exception("Doa��o encontrada.");
				} else {
					panel.getModelTable().setDoacoes(doacoes);
					panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Doa��es cadastradas");
					throw new Exception("Doa��o n�o encontrada.");
				}
			}

		} catch (Exception e) {
			panel.getTxtStatus().setText(e.getMessage());
		}

	}

	public void clearTextFields() {
		Component components[] = panel.getComponents();

		for (Component c : components) {
			if (c instanceof JTextField) {
				((JTextField) c).setText("");

			}
		}
	}

	public void checkDate() throws Exception {
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(stringToDate(panel.getTxtData().getText()));

		try {
			endCalendar.add(Calendar.MONTH, -1);
			analiseDate(panel.getTxtData().getText());
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}
	}

	public void analiseDate(String text) throws Exception {
		try {
			int dia = Integer.parseInt(text.substring(0, 2));
			int mes = Integer.parseInt(text.substring(3, 5));
			int ano = Integer.parseInt(text.substring(6, 10));

			int[] meses31 = { 4, 6, 9, 11 };

			if (1 > dia || dia > 31)
				throw new Exception("Dia inv�lido");

			if (1 > mes || mes > 12)
				throw new Exception("M�s inv�lido");
			if (dia == 31) {
				for (int i = 0; i < 4; i++) {
					if (mes == meses31[i]) {
						throw new Exception("Dia inv�lido");
					}
				}
			}
			if (mes == 2) {
				boolean ehBissexto = false;
				if ((ano % 400) == 0) {
					ehBissexto = true;
				} else {
					if (((ano % 4) == 0) && ((ano % 100) != 0)) {
						ehBissexto = true;
					}
				}

				if (ehBissexto) {
					if (dia > 29)
						throw new Exception("Dia inv�lido");
				} else {
					if (dia > 28)
						throw new Exception("Dia inv�lido");
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage().toUpperCase());
		}

	}

	public Date stringToDate(String string) throws Exception {
		Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
		Matcher matcher = dataPadrao.matcher(string);
		if (matcher.matches()) {
			int dia = Integer.parseInt(matcher.group(1));
			int mes = Integer.parseInt(matcher.group(2));
			int ano = Integer.parseInt(matcher.group(3));
			return (new GregorianCalendar(ano, mes, dia)).getTime();
		} else
			throw new Exception(" Data final inv�lida.");
	}

	public JPanel getPanel() {
		return this.panel;
	}
}