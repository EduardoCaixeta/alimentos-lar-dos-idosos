package model.entity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import com.j256.ormlite.table.DatabaseTable;

import controller.DoacaoController;
import model.Database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

@DatabaseTable(tableName = "donations")
public class Doacao {
	@DatabaseField(generatedId = true)
	int id;

	@DatabaseField
	String tipo;

	@DatabaseField
	String doador;

	@DatabaseField
	String receptor;

	@DatabaseField(dataType = DataType.DATE)
	Date date;

	@DatabaseField
	String qnt;

	@DatabaseField
	String desc;

	@DatabaseField
	String db;

	@DatabaseField
	int indexDiario;

	public String printData() {
		SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
		return dateFor.format(date.getTime());
	}

	public int getId() {

		return id;

	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo.toUpperCase();
	}

	public String getDoador() {
		return doador;
	}

	public void setDoador(String doador) {
		this.doador = doador.toUpperCase();
	}

	public String getReceptor() {
		return receptor;
	}

	public void setReceptor(String receptor) {
		this.receptor = receptor.toUpperCase();
	}

	public Date getData() {
		return this.date;
	}

	public void setData(Date data) {
		this.date = data;
	}

	public String getQnt() {
		if(tipo.toUpperCase().equals("DINHEIRO"))
		{
			return "R$ "+ String.format("%.2f", Double.valueOf(qnt));
			
		}
		else if (tipo.toUpperCase().equals("ALIMENTO")==false){
			return  String.format("%d", Double.valueOf(qnt).intValue());
			
		}
		else
		{
			DecimalFormat format = new DecimalFormat();
			format.setMaximumFractionDigits(2);
			return  String.format("%.2f", Double.valueOf(qnt));
		}
			
	}

	public void setQnt(String qnt) {
		this.qnt = qnt;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc.toUpperCase();
	}

	public String getCodigo() {

		String codigo = String.valueOf(indexDiario + 1);
		codigo += tipo.substring(0, 2) + "-";
		String date = printData();
		codigo += date.substring(0, 2);
		codigo += date.substring(3, 5);
		codigo += date.substring(6, 10);

		return codigo;

	}

	public void setIndexDiario()
	{
		try {
			List<Doacao> doacoes = new DoacaoController(new Database(db)).readAll();
			int count = 0;
			for (int i = 0; i < doacoes.size(); i++) {
				if (doacoes.get(i).printData().equals(printData())) {
					count++;
				}
			}
			this.indexDiario = count;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	public int getIndexDiario()
	{
		return indexDiario;
	}
	
	public String getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db.getName();
	}
}
