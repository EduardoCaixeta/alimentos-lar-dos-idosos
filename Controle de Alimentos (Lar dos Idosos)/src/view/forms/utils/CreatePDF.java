package view.forms.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import model.entity.Doacao;
import model.entity.Produtos;
import model.entity.Validade;

public class CreatePDF {

	public void criarValidades(JFrame frame, List<Validade> validades) {
		FileNameExtensionFilter filtroWord = new FileNameExtensionFilter("pdf", "docx");
		final JFileChooser arq = new JFileChooser("C:\\Users\\" + System.getProperty("user.name") + "\\Desktop");
		;
		arq.setSelectedFile(new File("Validades.pdf"));

		arq.setFileFilter(filtroWord);
		int save = arq.showSaveDialog(null);
		arq.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		if (save == JFileChooser.APPROVE_OPTION) {
			File filePDF = arq.getSelectedFile();
			String nome = filePDF.getName();
			if (nome.indexOf('.') == -1) {
				nome += ".pdf";
				filePDF = new File(filePDF.getParent(), nome);
			}

			try {

				Font font = FontFactory.getFont("Arial", 10, Font.NORMAL, new BaseColor(BaseColor.BLACK.getRGB()));

				PdfPTable table = new PdfPTable(4);
				table.setWidths(new int[] { 40, 120, 40, 40 });
				PdfPCell table_cell;
				Phrase txt;

				// header
				for (int i = 0; i < 4; i++) {
					if (i == 0) {
						txt = new Phrase("C�D. BARRAS", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else if (i == 1) {
						txt = new Phrase("NOME", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else if (i == 2) {
						txt = new Phrase("QUANTIDADE", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else {
						txt = new Phrase("VENCIMENTO", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					}

					table_cell.setVerticalAlignment(Element.ALIGN_CENTER);
					table_cell.setBorder(0);
					table.addCell(table_cell);
				}

				for (int i = 0; i < 4; i++) {
					table_cell = new PdfPCell(new Phrase("", font));
					table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table_cell.setBorder(0);
					table_cell.setFixedHeight(4);
					table.addCell(table_cell);

				}

				for (int j = 0; j < validades.size(); j++) {
					for (int i = 0; i < 4; i++) {

						if (i == 0) {
							txt = new Phrase(validades.get(j).getCodBar(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							table_cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
						} else if (i == 1) {
							txt = new Phrase(validades.get(j).getNome(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							table_cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						} else if (i == 2) {
							txt = new Phrase(String.valueOf(validades.get(j).getQuantidade()), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							table_cell.setVerticalAlignment(Element.ALIGN_TOP);
						} else {
							txt = new Phrase(String.valueOf(validades.get(j).printEndDate()), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						}

						table_cell.setBorder(0);
						if (j == 0) {
							table_cell.setBorderWidthTop((float) 0.3);
						}
						table_cell.setBorderWidthBottom((float) 0.3);
						table.addCell(table_cell);
					}
				}

				font = FontFactory.getFont("Arial", 15, Font.BOLD, new BaseColor(BaseColor.BLACK.getRGB()));

				Paragraph p = new Paragraph();

				p.add(new Phrase("VALIDADES CADASTRADAS  -  "
						+ new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()) + "\r\n\r\n",
						font));
				p.setAlignment(Element.ALIGN_CENTER);

				p.setLeading(20);

				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePDF));
				document.setMargins(-20, -20, 20, 10);
				document.setMarginMirroring(false);
				document.open();
				document.add(p);
				document.add(table);
				document.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(frame, e.getMessage());
			}
		}
	}

	public void criarProdutos(JFrame frame, List<Produtos> produtos) {
		FileNameExtensionFilter filtroWord = new FileNameExtensionFilter("pdf", "docx");
		final JFileChooser arq = new JFileChooser("C:\\Users\\" + System.getProperty("user.name") + "\\Desktop");
		;
		arq.setSelectedFile(new File("Produtos.pdf"));

		arq.setFileFilter(filtroWord);
		int save = arq.showSaveDialog(null);
		arq.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		if (save == JFileChooser.APPROVE_OPTION) {
			File filePDF = arq.getSelectedFile();
			String nome = filePDF.getName();
			if (nome.indexOf('.') == -1) {
				nome += ".pdf";
				filePDF = new File(filePDF.getParent(), nome);
			}

			try {

				Font font = FontFactory.getFont("Arial", 10, Font.NORMAL, new BaseColor(BaseColor.BLACK.getRGB()));

				PdfPTable table = new PdfPTable(3);
				table.setWidths(new int[] { 20, 50, 170 });

				PdfPCell table_cell;
				Phrase txt;

				// header
				for (int i = 0; i < 3; i++) {
					if (i == 0) {
						txt = new Phrase("ID", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else if (i == 1) {
						txt = new Phrase("C�D. BARRAS", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else {
						txt = new Phrase("NOME", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					}

					table_cell.setVerticalAlignment(Element.ALIGN_CENTER);
					table_cell.setBorder(0);
					table.addCell(table_cell);
				}

				for (int i = 0; i < 3; i++) {
					table_cell = new PdfPCell(new Phrase("", font));
					table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table_cell.setBorder(0);
					table_cell.setFixedHeight(4);
					table.addCell(table_cell);

				}

				for (int j = 0; j < produtos.size(); j++) {
					for (int i = 0; i < 3; i++) {

						if (i == 0) {
							txt = new Phrase(produtos.get(j).getCodBar(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						} else if (i == 1) {
							txt = new Phrase(produtos.get(j).getNome(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						} else {
							txt = new Phrase(String.valueOf(produtos.get(j).getId()), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						}

						table_cell.setBorder(0);
						if (j == 0) {
							table_cell.setBorderWidthTop((float) 0.3);
						}
						table_cell.setBorderWidthBottom((float) 0.5);
						table.addCell(table_cell);
					}
				}
				Paragraph p = new Paragraph();

				font = FontFactory.getFont("Arial", 15, Font.BOLD, new BaseColor(BaseColor.BLACK.getRGB()));
				p.add(new Phrase("PRODUTOS CADASTRADOS  -  "
						+ new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()) + "\r\n\r\n",
						font));
				p.setAlignment(Element.ALIGN_CENTER);

				p.setLeading(20);
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePDF));
				document.setMargins(-20, -20, 20, 10);
				document.setMarginMirroring(false);
				document.open();
				document.add(p);
				document.add(table);
				document.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(frame, e.getMessage());
			}
		}
	}

	public void criarDoacao(JFrame frame, List<Doacao> doacoes) {
		FileNameExtensionFilter filtroWord = new FileNameExtensionFilter("pdf", " ");
		final JFileChooser arq = new JFileChooser("C:\\Users\\" + System.getProperty("user.name") + "\\Desktop");

		arq.setSelectedFile(new File("Doa��es.pdf"));

		arq.setFileFilter(filtroWord);
		int save = arq.showSaveDialog(null);
		arq.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		if (save == JFileChooser.APPROVE_OPTION) {
			File filePDF = arq.getSelectedFile();
			String nome = filePDF.getName();
			if (nome.indexOf('.') == -1) {
				nome += ".pdf";
				filePDF = new File(filePDF.getParent(), nome);
			}

			try {

				Font font = FontFactory.getFont("Arial", 10, Font.NORMAL, new BaseColor(BaseColor.BLACK.getRGB()));

				PdfPTable table = new PdfPTable(5);
				table.setWidths(new int[] { 90, 70, 265, 70, 65 });

				PdfPCell table_cell;
				Phrase txt;

				// header
				for (int i = 0; i < 5; i++) {
					if (i == 0) {
						txt = new Phrase("C�DIGO", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else if (i == 1) {
						txt = new Phrase("TIPO", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else if (i == 2) {
						txt = new Phrase("DESCRI��O", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else if (i == 3) {
						txt = new Phrase("QNTD", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					} else {
						txt = new Phrase("DATA", font);
						table_cell = new PdfPCell(txt);
						table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					}

					table_cell.setVerticalAlignment(Element.ALIGN_CENTER);
					table_cell.setBorder(0);
					table.addCell(table_cell);
				}

				for (int i = 0; i < 5; i++) {
					table_cell = new PdfPCell(new Phrase("", font));
					table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table_cell.setBorder(0);
					table_cell.setFixedHeight(4);
					table.addCell(table_cell);

				}

				font = FontFactory.getFont("Arial", 10, Font.NORMAL, new BaseColor(BaseColor.BLACK.getRGB()));

				for (int j = 0; j < doacoes.size(); j++) {
					for (int i = 0; i < 5; i++) {

						if (i == 0) {
							txt = new Phrase(doacoes.get(j).getCodigo(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						} else if (i == 1) {
							txt = new Phrase(doacoes.get(j).getTipo(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						} else if (i == 2) {
							txt = new Phrase(String.valueOf(doacoes.get(j).getDesc()), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						} else if (i == 3) {
							txt = new Phrase(doacoes.get(j).getQnt(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						} else {
							txt = new Phrase(doacoes.get(j).printData(), font);
							table_cell = new PdfPCell(txt);
							table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						}

						table_cell.setBorder(0);
						if (j == 0) {
							table_cell.setBorderWidthTop((float) 0.3);
						}
						table_cell.setBorderWidthBottom((float) 0.5);
						table.addCell(table_cell);
					}
				}
				Paragraph p = new Paragraph();

				font = FontFactory.getFont("Arial", 15, Font.BOLD, new BaseColor(BaseColor.BLACK.getRGB()));
				p.add(new Phrase("DOA��ES CADASTRADAS  -  "
						+ new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()) + "\r\n\r\n",
						font));
				p.setAlignment(Element.ALIGN_CENTER);

				p.setLeading(20);
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(filePDF));
				document.setMargins(-20, -20, 20, 10);
				document.setMarginMirroring(false);
				document.open();
				document.add(p);
				document.add(table);
				document.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(frame, e.getMessage());

			}
		}
	}

}
