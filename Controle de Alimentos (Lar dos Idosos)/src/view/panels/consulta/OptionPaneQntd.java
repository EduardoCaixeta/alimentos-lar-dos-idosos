package view.panels.consulta;

import java.io.File;

@SuppressWarnings("serial")
public class OptionPaneQntd extends javax.swing.JPanel {

    public OptionPaneQntd() {
        initComponents();
    }                       
    private void initComponents() {

    	
        buttonMinus = new javax.swing.JButton();
        buttonAdd = new javax.swing.JButton();

        try
        {
        	 buttonMinus.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//minus_48px.png"));

             buttonAdd.setIcon(new javax.swing.ImageIcon(new File("").getCanonicalPath() + "//images//add_48px.png")); // NOI18N
        }catch(Exception e)
        {
        	
        }
       
       

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(buttonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 53, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonMinus, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(buttonMinus, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(buttonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>                        


    public javax.swing.JButton getButtonAdd() {
		return buttonAdd;
	}
	public void setButtonAdd(javax.swing.JButton buttonAdd) {
		this.buttonAdd = buttonAdd;
	}
	public javax.swing.JButton getButtonMinus() {
		return buttonMinus;
	}
	public void setButtonMinus(javax.swing.JButton buttonMinus) {
		this.buttonMinus = buttonMinus;
	}


	// Variables declaration - do not modify                     
    private javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonMinus;
    // End of variables declaration                   
}
